/*
* script to load a couple of breakdown graphs
* This will need to be replaced by a migration script which copies this data from the test environment
*
*/


-----------------------------------
-- these will all return 0 rows if this script has never been run before
-----------------------------------
/*
select * from metric_category where parent is null and account_code = 50;

select * from metric_category mc, metric_category mcp where mc.parent = mcp.id and mcp.account_code = 50;

select * from metric where breakdown is not null;

select * from metric_value where breakdown_item is not null;

select * from metric_breakdown;

select * from metric_breakdown_item;


select * from breakdown_graph_columns;

select * from breakdown_graph;

select * from breakdown_column;
*/


--------------------------------------------------------------
-- First delete all the breakdown related data
---------------------------------------------------------------

-- 1) delete the graphs
delete from breakdown_graph_columns;

delete from breakdown_graph;

delete from breakdown_column;

-- 2) 
delete from metric_value where breakdown_item is not null;


-- 3) delete any existing metrics with breakdowns
delete from metric where breakdown is not null;

-- 4) delete the breakdown sub categories
delete from metric_category where id in (
	select mcid from (
		select mc.id as mcid from metric_category mc, metric_category mcp where mc.parent = mcp.id and mcp.account_code = 50 and mcp.parent is null
	) as x
);

-- 5) delete the root breakdown metric category
-- now part of main migration script
-- delete from metric_category where parent is null and account_code = 50;

-- 6) delete the breakdown items and the breakdown
delete from metric_breakdown_item;
delete from metric_breakdown;

-- 7) create the metric categories
-- now part of main migration script
-- insert into metric_category (account_code, name, version)
-- values(50, "Breakdowns", 0);

insert into metric_category (account_code, name, version, parent)
values(10, "Debt Composition", 0,
	(select mc.id from metric_category mc where mc.parent is null and mc.account_code = 50)
);




---------------------------------------------------------------
-- 8) create the metric breakdown and breakdown items
---------------------------------------------------------------

INSERT INTO metric_breakdown (id, name, label, version)
VALUES
(1, "Debt by Investor", "Investor Type", 0);

insert into metric_breakdown_item (id, active, name, rank, version, metric_breakdown)
values
(1,1,"Federal Gov",0,0,1),
(2,1,"State or Local Gov",1,0,1),
(3,1,"Individuals",2,0,1),
(4,1,"Faith Based",3,0,1),
(5,1,"Foundations",4,0,1),
(6,1,"Financial Institutions",5,0,1);

-- 9) create the  breakdown metrics

insert into metric (account_code, name, version, type, rank, breakdown, parent)
values
(1, "Number of Investors", 1, 0, 2154, 1,
	(select mc.id from metric_category mc, metric_category mcp
	where mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(2, "Debt Outstanding", 1, 0, 2155, 1,
	(select mc.id from metric_category mc, metric_category mcp
	where mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(3, "Interest Paid", 1, 0, 2156, 1,
	(select mc.id from metric_category mc, metric_category mcp
	where mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
);


-----------------------------------------------
-- 10) now the metric values themselves... for Craft3
-----------------------------------------------
insert into metric_value (amount, quarter, year, company, breakdown_item, metric)
values
(1, 4, 2012, 74, 1,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Number of Investors" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(16, 4, 2012, 74, 2,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Number of Investors" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(392732.81, 4, 2012, 74, 1,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Debt Outstanding" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(2187565.93, 4, 2012, 74, 2,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Debt Outstanding" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(20.38, 4, 2012, 74, 1,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Interest Paid" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(96308.22, 4, 2012, 74, 2,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Interest Paid" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
);


---- 10a) some metric values for history
--
insert into metric_value (amount, quarter, year, company, breakdown_item, metric)
values
(100000.0, 4, 2011, 74, 1,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Debt Outstanding" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(200000.0, 4, 2011, 74, 2,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Debt Outstanding" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(150000.0, 4, 2010, 74, 1,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Debt Outstanding" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
),
(250000.0, 4, 2010, 74, 2,
    (select m.id from metric m, metric_category mc, metric_category mcp
	where m.name = "Debt Outstanding" and m.parent = mc.id
	and mc.parent = mcp.id and mc.account_code = 10 and mcp.parent is null and mcp.account_code = 50)
);


-----------------------------
-- 11) and finally the breakdown graphs 
-----------------------------

/*
ALTER TABLE breakdown_graph_columns AUTO_INCREMENT = 1;
ALTER TABLE breakdown_graph AUTO_INCREMENT = 1;
ALTER TABLE breakdown_column AUTO_INCREMENT = 1;
*/
 
insert into breakdown_column (id, equation, total_equation, name, rank, show_total, version, breakdown, decimal_places, unit_type)
values
(1, "501001!", NULL, "Number of Investors", 0, 1, 0, 1, 0, 0),
(2, "501002!", NULL, "Total Loans Payable", 1, 1, 0, 1, 0, 2),
(3, "501003!", NULL, "Interest Paid", 2, 1, 0, 1, 0, 2),
(4, "501002! / SUM 501002!", "1", "% of Total Loans Payable", 3, 1, 0, 1, 1, 1),
(5, "501002! / 501001!", "SUM 501002! / SUM 501001!", "Average per Investor", 4, 1, 0, 1, 0, 2),
(6, "501003! / 501002!", "SUM 501003! / SUM 501002!", "Weighted Average Rate", 5, 1, 0, 1,1, 1);


insert into breakdown_graph (id, dtype, title, version, breakdown, equation, graph_type, show_interim, show_years, export)
values
(1, "BreakdownTable", "Sources of Debt", 0, 1, NULL, NULL, NULL, NULL, 1),
(2, "BreakdownPie", "Debt Composition by Source", 0, 1, 2, NULL, NULL, NULL, 1),
(3, "BreakdownHistory", "Debt History Table", 0, 1, 2, 3, 0, 5, 0),
(4, "BreakdownHistory", "Debt History Chart", 0, 1, 2, 1, 0, 5, 1);


-- only for BreakdownTable
insert into breakdown_graph_columns (breakdown_graph, columns)
values
(1, 1),
(1, 2),
(1, 4),
(1, 5),
(1, 6);

-- select * from breakdown_column








