DELIMITER $$
CREATE DEFINER=CURRENT_USER PROCEDURE `migrate_threepack`()
BEGIN


CREATE TEMPORARY TABLE IF NOT EXISTS mytable
(p_owner bigint(20), p_cdfi bigint(20), p_date datetime) ENGINE=MyISAM;

INSERT IGNORE INTO mytable SELECT s.owner, a.company, a.expiration_date from subscription_cdfis sc, subscription s, access a
where sc.subscription = s.id and sc.cdfis = a.id and s.subscription_type = 1;

-- select create_single_subscription(p_owner, p_cdfi, p_date, 0, 1) from mytable;

select create_single_subscription(p_owner, p_cdfi, p_date, 1, 0) from mytable;

-- delete the old THREE_PACK subscriptions
select delete_subscription(id) from subscription s where s.subscription_type = 1;

delete from subscription where subscription_type = 1;

DROP TEMPORARY TABLE mytable;

END$$
DELIMITER ;





DELIMITER $$
CREATE DEFINER=CURRENT_USER PROCEDURE `migrate_financials`()
BEGIN


CREATE TEMPORARY TABLE IF NOT EXISTS mytable
(p_owner bigint(20), p_cdfi bigint(20), p_date datetime) ENGINE=MyISAM;

INSERT IGNORE INTO mytable SELECT a.company, s.owner, s.expiration_date from subscription_subscribers ss, subscription s, access a
where ss.subscription = s.id and ss.subscribers = a.id and s.financials = 1 and (s.subscription_type = 3 or s.subscription_type = 4);

select create_single_subscription(p_owner, p_cdfi, p_date, 0, 1) from mytable;

-- delete subscription which have only financials set (but lets leave Select subscriptions alone... they shouldn't have financials set)
select delete_subscription(id) from subscription s where s.subscription_type = 3 and s.financials = 1 and s.ratings = 0 and s.library = 0;
delete from subscription where subscription_type = 3 and financials = 1 and ratings = 0 and library = 0;


DROP TEMPORARY TABLE mytable;


END$$
DELIMITER ;