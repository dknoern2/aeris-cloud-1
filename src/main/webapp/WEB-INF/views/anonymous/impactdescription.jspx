<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<div xmlns:jsp="http://java.sun.com/JSP/Page" 
xmlns:spring="http://www.springframework.org/tags" 
xmlns:tiles="http://tiles.apache.org/tags-tiles"
xmlns:util="urn:jsptagdir:/WEB-INF/tags/util" version="2.0">
  <jsp:directive.page contentType="text/html;charset=UTF-8"/>
  <jsp:output omit-xml-declaration="yes"/>	
   
    
    
    <div id="impact-description-div">
    
    <p>Aeris ratings are not based on a normative curve. Given the unique mission and operating 
    environment in which a CDFI works, it is not rated in comparison to other CDFIs. 
    Instead each institution is rated based on how it compares with the following ratings descriptions.</p>
    
    <p>
    <table>
    <tr><th>Rating</th><th>Description</th></tr>
    
    <tr><td>★★★★</td><td>A CDFI in this group has clear alignment of mission, strategies, 
    activities, and data that guides its programs and planning. The CDFI presents data 
    that clearly indicate that it is using its resources effectively to benefit disadvantaged 
    people and communities and achieve positive impacts related to its mission. It has 
    processes and systems that track output and outcome data on an ongoing basis, and 
    it can provide data showing positive changes in the communities or populations being 
    served. This CDFI uses its data on an ongoing basis to adjust strategies and activities 
    in line with its desired impact.</td></tr>
    <tr><td>★★★</td><td>A CDFI in this group has clear alignment of mission, strategies, 
    activities and data that guides its programs and planning. It accurately tracks 
    appropriate output data that indicate that it is using its resources effectively to 
    benefit its target populations or communities in line with its mission. The CDFI uses 
    its data on an ongoing basis to adjust strategies and activities in accordance with its 
    desired impact. It may track a limited number of impact indicators as well, but impact 
    data tracking may not be rigorous or consistent.</td></tr>
    <tr><td>★★</td><td>A CDFI in this group has reasonable strategies and activities given 
    its mission. It tracks basic output data that indicate fairly effective use of its 
    resources to benefit its target populations or communities in line with its mission.</td></tr>
    <tr><td>★</td><td>A CDFI in this group may lack alignment of its mission, strategies, 
    activities and data. Either it lacks output and longer term outcome data to form an opinion 
    of its impact, or the output and outcome data are unsatisfactory. It may also have a history of 
    underutilizing its resources to serve its target populations or communities. Board and management 
    provide limited oversight of impact strategies and activities.</td></tr>
     
    </table>

    </p>
    
    <p><b>Policy Plus</b></p>
    
    <p>
    Policy change is an integral part of this CDFI’s strategies. The CDFI leads initiatives 
    to change government policy to benefit the community development finance industry or disadvantaged 
    people and communities. The CDFI can provide evidence of its leadership role in recent policy 
    changes that produced benefits beyond additional resources for the CDFI itself, and management 
    can clearly articulate the CDFI’s leadership role in current policy activities.
    </p>  
    </div>
    
    
</div>
