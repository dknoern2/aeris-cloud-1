package com.cars.platform.web;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.DocumentTypeCategory;
import com.cars.platform.domain.DocumentTypeCompany;
import com.cars.platform.domain.DocumentTypeFolder;
import com.cars.platform.domain.DocumentTypeFormat;
import com.cars.platform.domain.DocumentTypeSubFolder;
import com.cars.platform.domain.ReportType;
import com.cars.platform.util.UserHelper;

@RequestMapping("/documenttypes")
@Controller
public class DocumentTypeController {
	
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid DocumentType documentType, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, documentType);
            return "documenttypes/create";
        }
        uiModel.asMap().clear();
        
        if (documentType.getDocumentTypeCategory() == null) {
        	DocumentTypeCategory docTypeCategoryForMigration = DocumentTypeCategory.getCategoryTypeForLibraryUpdatesMigration();
        	documentType.setDocumentTypeCategory(docTypeCategoryForMigration);
        }
        documentType.persist();
        return "redirect:/documenttypes";
    }
    

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(@RequestParam(value = "folder", required = false) Long folderId,
    		@RequestParam(value = "name", required = false) String name,
    		@RequestParam(value = "quarterly", required = false) Boolean quarterly,
    		@RequestParam(value = "annual", required = false) Boolean annual,
    		@RequestParam(value = "full", required = false) Boolean full,
    		@RequestParam(value = "annualIfAvailable", required = false) Boolean annualIfAvailable,
    		@RequestParam(value = "oneTime", required = false) Boolean oneTime,
    		@RequestParam(value = "documentTypeFormat", required = false) String documentTypeFormat,
    		@RequestParam(value = "reportType", required = false) String reportType,
    		@RequestParam(value = "description", required = false) String description,
    		Model uiModel) {
    	DocumentType docType = new DocumentType();
    	if (name != null) {
    		docType.setName(name);
    	}
    	if (quarterly != null) {
    		docType.setQuarterly(quarterly);
    	}
    	if (annual != null) {
    		docType.setAnnual(annual);
    	}
    	if (full != null) {
    		docType.setFull(full);
    	}
    	if(annualIfAvailable != null) {
    		docType.setAnnualIfAvailable(annualIfAvailable);
    	}
    	if (oneTime != null) {
    		docType.setOneTime(oneTime);
    	}
    	if (documentTypeFormat != null) {
    		DocumentTypeFormat docTypeFormat = DocumentTypeFormat.valueOf(documentTypeFormat);
    		docType.setDocumentTypeFormat(docTypeFormat);
    	}
    	if (reportType != null) {
    		ReportType repType = ReportType.valueOf(reportType);
    		docType.setReportType(repType);
    	}
    	if (description != null) {
    		docType.setDescription(description);
    	}
    	
    	DocumentTypeFolder docTypeFolder = null;
    	if (folderId != null) {
    		docTypeFolder = DocumentTypeFolder.findDocumentTypeFolder(folderId);
    	} else {
    		List<DocumentTypeFolder> documentTypeFolders = DocumentTypeFolder.findAllDocumentTypeFolders();
    		if (documentTypeFolders.size() > 0) {
    			docTypeFolder = documentTypeFolders.get(0);
    		}
    	}
    	docType.setDocumentTypeFolder(docTypeFolder);
    	
        populateEditForm(uiModel, docType);
        DashboardController.addQuicklinksContent(uiModel,UserHelper.getCurrentUser());
        return "documenttypes/create";
    }
    
    
    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "companyId", required = false) Long companyId, Model uiModel) {
    	
    	Company company = null;
    	if (null != companyId) {
    		company = Company.findCompany(companyId);
    		uiModel.addAttribute("company", company);
    	}
    	
        uiModel.addAttribute("documenttypes", DocumentType.findAllDocumentTypesSorted(company));
        
        DashboardController.addQuicklinksContent(uiModel,UserHelper.getCurrentUser());

        return "documenttypes/list";
    }


    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid DocumentType documentType, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, documentType);
            return "documenttypes/update";
        }
        uiModel.asMap().clear();
        
        if (documentType.getDocumentTypeCategory() == null) {
        	DocumentType originalDocumentType = DocumentType.findDocumentType(documentType.getId());
        	documentType.setDocumentTypeCategory(originalDocumentType.getDocumentTypeCategory());
        }
        documentType.merge();
        return "redirect:/documenttypes/";
    }
    
    
    /*
     * Update the Company overrides
     */
    @RequestMapping(value = "/company", method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid DocumentTypeCompany documentTypeCompany, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
	        uiModel.addAttribute("documentTypeCompany", documentTypeCompany);
            return "documenttypes/updatecompany";
        }
        uiModel.asMap().clear();
        documentTypeCompany.merge();
        return "redirect:/documenttypes?companyId=" + documentTypeCompany.getCompany().getId();
    }


    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(
    		@PathVariable("id") Long id,
    		@RequestParam(value = "company", required = false) Long companyId,
    		@RequestParam(value = "folder", required = false) Long folderId,
    		@RequestParam(value = "name", required = false) String name,
    		@RequestParam(value = "quarterly", required = false) Boolean quarterly,
    		@RequestParam(value = "annual", required = false) Boolean annual,
    		@RequestParam(value = "full", required = false) Boolean full,
    		@RequestParam(value = "annualIfAvailable", required = false) Boolean annualIfAvailable,
    		@RequestParam(value = "oneTime", required = false) Boolean oneTime,
    		@RequestParam(value = "documentTypeFormat", required = false) String documentTypeFormat,
    		@RequestParam(value = "reportType", required = false) String reportType,
    		@RequestParam(value = "description", required = false) String description,
    		Model uiModel)
    {
    	DocumentType docType = DocumentType.findDocumentType(id);
    	
    	// If we are updating for a company update only the request times (DocumentTypeDocument), creating an override if one does not exist
    	if (null != companyId) {
    		Company company = Company.findCompany(companyId);
    		DocumentTypeCompany dtc = DocumentTypeCompany.findForTypeAndCompany(docType, company);
    		if (null == dtc) {
    			dtc = new DocumentTypeCompany(docType, company);
    			dtc.persist();
    		}
	        uiModel.addAttribute("documentTypeCompany", dtc);
	        uiModel.addAttribute("company", company);
	        uiModel.addAttribute("companys", Arrays.asList(company));
	        uiModel.addAttribute("documentTypes", Arrays.asList(docType));
	        DashboardController.addQuicklinksContent(uiModel,UserHelper.getCurrentUser());
	        return "documenttypes/updatecompany";
    	}
    	
    	if (name != null) {
    		docType.setName(name);
    	}
    	if (quarterly != null) {
    		docType.setQuarterly(quarterly);
    	}
    	if (annual != null) {
    		docType.setAnnual(annual);
    	}
    	if (full != null) {
    		docType.setFull(full);
    	}
    	if(annualIfAvailable != null) {
    		docType.setAnnualIfAvailable(annualIfAvailable);
    	}
    	if (oneTime != null) {
    		docType.setOneTime(oneTime);
    	}
    	if (folderId != null) {
    		DocumentTypeFolder docTypeFolder = DocumentTypeFolder.findDocumentTypeFolder(folderId);
    		docType.setDocumentTypeFolder(docTypeFolder);
    	}
    	if (documentTypeFormat != null) {
    		DocumentTypeFormat docTypeFormat = DocumentTypeFormat.valueOf(documentTypeFormat);
    		docType.setDocumentTypeFormat(docTypeFormat);
    	}
    	if (reportType != null) {
    		ReportType repType = ReportType.valueOf(reportType);
    		docType.setReportType(repType);
    	}
    	if (description != null) {
    		docType.setDescription(description);
    	}
        populateEditForm(uiModel, docType);
        DashboardController.addQuicklinksContent(uiModel,UserHelper.getCurrentUser());
        return "documenttypes/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {

    	try{

    	DocumentType documentType = DocumentType.findDocumentType(id);
        documentType.remove();
        uiModel.asMap().clear();
    	}catch(Exception e){
    		uiModel.addAttribute("error","unable to delete, all documents of this type must be deleted first");
    	}
        return "redirect:/documenttypes";
    }

    void populateEditForm(Model uiModel, DocumentType documentType) {
        uiModel.addAttribute("documentType", documentType);
        uiModel.addAttribute("documenttypecategorys", DocumentTypeCategory.findAllDocumentTypeCategorys());
        uiModel.addAttribute("documenttypefolders", DocumentTypeFolder.findAllDocumentTypeFolders());
        uiModel.addAttribute("documenttypesubfolders", DocumentTypeSubFolder.findSubFoldersByFolder(documentType.getDocumentTypeFolder()));
        uiModel.addAttribute("documenttypeformats", DocumentType.findAllDocumentTypeFormats());
        uiModel.addAttribute("reportTypes", DocumentType.findAllReportTypes());
    }

    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
	
}
