package com.cars.platform.web;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.util.Log;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.PeerGroup;
import com.cars.platform.domain.PeerGroupAccess;
import com.cars.platform.domain.PeerGroupFilter;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.service.PeerGroupReportService;
import com.cars.platform.util.AuthorizationHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.PeerGroupPOJO;
import com.cars.platform.viewmodels.PeerPOJO;
import com.cars.platform.viewmodels.peergroup.PeerMetrics;
import com.cars.platform.viewmodels.peergroup.Report;

import flexjson.JSONDeserializer;

@RequestMapping("/peergroups")
@Controller
public class PeerGroupController {
		          
    /**
     * REST GET method returns all PeerGroupsPOJOs for the provided company, filtered by those with peers containing the cdfi
     * @param companyId the company to return the PeerGroups for, can be null or 0 in which case it returns the global instances
     * @param cdfiId optional Id of the CDFI for which the peer groups must contain as a peer.
     * @return An array of PeerGroupPOJOs associated to the company along with the global ones. Sorted by date last updated
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<PeerGroupPOJO> getForCompany(
    		@RequestParam(value = "companyId", required = false) Long companyId,
    		@RequestParam(value = "cdfiId", required = false) Long cdfiId
    	)
    {	
    	Company company = Company.findCompany(companyId);
    	Company cdfi = Company.findCompany(cdfiId);
    	List<PeerGroup> peerGroups = PeerGroup.findPeerGroupsByCompanyAndCdfi(company, cdfi);
    	if (null != company && (null == peerGroups || peerGroups.size() == 0)) {
    		peerGroups = PeerGroup.cloneGlobal(company, cdfi);
    	}
    	
    	List<PeerGroupPOJO> pojos = new ArrayList<PeerGroupPOJO>();
    	if (null != peerGroups) {
    		try {
        	for (PeerGroup peerGroup : peerGroups) {
        		pojos.add(new PeerGroupPOJO(peerGroup));
        	}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        }
    	return pojos;
    }
    
    
    /**
     * REST GET method for retrieving the PeerGroupPOJO with the given id
     * @param id is the id of the peer group to return
     * @return a PeerGroupPOJO if found otherwise null
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    PeerGroupPOJO get(@PathVariable("id") Long id) {
        PeerGroup peerGroup = PeerGroup.findPeerGroup(id);
        if (null == peerGroup) {
        	return null;
        }

        return new PeerGroupPOJO(peerGroup);
    }
    

    /**
     * REST POST method for creating a PeerGroup
     * @param pojo
     * @return the ID of the created PeerGroup
     */
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Long create (@RequestBody PeerGroupPOJO pojo) {
		PeerGroup peerGroup = new PeerGroup();
		peerGroup.setName(pojo.getName());
		peerGroup.setCreated(new Date());
		peerGroup.setUpdated(new Date());
		
		Company company = Company.findCompany(pojo.getCompanyId());
		peerGroup.setCompany(company);
		
		Company cdfi = Company.findCompany(pojo.getCdfiId());
		peerGroup.setCdfi(cdfi);
		
		PeerGroupFilter[] newFilters = pojo.getFilters();
		if (null != newFilters) {
			peerGroup.setFilters(Arrays.asList(newFilters));
		}
		setFilterReferences(peerGroup);
		
		List<Company> peers = new ArrayList<Company>();
		for (PeerPOJO peerPojo : pojo.getPeers()) {
			Company peer = Company.findCompany(peerPojo.getId());
			if (peer != null) {
				peers.add(peer);
			}
		}
		peerGroup.setPeers(peers);
		
		if (pojo.isTransitory()) {
			peerGroup.setTransitory(true);
			if (null != pojo.getOriginalId()) {
				PeerGroup original = PeerGroup.findPeerGroup(pojo.getOriginalId());
				peerGroup.setOriginal(original);			
			}
		}
		
		peerGroup.persist();
		
		return peerGroup.getId();
	}

	
	/**
	 * REST PUT method for updating a PeerGroup
	 * @param id
	 * @param pojo
	 * @return the Id of the updated PeerGroup
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    Long update(
    		@PathVariable("id") Long id,
       		@RequestBody PeerGroupPOJO pojo)
	{
		PeerGroup peerGroup = PeerGroup.findPeerGroup(id);
		
        if (null == peerGroup) {
        	throw new IllegalArgumentException("No PeerGroup with ID: " + id);
        }
        
        boolean nameChanged = peerGroup.getName() != pojo.getName();
		peerGroup.setName(pojo.getName());
		peerGroup.setUpdated(new Date());
		
		// these can't be updated
//		Company company = Company.findCompany(pojo.getCompanyId());
//		peerGroup.setCompany(company);
//		Company cdfi = Company.findCompany(pojo.getCdfiId());
//		peerGroup.setCdfi(cdfi);
		
		List<Company> peers = new ArrayList<Company>();
		for (PeerPOJO peerPojo : pojo.getPeers()) {
			Company peer = Company.findCompany(peerPojo.getId());
			if (peer != null) {
				peers.add(peer);
			}
		}
		peerGroup.setPeers(peers);
		
        List<PeerGroupFilter> oldFilters = peerGroup.getFilters();
        if (null == oldFilters) {
        	peerGroup.setFilters(Arrays.asList(pojo.getFilters()));
        }
        else {
            oldFilters.clear();
            PeerGroupFilter[] newFilters = pojo.getFilters();
            if (null != newFilters) {
            	oldFilters.addAll(Arrays.asList(newFilters));
            }
        }
		setFilterReferences(peerGroup);
		
		PeerGroup original = null;
		if (peerGroup.isTransitory()) {
			if (!pojo.isTransitory()) {
				peerGroup.setTransitory(false);
				original = peerGroup.getOriginal();
				if (null != original) {
					peerGroup.setOriginal(null);
				}
				
				if (peerGroup.getCdfi() == null && peerGroup.getCompany() == null) {
					List<Company> subscriersWithAllPeerGroupSubscription = Company.findSubscriersWithAllPeerGroupSubscription();
			    	for (Company subscriber : subscriersWithAllPeerGroupSubscription) {
			    		List<PeerGroup> peerGroups = PeerGroup.findPeerGroupsByCompanyAndCdfi(subscriber, null);
			        	if (null == peerGroups || peerGroups.size() == 0) {
			        		PeerGroup.cloneGlobal(subscriber, null);
			        	}
			        	try {
			        		PeerGroup copiedPeerGroup = PeerGroup.findPeerGroupCopiedFrom(original, subscriber);
			        		if (copiedPeerGroup != null) {
				        		copiedPeerGroup.remove();
				        	}
			        	} catch (Exception ignore) {}
			        	peerGroup.cloneForCompany(subscriber);
			    	}
				}
			}
		} 
		else if (peerGroup.getId().equals(pojo.getId()) && nameChanged) {
			List<Company> subscriersWithAllPeerGroupSubscription = Company.findSubscriersWithAllPeerGroupSubscription();
	    	for (Company subscriber : subscriersWithAllPeerGroupSubscription) {
	        	try {
	        		PeerGroup copiedPeerGroup = PeerGroup.findPeerGroupCopiedFrom(peerGroup, subscriber);
	        		if (copiedPeerGroup != null) {
		        		copiedPeerGroup.setName(peerGroup.getName());
		        		copiedPeerGroup.persist();
		        	}
	        	} catch (Exception ignore) {}
	    	}
		}
		else if (pojo.isTransitory()) {
			throw new IllegalArgumentException("Can not update a non transitory peer group to transitory");
		}
         
        peerGroup.merge();
        
        // have to remove the original after the peer group saved
        // using orphanRemoval on the relationship didn't work as original got deleted when the transitory one did
        if (null!=original) {
		    original.remove(); // this will remove previous, 'zombie' transitory references to this object
        }

		return peerGroup.getId();
	}
	
	
	/**
	 * REST DELETE method for deleting a PeerGroup
	 * @param id
	 * @return the ID of the deleted PeerGroup
	 */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody
    Long delete(@PathVariable("id") Long id) {
        PeerGroup peerGroup = PeerGroup.findPeerGroup(id);
        if (null == peerGroup) {
        	throw new IllegalArgumentException("No PeerGroup with ID: " + id);
        }
        
        peerGroup.remove();
        return id;
    }

    
    /**
     * REST Get method returning the peer group report data for the provided peer group
     * @param id of the peer group
     * @return peergroup.Report as JSON
     */
    @RequestMapping(value = "/{id}/report", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Report getReportData(@PathVariable long id, @RequestParam( value = "showIncompleteYears", required = false) Boolean showIncomplete) {
    	return getReportData(id, null, showIncomplete);
    };

   
    /**
     * REST Get method returning the peer group report data for the provided peer group and cdfi
     * Note that now the cdfi ID is ignored as the peer group has a separate attribute for the CDFI being compared.
     * changing the jspx and js files to not sent the cdfiId was too brittle so this was left in tact.
     * @param id of the peer group
     * @param cdfiId the cdfi to compare against, this value is now ignored
     * @return peergroup.Report as JSON
     */
     @RequestMapping(value = "/{id}/report/{cdfiId}", method = RequestMethod.GET, produces = "application/json")
     public @ResponseBody
     Report getReportData(@PathVariable long id, @PathVariable Long cdfiId, @RequestParam( value = "showIncompleteYears", required = false) Boolean showIncomplete)
     {
        if (showIncomplete == null) {
        	showIncomplete = false;
        }
    	 
    	 PeerGroup peerGroup = PeerGroup.findPeerGroup(id);
    
         
/*  Originally the cdfi had to be one of the peers. This was changed to have the cdfi, if it existed,
 *  as a separate attribute. This allowed for CDFI specific peer groups     
         Company cdfi = null;
         if (null != cdfiId) {
         	cdfi = Company.findCompany(cdfiId);
         	if (null == cdfi) {
 	        	throw new IllegalArgumentException("No CDFI found for ID: " + cdfiId);
         	}
         	if (!cdfi.isCDFI()) {
 	        	throw new IllegalArgumentException("The company with ID: " + cdfiId + " is not a CDFI");       		
         	}
         
 	        if (!peerGroup.getPeers().contains(cdfi)) {
 	        	throw new IllegalArgumentException("The cdfi [" + cdfi.getName() + "] is not a member of peer group [" + peerGroup.getName() + "]");
 	        }
         }
*/         
         PeerGroupReportService pgrs = new PeerGroupReportService(peerGroup, showIncomplete);
         
         Report report =  pgrs.getReportData();

         return report;
     }
     
     
     /**
      * Returns the peer metrics (formula) values for specified equationId
      * @param id
      * @param equationId
      * @return
      */
      @RequestMapping(value = "/{id}/peerMetric/{equationId}", method = RequestMethod.GET, produces = "application/json")
      public @ResponseBody
      PeerMetrics getPeerMetricData(@PathVariable long id, @PathVariable Long equationId, @RequestParam(value="showIncomplete", required = false) boolean showIncomplete)
      {
          PeerGroup peerGroup = PeerGroup.findPeerGroup(id);
          Equation equation = Equation.findEquation(equationId);
             
          PeerGroupReportService pgrs = new PeerGroupReportService(peerGroup, showIncomplete);
          
          PeerMetrics peerMetrics =  pgrs.getPeerMetric(equation);

          return peerMetrics;
      }
      
      
     // from JSON serialization the filters don't have their reference to their PeerGroup set
     private void setFilterReferences(PeerGroup peerGroup) {
    	 if (null == peerGroup.getFilters()) {
    		 return;
    	 }
    	 for (PeerGroupFilter filter : peerGroup.getFilters()) {
    		 filter.setPeerGroup(peerGroup);
    	 }
     } 

     @RequestMapping(value = "/report/{id}")
     public String showPeergroupReport(@PathVariable Long id, Model model, HttpServletRequest request, HttpServletResponse response) {   	
     	model.addAttribute("pgId", id);
 	    return "peergroups/report";
     }  
     
     @RequestMapping(value = "/list", produces = "text/html")
     public String list(Model uiModel) {
    	 return populateForPeerGroup(uiModel, "peergroups/list");
     }
     
  // peer group selector without a peer group in context... that is Create new peer group
 	@RequestMapping(value = "/peergroupselector")
 	public String peergroupselector(Model model,
 			HttpServletRequest request, HttpServletResponse response)
 	{
 		return populateForPeerGroup(model, "peergroups/globalSelector");
 	}
 	
 	// peer group selector with a peer group in context. That is go into selector with an existing peer group	
 	@RequestMapping(value = "/peergroupselector/{pgId}")
 	public String peergroupselector(@PathVariable("pgId") Long pgId, Model model,
 			HttpServletRequest request, HttpServletResponse response)
 	{
 	   	model.addAttribute("pgId", pgId);
 		return populateForPeerGroup(model, "peergroups/globalSelector");
 	}
 	
 	private String populateForPeerGroup(Model model, String destinationPath) {
		Person person = UserHelper.getCurrentUser();
		
		model.addAttribute("company", null);
		model.addAttribute("person", person);
		
		// The CDFI selector uses the fact that an active full subscription exists
		// in order to know whether or not to include 'add to garage' to the menu options
		Subscription activeSubscriptionOfTypeAll = null;
		for (Subscription sub : Subscription.findActiveOwnedSubscriptionsForSubscriber(person.getCompany())) {
			if (sub.getSubscriptionType() == SubscriptionType.ALL) {
				activeSubscriptionOfTypeAll = sub;
				break;
			}
		}
		model.addAttribute("activeSubscriptionOfTypeAll", activeSubscriptionOfTypeAll == null ? null : activeSubscriptionOfTypeAll.getId());

		
		PeerGroupAccess peerGroupAccess;
		if (person.getCompany().isCARS()) {
			if (person.isAdmin()) {
				peerGroupAccess = PeerGroupAccess.FULL;
			}
			else {
				peerGroupAccess = PeerGroupAccess.NONE;
			}
		}
		else if (person.getCompany().isCDFI()) {
			peerGroupAccess = PeerGroupAccess.NONE; // should be unreachable
		}
		else { // Investor should be unreachable
			if (null != activeSubscriptionOfTypeAll && activeSubscriptionOfTypeAll.isPeerGroups()) {
				peerGroupAccess = PeerGroupAccess.FULL;				
			}
			else {
				peerGroupAccess = PeerGroupAccess.NONE;								
			}
		}
		model.addAttribute("peerGroupAccess", peerGroupAccess);
		
		return destinationPath;
	}
 	
 
 	
 	// If the create and update methods fail to get called its probably due to serialization issues with
 	// PeerGroupPOJO. You can comment those out and uncomment these to debug
 	
/*@RequestMapping(method = RequestMethod.POST, produces = "text/html")
public @ResponseBody
String create(
   		@RequestBody String pojo)
	{
		System.out.println("Received: " + pojo);
		
		JSONDeserializer<PeerGroupPOJO> d = new JSONDeserializer<PeerGroupPOJO>();
		try {
		    PeerGroupPOJO pojo2 = d.deserialize(pojo, PeerGroupPOJO.class);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return "OK";
	}
*/
 	
/* 	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "text/html")
 	public @ResponseBody
    String update(
    		@PathVariable("id") Long id,
       		@RequestBody String pojo)
	{

 			System.out.println("Received: " + pojo);
 			
 			JSONDeserializer<PeerGroupPOJO> d = new JSONDeserializer<PeerGroupPOJO>();
 			PeerGroupPOJO pojo2 = null;
 			try {
 			    pojo2 = d.deserialize(pojo, PeerGroupPOJO.class);
 			}
 			catch (Exception e) {
 				System.out.println(e.getMessage());
 			}
 			System.out.println(pojo2.toString());
 			return "OK";
 		}
*/


}
