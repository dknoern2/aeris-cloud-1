package com.cars.platform.web;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.util.UserHelper;

@RequestMapping("/profile/**")
@Controller
public class ProfileController {

	@RequestMapping(value = "/profile/{id}", method = RequestMethod.GET)
	public String index(@PathVariable Long id, Model model) {

		Person me = UserHelper.getCurrentUser();
		if(me.getId()!=id && me.getPersonRole()!=PersonRole.ADMIN){
			return "resourceNotFound";
		}
		
		Person person = Person.findPerson(id);
		model.addAttribute("profile", person);
		model.addAttribute("person", me);
		populateEditForm(model);
		
 		return "profile/index";
	}
	
	@RequestMapping(value = "/profile/mine", method = RequestMethod.GET)
	public String myprofile(Model model) {

		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", person);
		model.addAttribute("profile", person);
		
        populateEditForm(model);
		return "profile/index";
	}	
	
	
	@RequestMapping(method = RequestMethod.PUT)
	public String update(@Valid Person person, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest) {

		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("person", person);
			populateEditForm(uiModel);
			return "profile";
		}

		// I don't think you should have to do this, but I could
		// not get binding to work unless all the parameters were on the form.
		// need to research more.

		Person p = Person.findPerson(person.getId());
			
		// if username change is attempted, make sure other user with this name does not exist.
		if(!p.getUsername().equals(person.getUsername())){			
			if (Person.isUsernameInUse(person.getUsername())) {
				uiModel.addAttribute("error","user with username \"" + person.getUsername()
									+ "\" already exists.");
				uiModel.addAttribute("person", person);			
				populateEditForm(uiModel);
				return "profile/create";
			}
		}
		System.out.println(person.toString());
		p.setEnabled(person.isEnabled());
		p.setUsername(person.getUsername());
		p.setFirstName(person.getFirstName());
		p.setLastName(person.getLastName());
		p.setTitle(person.getTitle());
		p.setPassword(person.getPassword());
		p.setEmail(person.getEmail());
		p.setEmailRemindersEnabled(person.isEmailRemindersEnabled());
		p.setEmailUploadRemindersEnabled(person.isEmailUploadRemindersEnabled());
		p.setNewFinancialRemindersEnabled(person.isNewFinancialRemindersEnabled());
		p.setPublicContact(person.isPublicContact());
	
		p.setPhone(person.getPhone());
		if(CompanyType.CARS.equals(p.getCompany().getCompanyType())){
			p.setPersonRole(person.getPersonRole());
		}
		
		uiModel.asMap().clear();
		p.merge();
		
		if(p.getCompany().getCompanyType()==CompanyType.CDFI){
			return "redirect:/cdfi/"+p.getCompany().getId()+"/overview";
		}else if(p.getCompany().getCompanyType()==CompanyType.INVESTOR){
			return "redirect:/subscriber/"+p.getCompany().getId();
		}else if(p.getCompany().getCompanyType()==CompanyType.CARS){
			return "redirect:/usermanagement/";
		}
			
		// if it's your own profile, return to dashboard
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(@Valid Person person, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest) {
		
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("profile", person);
			// populateEditForm(uiModel, person);
			return "profile";
		}
		// check for existing user with same name
		if (Person.isUsernameInUse(person.getUsername())) {

			uiModel.addAttribute("error",
					"user with username \"" + person.getUsername()
							+ "\" already exists.");
			uiModel.addAttribute("profile", person);			
			
            populateEditForm(uiModel);
			return "profile/create";
		}
				
		//String password = RandomHelper.randomString(6);
		System.out.println(person.toString());
		
		Person p = new Person();
		p.setEnabled(person.isEnabled());
		p.setUsername(person.getUsername());
		p.setPassword(person.getPassword());
		p.setFirstName(person.getFirstName());
		p.setLastName(person.getLastName());
		p.setTitle(person.getTitle());
		p.setEmail(person.getEmail());
		p.setEmailRemindersEnabled(person.isEmailRemindersEnabled());
		p.setEmailUploadRemindersEnabled(person.isEmailUploadRemindersEnabled());
		p.setNewFinancialRemindersEnabled(person.isNewFinancialRemindersEnabled());
		p.setPublicContact(person.isPublicContact());
		p.setPhone(person.getPhone());
		p.setCompany(person.getCompany());
		
		if(CompanyType.CARS.equals(p.getCompany().getCompanyType())){
			p.setPersonRole(person.getPersonRole());
		}		
		
		
		uiModel.asMap().clear();
		p.merge();
		
		if(p.getCompany().getCompanyType()==CompanyType.CDFI){
			return "redirect:/cdfi/"+p.getCompany().getId()+"/overview";
		}else if(p.getCompany().getCompanyType()==CompanyType.INVESTOR){
			return "redirect:/subscriber/"+p.getCompany().getId();
		}else if(p.getCompany().getCompanyType()==CompanyType.CARS){
		    return "redirect:/usermanagement/";
		}
			
		return "redirect:/";
	}
	
	
    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(@RequestParam(value = "companyId", required = false) Long companyId,@RequestParam(value = "role", required = false) String role,Model uiModel) {
 
    	Person person = new Person();
    	
    	uiModel.addAttribute("profile", person);
    	uiModel.addAttribute("person", UserHelper.getCurrentUser());
        
        if(companyId!=null){
        	Company company = Company.findCompany(companyId);
        	person.setCompany(company);
        	
        	if(CompanyType.CARS.equals(company.getCompanyType())){
        		if("admin".equals(role)){
        			person.setPersonRole(PersonRole.ADMIN);
        		}else if("basic".equals(role)){
        			person.setPersonRole(PersonRole.BASIC);
        		}
        		
        		populateEditForm(uiModel);		
        	}
        }
        
   
        uiModel.addAttribute("companys",Company.findAllCompanys());

        return "profile/create";
    }  	
	
	
	@RequestMapping(value = "/profile/delete/{id}")
	public String delete(@PathVariable Long id, Model model) {
		Person person = Person.findPerson(id);
		
		String username = person.getUsername();
		Company company = person.getCompany();
		
		try{		
		    person.remove();
		    model.addAttribute("notice", "User " + username + " deleted");		
		
		}catch(Exception e){
			
			String errorString = "Unable to delete profile";

			if (e.getMessage().contains("ConstraintViolationException")){
				errorString += " due to related data that requires this record";
			}
			
			model.addAttribute("error", errorString);		
		}

		if(company.getCompanyType()==CompanyType.CDFI){
			return "redirect:/cdfi/"+company.getId()+"/overview";
		}else if(company.getCompanyType()==CompanyType.INVESTOR){
			return "redirect:/subscriber/"+company.getId();
		}else if(company.getCompanyType()==CompanyType.CARS){
			return "redirect:/usermanagement";
		}
		
		return  "redirect:/";
	}	
	
	private void populateEditForm(Model model){
        model.addAttribute("personroles", Arrays.asList(PersonRole.values()));
        model.addAttribute("companys",Company.findAllCompanys());
	}
}
