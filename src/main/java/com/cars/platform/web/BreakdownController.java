package com.cars.platform.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.service.graph.breakdown.BreakdownGraphService;
import com.cars.platform.viewmodels.graph.Graph;


@RequestMapping("/breakdowns")
@Controller
public class BreakdownController {
    private static final Logger logger = LoggerFactory.getLogger(BreakdownController.class);
    
    // REST service which provides data for the breakdown graphs for the provided company
    @RequestMapping(value = "/{companyId}")
    public @ResponseBody List<Graph> report(@PathVariable Long companyId, Model model,
	    HttpServletRequest request, HttpServletResponse response) {

    	Company company = Company.findCompany(companyId);
    	
    	BreakdownGraphService gs = new BreakdownGraphService(company);
    	
    	List<Graph> graphs = gs.getBreakdownGraphs(true, true);

 	    return graphs;
    }
    
}
