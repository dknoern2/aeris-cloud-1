package com.cars.platform.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cars.platform.domain.Document;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.util.SpringApplicationContext;

public class ImageViewServlet extends HttpServlet {

    private DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
    
	// This method is called by the servlet container to process a GET request.
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	    // Get the absolute path of the image
	    //ServletContext sc = getServletContext();
	    //String filename = sc.getRealPath("image.gif");

	    // Get the MIME type of the image
	    //String mimeType = sc.getMimeType(filename);
	    //if (mimeType == null) {
	    //    sc.log("Could not get MIME type of "+filename);
	    //    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    //    return;
	    //}

	    // Set content type
		
	
	    //resp.setContentType(mimeType);

		long documentId = Long.parseLong(req.getParameter("id"));
		
		Document doc  = Document.findDocument(documentId);
		
		if(doc.getFileName().endsWith("png")){
			resp.setContentType("image/png");
		}
		
		
		

	    OutputStream out = resp.getOutputStream();


	    InputStream in = documentDataService.getDocumentData(doc.getCompany().getId(), doc.getId());


	    int totalLength=0;
	    byte[] buffer = new byte[1024];
	    int len;
	    while ((len = in.read(buffer)) != -1) {
	       out.write(buffer, 0, len);
	       totalLength+=len;
	    }
	            
        resp.setContentLength(totalLength);

	    out.close();
	}
	
}
