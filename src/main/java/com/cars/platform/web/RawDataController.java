package com.cars.platform.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.service.ValueFactLoader;
import com.cars.platform.service.ValueFactService;
import com.cars.platform.service.export.MetricReportService;
import com.cars.platform.service.export.MetricValuesService;
import com.cars.platform.util.SelectOptionHelper;

@RequestMapping("/rawdata")
@Controller
public class RawDataController {
	
	private static final Logger logger = LoggerFactory.getLogger(RawDataController.class);

	
	@Autowired
	ValueFactLoader valueFactLoader;
	  
        
    // TODO allow for query on status
    // see http://blog.inflinx.com/2012/09/09/spring-async-and-future-report-generation-example/
    @RequestMapping(value = "/etl")
    public String etlAll(Model model) {
    	
    	if (valueFactLoader.inProgress()) {
    		return index(model);
   	    }
    	else {
    		valueFactLoader.updateAllCompanies();
    	}
    	
    	model.addAttribute("progress", 0);	
	    return "rawdata/index";
    }

	
    @RequestMapping(produces = "text/html")
    public String index(Model model) {
     
        if (valueFactLoader.inProgress()) {
        	model.addAttribute("progress", valueFactLoader.progressPercent());
        }
        model.addAttribute("years", SelectOptionHelper.getSelectYears(11, 1));
        model.addAttribute("quarters", SelectOptionHelper.getSelectQuarters());
        return "rawdata/index";
    }

	
	/*
	 * Hidden method for loading the value facts for a single company
	 */
	@RequestMapping(value = "/{id}/etl")
	public @ResponseBody String etl(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		long start = System.currentTimeMillis();

		Company company = Company.findCompany(id);

		ValueFactService valueFactService = new ValueFactService(company);
		int factsCreated = valueFactService.updateCompany();

		return "[" + factsCreated + "] facts were created in [" + (System.currentTimeMillis()-start) + "] ms.";
	}
	
    
    
    /**
     * REST method for downloading a tear sheet. The xlsx and exportParams are for debugging only
     */
    @RequestMapping(value = "/metricsbycompany", method = RequestMethod.GET)
    public void getMetricsByCompany(
        HttpServletRequest request, HttpServletResponse response) {
    	
    	
    	String filename = "MetricReport.xls";
    	response.setContentType("application/vnd.ms-excel");
    	response.setHeader("Content-Disposition", "attachment; filename=" + filename);

    	
    	try {
        	MetricReportService mrs = new MetricReportService(response.getOutputStream());
   		    mrs.export();
	    	response.flushBuffer();
		} catch (Exception e) {
			e.printStackTrace();
			// rethrowing here gives the user an even worse experience than getting an empty file
			//throw new RuntimeException("Failed to generate tear sheet", e);
		}

    	
    }
    
    @RequestMapping(value = "/metricvaluesbycompany", method = RequestMethod.GET)
    public void getMetricValuesByCompany(
        HttpServletRequest request, HttpServletResponse response) {
    	
    	try {
    		int quarter = Integer.parseInt(request.getParameter("quarter"));
    		int year = Integer.parseInt(request.getParameter("year"));
    		boolean active = Boolean.parseBoolean(request.getParameter("active"));
    		boolean rated = Boolean.parseBoolean(request.getParameter("rated"));
    		
    		String filename = String.format("MetricValuesReport Q%d %d.xls", quarter, year);
        	response.setContentType("application/vnd.ms-excel");
        	response.setHeader("Content-Disposition", "attachment; filename=" + filename);
    	
        	MetricValuesService mvs = new MetricValuesService(response.getOutputStream());
        	mvs.export(quarter, year, active, rated);
	    	response.flushBuffer();
		} catch (Exception e) {
			e.printStackTrace();
			// rethrowing here gives the user an even worse experience than getting an empty file
			//throw new RuntimeException("Failed to generate tear sheet", e);
		}
    }	

}
