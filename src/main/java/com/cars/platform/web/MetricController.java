package com.cars.platform.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.service.FormulaService;
import com.cars.platform.viewmodels.EquationUnit;
import com.cars.platform.viewmodels.MetricBreakdownPOJO;
import com.cars.platform.viewmodels.MetricPOJO;
import com.cars.platform.viewmodels.Response;

@RequestMapping("/metric/**")
@Controller
public class MetricController {

	// TODO add edittability of breakdown for global metrics
	@RequestMapping(value = "/index/{companyId}", method = RequestMethod.GET)
	public String index(@PathVariable Long companyId, Model model,
			HttpServletRequest request) throws ServletRequestBindingException {

		model.addAttribute("companyId", companyId);

		if (companyId != null && companyId > 0) {
			model.addAttribute("company", Company.findCompany(companyId));
		}

		String accountCode = ServletRequestUtils.getStringParameter(request,
				"accountCode");

		if (accountCode == "" || accountCode == null) {
			accountCode = "-1";
		}
		model.addAttribute("accountCode", accountCode);

		return "metric/index";
	}

	@RequestMapping(value = "/create/{companyId}", method = RequestMethod.GET)
	public String create(@PathVariable Long companyId, Model model) {

		model.addAttribute("companyId", companyId);
		return "metric/create";
	}

	@RequestMapping(value = "/addMetric/{companyId}", method = RequestMethod.POST)
	public @ResponseBody
	Response addMetric(@RequestBody MetricPOJO metric, @PathVariable long companyId, Model model) {

		Company company = null;
		if (companyId != 0) {
			company = Company.findCompany(companyId);
		}

		int code = (Metric.findMaxAccountCode(metric.getParentId()) + 1);

		MetricCategory parent = MetricCategory.findMetricCategory(metric.getParentId());
		MetricBreakdown breakdown = MetricBreakdown.findMetricBreakdown(metric.getBreakdownId());

		Metric newMetric = new Metric();
		newMetric.setParent(parent);
		newMetric.setName(metric.getName());
		newMetric.setAccountCode(Integer.toString(code));
		newMetric.setCompany(company);
		newMetric.setType(metric.getType());
		newMetric.setBreakdown(breakdown);
		
		newMetric.merge();
		
		// just in case there are existing metrics that have the same accountcode
		List<Metric> temp = Metric.findMetricByFullAccountCode(newMetric.getFullAccountCode()).getResultList();
		for(Metric tempMetric : temp){
		    if(tempMetric.getRank() == 0){ // new metric don't have a rank
		    	tempMetric.setRank(tempMetric.getId());
			tempMetric.merge();
		    }
		}
		
		ReportDataCache.removeAll();
		

		return new Response("?accountCode=" + newMetric.getFullAccountCode());

	}

	@RequestMapping(value = "/getMetrics/{companyId}", method = RequestMethod.GET)
	public @ResponseBody
	List<MetricPOJO> getMetrics(@PathVariable Long companyId) {
		List<Metric> metrics;
		List<MetricPOJO> metricPOJOS = new ArrayList<MetricPOJO>();

		try {

			if (companyId == 0) {
				metrics = Metric.findCommonMetrics();
			} else {
			    	metrics = Metric.findAllCompanyMetrics(companyId);

				//metrics = Metric.findCompanyOnlyMetrics(companyId);
			}

			for (Metric metric : metrics) {

				MetricPOJO newMetric = new MetricPOJO();
				newMetric.setName(metric.getName());
				newMetric.setAccountCode(metric.getFullAccountCode());
				newMetric.setParentId(metric.getParent().getId());
				newMetric.setGrandParentId(metric.getParent().getParent().getId());
				newMetric.setRank(metric.getRank());
				newMetric.setBreakdownId(metric.getBreakdown() == null ? 0 : metric.getBreakdown().getId());
				
				if(metric.getCompany() != null && metric.getCompany().getId() != null){
				    newMetric.setCompanyId(metric.getCompany().getId());
				}
				
				MetricType metricType = MetricType.NUMERIC;
				if (metric.getType() == null) {
					metricType = MetricType.NUMERIC;
				} else {
					metricType = metric.getType();
				}

				newMetric.setType(metricType);
				newMetric.setId(metric.getId());
				metricPOJOS.add(newMetric);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return metricPOJOS;
	}

	@RequestMapping(value = "/getParentCategories", method = RequestMethod.GET)
	public @ResponseBody
	List<MetricCategory> getParentCategories() {

		List<MetricCategory> rootCategories = MetricCategory
				.findParentCategories();
		return rootCategories;
	}

	@RequestMapping(value = "/getChildCategories", method = RequestMethod.GET)
	public @ResponseBody
	List<MetricCategory> getAllChildCategories() {

		List<MetricCategory> childCategories = MetricCategory
				.findChildCategories();

		return childCategories;
	}
	
	
	@RequestMapping(value = "/getBreakdowns/{companyId}", method = RequestMethod.GET)
	public @ResponseBody
	List<MetricBreakdownPOJO> getBreakdowns(@PathVariable Long companyId) {
		
		MetricBreakdownPOJO nullBreakdown = new MetricBreakdownPOJO();
		nullBreakdown.setName("-- NONE --");
		nullBreakdown.setId(0);
		List<MetricBreakdownPOJO> pojos = new ArrayList<MetricBreakdownPOJO>();
		pojos.add(nullBreakdown);
		
        // only global metrics can have breakdowns
		if (companyId == 0) {
			List<MetricBreakdown> breakdowns = MetricBreakdown.findAllMetricBreakdowns();
			for (MetricBreakdown breakdown : breakdowns) {
				MetricBreakdownPOJO pojo = new MetricBreakdownPOJO(breakdown);
				pojos.add(pojo);
			}
		}

		return pojos;
	}


	@Transactional
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	public @ResponseBody
	Response remove(@RequestBody MetricPOJO metric, HttpServletResponse response) {
		List<Metric> metricList = null;
		
		try{
			metricList = Metric.findMetricByFullAccountCode(metric.getAccountCode()).getResultList();
			for(Metric m: metricList){
				if(m.getId() == metric.getId()){
					Metric.removeMetric(m);
					break;
				}				
			}
		}catch(Exception e){			
			try{
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.toString());
			}catch(Exception ee){}
		}finally{
			ReportDataCache.removeAll();
		}		
		return new Response("");
	}

	@RequestMapping(value = "/getFormulaTranslated", method = RequestMethod.POST)
	public @ResponseBody
	List<EquationUnit> getFormulaTranslated(@RequestBody String equation) throws Exception {
		 FormulaService formulaService = new FormulaService(1);
		 List<EquationUnit> equationUnits = formulaService.transformFormula(equation);
		 return equationUnits;
	}

   
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	Response save(@RequestBody MetricPOJO metric, HttpServletResponse response) {
		List<Metric> metricList =null;		
		try
		{
			metricList = Metric.findMetricByFullAccountCode(metric.getAccountCode()).getResultList();
			
			//go through list and find the corresponding metric and update its data
			for(Metric m: metricList){
				if(m.getId() == metric.getId())
				{
					m.setName(metric.getName());
					m.setType(metric.getType());
					m.setBreakdown(MetricBreakdown.findMetricBreakdown(metric.getBreakdownId()));
					m.merge();					
					break;
				}				
			}
		}catch(Exception e){
			try{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.toString());
			}catch(Exception ee){}
		}finally{
			ReportDataCache.removeAll();
		}		
		return new Response("");
	}

	@RequestMapping(value ="reorder", method= RequestMethod.POST)
	public @ResponseBody
	Response reorder(@RequestBody String rank,HttpServletResponse response){	    
	    Pattern p = Pattern.compile("\"\\{rank: \\[\\d+(,\\d+)*\\]\\}\"");	    
	    Matcher m =p.matcher(rank);
	    boolean clearCache = false;
	    
	    if(m.matches()){
		try{

		    //get custom rank of metrics
		    String[] sRank = rank.substring(rank.indexOf('[') + 1, rank.indexOf(']')).split(",");
		    ArrayList<Long> lRank = new ArrayList<Long>();
		    for(int i = 0; i < sRank.length; ++i){
			lRank.add(Long.parseLong(sRank[i]));		    
		    }
		
		    // get list of metrics in order of current rank
		    List<Metric> filteredMetrics = Metric.findMetricsByRank(lRank);
		
		    //update new ranks\
		    Collections.sort(lRank);
		    for(Metric metric:filteredMetrics){			
			for(int si = 0; si < sRank.length; si++){
			    if(metric.getRank() == Long.parseLong(sRank[si])){
				if(lRank.get(si) != metric.getRank()){
				    metric.setRank(lRank.get(si));
				    metric.merge();
				    if(!clearCache ){
					ReportDataCache.removeAll();
					clearCache = !clearCache;
				    }
				    break;
				}				
			    }			    
			}			
			
		    }
		}catch(Exception e){
		    try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (IOException e1) {
				// could not send response
			}
		}
		
	    }
	    
	    
	    return new Response("");
	}
}