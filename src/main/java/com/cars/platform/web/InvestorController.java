package com.cars.platform.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.DocumentTypeCategory;
import com.cars.platform.domain.DocumentTypeFolder;
import com.cars.platform.domain.LendingType;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.PeerGroup;
import com.cars.platform.domain.PeerGroupAccess;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.SectoralFocus;
import com.cars.platform.domain.ShareFinancials;
import com.cars.platform.domain.State;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.util.AuthorizationHelper;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.CDFISearch;
import com.cars.platform.viewmodels.CdfiPOJO;
import com.cars.platform.viewmodels.investor.CDFI;
import com.cars.platform.viewmodels.investor.Report;

@RequestMapping("/investor")
@Controller
public class InvestorController {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestorController.class);


	@RequestMapping(value = "/{id}")
	public String dashboard(@PathVariable("id") Long id, Model model) {

		Company investor = Company.findCompany(id);
    	Person person = authorizeCurrentUser(investor);
    	if (null == person) {
    		// TODO test this it may go into infinite loop
    		model.addAttribute("error", "You are not authorized to view the page requested.");
    		return "redirect:/";
    	}
    	
    	List<Subscription> activeSubscriptions = Subscription.findAllActiveSubscriptionsForSubscriber(investor);
    	
    	if (null != activeSubscriptions) {
    		for (Subscription subscription : activeSubscriptions) {
    			SubscriptionType type = subscription.getSubscriptionType();
    			if (type == SubscriptionType.LIBRARY && person.getAgreementDate() == null) {
    				return "investor/agreement";
    			}
    			if (type == SubscriptionType.SELECT && person.getAgreementDateSelect() == null) {
    				return "investor/selectagreement";
    			}
    		}
    	}
    	
    	Set<CDFI> cdfis = getCdfis(investor, activeSubscriptions, 60);
    	
    	model.addAttribute("company", investor);
    	model.addAttribute("cdfis", cdfis);
    	model.addAttribute("person", person);
    	
    	Subscription expiring = populateSubscriptions(model, Subscription.findActiveOwnedSubscriptionsForSubscriber(investor));
    	if (null == expiring) {
    		model.addAttribute("expireNumber", 0);
    		model.addAttribute("expireMessage", "Your subscription has expired");    		
    	}
    	else {
	    	int days = DateHelper.getDaysUntilDue(expiring.getExpirationDate());
	    	if (days > 60) {
	    		model.addAttribute("expireNumber", days/30);
	    		model.addAttribute("expireMessage", "Months left in your subscription");
	    	}
	    	else {
	    		model.addAttribute("expireNumber", days);
	    		if (days == 1) {
	    			model.addAttribute("expireMessage", "Day left in your subscription");
	    		}
	    		else {
	    			model.addAttribute("expireMessage", "Days left in your subscription");
	    			
	    		}
	    	}
    	}
    	
    	List<PeerGroup> peerGroups = PeerGroup.findPeerGroupsByCompanyAndCdfi(investor, null);
    	model.addAttribute("peerGroups", peerGroups);
    	
    	//model.addAttribute("reviewsPending", getPendingReviews());
    	
    	model.addAttribute("ratedCdfis", Company.findRatedCDFIs().size());
    	
    	model.addAttribute("newCDFIs", getNewCdfis(cdfis));

    	return "investor/index";
    }
	
	@RequestMapping(value = "/{id}/myreports")
	public String myReports(@PathVariable("id") Long id, Model model) {

		Company investor = Company.findCompany(id);
    	Person person = authorizeCurrentUser(investor);
    	if (null == person) {
    		model.addAttribute("error", "You are not authorized to view the page requested.");
    		return "redirect:/";
    	}
    	
    	List<Subscription> activeSubscriptions = Subscription.findAllActiveSubscriptionsForSubscriber(investor);
    	
    	
    	Set<CDFI> cdfis = getCdfis(investor, activeSubscriptions, null);
    	
    	model.addAttribute("company", investor);
    	model.addAttribute("cdfis", cdfis);
    	model.addAttribute("person", person);
    	populateSubscriptions(model, activeSubscriptions);

    	return "investor/myreports";
    }

	
	@RequestMapping(value = "/{id}/peergroups")
	public String peerGroups(@PathVariable("id") Long id, Model model) {

		Company investor = Company.findCompany(id);
    	Person person = authorizeCurrentUser(investor);
    	if (null == person) {
    		model.addAttribute("error", "You are not authorized to view the page requested.");
    		return "redirect:/";
    	}
    	
    	model.addAttribute("company", investor);
    	model.addAttribute("person", person);

    	Subscription subscription = getActiveAllSubscription(model, investor);
    	// we could allow admins access but seems better for them to see the same behaviour as the investor
//    	if (null == subscription || !subscription.isPeerGroups()) {
//			model.addAttribute("error",
//					"You are not authorized to view peer groups");
//			return "redirect:/investor/" + id + "/myreports";
//
//		}
    	
    	return "investor/peergroups";
    }

	@RequestMapping(value = "/{id}/selector")
	public String cdfiSelector(@PathVariable("id") Long id, @RequestParam(value = "rated", required = false) boolean rated, Model model) {

		Company investor = Company.findCompany(id);
    	Person person = authorizeCurrentUser(investor);
    	if (null == person) {
    		model.addAttribute("error", "You are not authorized to view the page requested.");
    		return "redirect:/";
    	}   	
    	    	
    	model.addAttribute("company", investor);
    	model.addAttribute("person", person);
    	model.addAttribute("onlyrated", rated);
    	
    	// needed to set the activeSubscriptionOfTypeAll attribute
    	getActiveAllSubscription(model, investor);

    	return "investor/selector";
    }
	
	// selector with a peer group in context. That is go into selector with an existing peer group	
	@RequestMapping(value = "/{id}/selector/{pgId}")
	public String cdfiSelector(@PathVariable("id") Long id, @PathVariable("pgId") Long pgId, Model model,
			HttpServletRequest request, HttpServletResponse response)
	{
	   	model.addAttribute("pgId", pgId);
	   	return cdfiSelector(id, false, model);
	}


	// TODO finish this
	@RequestMapping(value = "/{id}/peergroupreport/{pgId}")
	public String peerGroupReport(@PathVariable("id") Long id, @PathVariable("pgId") Long pgId, Model model) {

		Company investor = Company.findCompany(id);
    	Person person = UserHelper.getCurrentUser();
    	 	 	
    	model.addAttribute("company", investor);
    	model.addAttribute("person", person);
    	model.addAttribute("pgId", pgId);


    	Subscription subscription = getActiveAllSubscription(model, investor);
    	// we could allow admins access but seems better for them to see the same behaviour as the investor
    	if (null == subscription || !subscription.isPeerGroups()) {
			model.addAttribute("error",
					"You are not authorized to view peer groups");
			return "redirect:/investor/" + id + "/myreports";

		}
		return "investor/peergroupreport";
    }
	
	
	@RequestMapping(value = "{id}/schedule")
	public String schedule(@PathVariable("id") Long id, Model model) {

		Company investor = Company.findCompany(id);
    	Person person = UserHelper.getCurrentUser();

    	model.addAttribute("company", investor);
    	model.addAttribute("person", person);
		model.addAttribute("reviews", getPendingReviews());

		return "investor/schedule";
    }

    
	@RequestMapping(value = "/accept", method = RequestMethod.POST)
	public String accept(Model model) {

		Person person = UserHelper.getCurrentUser();
		if (person.getAgreementDate() == null) {
			person.setAgreementDate(new Date());
			person.merge();
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/acceptselect", method = RequestMethod.POST)
	public String acceptSelect(Model model) {

		Person person = UserHelper.getCurrentUser();
		if (person.getAgreementDateSelect() == null) {
			person.setAgreementDateSelect(new Date());
			person.merge();
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/decline")
	public String decline(Model model, HttpServletRequest request, HttpServletResponse response) {

		request.getSession().invalidate();
		SecurityContextHolder.getContext().setAuthentication(null);
		return "investor/decline";
	}

   
    private Person authorizeCurrentUser(Company investor) {
    	Person person = UserHelper.getCurrentUser();
    	if (null == person) {
    		return null;
    	}
    	
    	if (person.isAdmin() || person.getCompany().equals(investor)) {
    		return person;
    	}
    	return null;
	}
    
    private Set<CDFI> getCdfis(Company investor, List<Subscription> subscriptions, Integer reportDays) {
    	if (null == subscriptions) {
    		return null;
    	}
    	
    	boolean allPeerGroups = false;
    	boolean allFinancials = false;
    	List<Activity> docActivities = Activity.findActivityByCompanyEmployeesAndType(investor, ActivityType.DOCUMENT_DOWNLOAD);
    	
    	Map<Company, CDFI> cdfiMap = new HashMap<Company, CDFI>();
    	for (Subscription subscription : subscriptions) {
    		Set<Access> cdfiAccess = subscription.getCdfis();
    		for (Access access : cdfiAccess) {
    			if (!access.isActive() || !access.getCompany().isActive()) {
    				continue;
    			}
    			Company company = access.getCompany();
    			CDFI cdfi = cdfiMap.get(company);
    			if (null == cdfi) {
    				cdfi = new CDFI(company);
    				cdfiMap.put(company, cdfi);
    			}
    			if (subscription.isPeerGroups() && company.isPeerGroupAllowed()) {
    				cdfi.setPeerGroupAccess(true);
    			}
    			if (subscription.getSubscriptionType().equals(SubscriptionType.SINGLE) && subscription.isFinancials() && company.getShareFinancials() != ShareFinancials.NONE) {
    				cdfi.setFinancialsAccess(true);
    			}
    			if (subscription.isLibrary()) {
    				cdfi.setLibraryAccess(true);
    			}
    			cdfi.getReports().addAll(getReports(subscription, company, reportDays, docActivities));
    		}
    		if (subscription.getSubscriptionType().equals(SubscriptionType.ALL)) {
    			if (subscription.isPeerGroups()) 
    				allPeerGroups = true;
    			if (subscription.isFinancials())
    				allFinancials = true;
    		}
    	}
    	
    	// For ALL subsciptions the CDFI doesn't need to be in their 'ALL garage' in order to have access to
    	// peer groups and financials if that ALL subscription has the appropriate flag set
    	// So if a CDFI provided library access we will show the peergroup and financial access for that CDFI if
    	// such an ALL subscription also exists
    	for (Map.Entry<Company, CDFI> entry : cdfiMap.entrySet()) {
    		if (allPeerGroups) {
    			if (entry.getKey().isPeerGroupAllowed()) {
    				entry.getValue().setPeerGroupAccess(true);
    			}
    		}
    		if (allFinancials) {
    			if (entry.getKey().getShareFinancials() == ShareFinancials.ALL) {
    				entry.getValue().setFinancialsAccess(true);
    			}
    		}
    	}
    	
    	Set<CDFI> cdfis = new TreeSet<CDFI>(cdfiMap.values());

    	return cdfis;
    }


	private List<Report> getReports(Subscription subscription, Company cdfi, Integer reportDays, List<Activity> docActivities) {
		ArrayList<Report> reports = new ArrayList<Report>();
		
		Date today = new Date();
		if (null == reportDays) {
			reportDays = 365*4;
		}
		Date fromDate = DateUtils.addDays(today, -reportDays);
		List<Document> docs = Document.findNewApprovedDocumentsByDocumentTypeFolderAndCompany(DocumentTypeFolder.CARS_REPORT, cdfi, fromDate, today);
		if (null != docs) {
			for (Document document : docs) {
				if (document.isCarsFull()) {
					if (!subscription.isRatings()) {
						continue;
					}
				}
				else if (document.isCarsOpinion()) {
					if (!subscription.isOpinions()) {
						continue;
					}
				}
				else if (document.isCarsAnnual()) {
					if (!subscription.isAnnualReviews()) {
						continue;
					}
				}
				else {
					continue; // unexpected doc type
				}
				
				Report report = new Report(document);
				for (Activity activity : docActivities) {
					if (null != activity.getDocument()) {
						if (activity.getDocument().equals(document)) {
							report.setDownloaded(true);
							break;
						}
					}
				}
				reports.add(report);
			}	
		}
		
		return reports;
	}
	
	private Subscription populateSubscriptions(Model model, List<Subscription> activeSubscriptions) {   	
    	Subscription expiringSubscription = null;
    	Subscription allSubscription = null;
    	boolean isRatings = false;
    	boolean isOpinions = false;
    	boolean isPeerGroups = false;
    	boolean isFinancials = false;
    	if (null != activeSubscriptions) {
    		for (Subscription subscription : activeSubscriptions) {
    			SubscriptionType type = subscription.getSubscriptionType();
    			if (type == SubscriptionType.ALL) {
    				if (null == allSubscription || allSubscription.getExpirationDate().before(subscription.getExpirationDate())) {
    					allSubscription = subscription;
    				}
    			}
				if (null == expiringSubscription || expiringSubscription.getExpirationDate().before(subscription.getExpirationDate())) {
					expiringSubscription = subscription;
				}
				if (isRatings==false) {
					isRatings = subscription.isRatings();		
				}
				if (isOpinions==false) {
					isOpinions = subscription.isOpinions();		
				}
				if (isPeerGroups==false) {
					isPeerGroups = subscription.isPeerGroups();		
				}
				if (isFinancials==false) {
					isFinancials = subscription.isFinancials();		
				}
    		}
    	}
    	model.addAttribute("expiringSubscription", expiringSubscription);
    	model.addAttribute("fullSubscription", allSubscription);
    	model.addAttribute("isRatings", isRatings);
    	model.addAttribute("isOpinions", isOpinions);
    	model.addAttribute("isPeerGroups", isPeerGroups);
    	model.addAttribute("isFinancials", isFinancials);
    	
    	return expiringSubscription;
	}
	
	
	
	private List<Review> getPendingReviews() {
    	return Review.findAllReviewsPending();
	}
	
	private List<Company> getNewCdfis(Set<CDFI> existingCdfis) {
		Date sixtyDaysAgo = DateUtils.addDays(new Date(), -60);
		List<Company> newCdfis = Company.findActiveCdfisSince(sixtyDaysAgo);
		List<Company> result = new ArrayList<Company>();
		for (Company company : newCdfis) {
			boolean found = false;
			for (CDFI cdfi : existingCdfis) {
				if (cdfi.getId() == company.getId()) {
					found = true;
					break;
				}
			}
			if (!found) {
				result.add(company);
			}
		}
		return result;
	}

	
	private Subscription getActiveAllSubscription(Model model, Company investor) {
		// The CDFI selector uses the fact that an active full subscription exists
		// in order to know whether or not to include 'add to garage' to the menu options
		Subscription activeSubscriptionOfTypeAll = null;
		for (Subscription sub : Subscription.findActiveOwnedSubscriptionsForSubscriber(investor)) {
			if (sub.getSubscriptionType() == SubscriptionType.ALL) {
				activeSubscriptionOfTypeAll = sub;
				break;
			}
		}
		model.addAttribute("activeSubscriptionOfTypeAll", activeSubscriptionOfTypeAll == null ? null : activeSubscriptionOfTypeAll.getId());
		setPeerGroupAccess(model, activeSubscriptionOfTypeAll);
		return activeSubscriptionOfTypeAll;
	}
	
	private void setPeerGroupAccess(Model model, Subscription activeSubscriptionOfTypeAll) {
		Person person = UserHelper.getCurrentUser();
		PeerGroupAccess peerGroupAccess;
		if (person.getCompany().isCARS()) {
			if (person.isAdmin()) {
				peerGroupAccess = PeerGroupAccess.FULL;
			}
			else {
				peerGroupAccess = PeerGroupAccess.NONE; // analyst... should be unreachable code
			}
		}
		else if (person.getCompany().isCDFI()) {
			peerGroupAccess = PeerGroupAccess.NONE; // should be unreachable
		}
		else { // Investor
			if (null != activeSubscriptionOfTypeAll && activeSubscriptionOfTypeAll.isPeerGroups()) {
				peerGroupAccess = PeerGroupAccess.FULL;				
			}
			else {
				peerGroupAccess = PeerGroupAccess.NONE;								
			}
		}
		model.addAttribute("peerGroupAccess", peerGroupAccess);
	}


/*	@RequestMapping(value = "/myreports/distribution")
	public String distribution(ModelMap model) {

		TreeMap<String, Integer> impactMap = new TreeMap<String, Integer>();
		TreeMap<String, Integer> financialMap = new TreeMap<String, Integer>();

		List<Company> list = Company.findRatedCDFIs();

		for (Company cdfi : list) {
			String rating = cdfi.getRating();
			// rating will be "AA + 2" or "AA 2", need to parse

			if (rating != null && rating.length() > 2) {
				String[] parts = rating.split(" ");
				String impact = parts[0];
				String financial = parts[parts.length - 1];

				Integer impactTotal = impactMap.get(impact);
				if (impactTotal == null)
					impactTotal = new Integer(0);
				impactTotal++;
				impactMap.put(impact, impactTotal);

				Integer financialTotal = financialMap.get(financial);
				if (financialTotal == null)
					financialTotal = new Integer(0);
				financialTotal++;
				financialMap.put(financial, financialTotal);
			}
		}

		String impactGraph = getPieChart(impactMap, 1, "Distribution of Current Impact Performance Ratings");
		String financialGraph = getPieChart(financialMap, 2, "Distribution of Current Financial Strength Ratings");

		model.addAttribute("impactGraph", impactGraph);
		model.addAttribute("financialGraph", financialGraph);

		return "myreports/distribution";
	}

*/

	@RequestMapping(value = "/{id}/addToGarage/{subscriptionId}", method = RequestMethod.GET)
	public @ResponseBody String addToGarage(@PathVariable("id") Long id, @PathVariable Long subscriptionId) {
		Subscription sub = Subscription.findSubscription(subscriptionId);
		if (sub != null) {
			Company cdfi = Company.findCompany(id);
			Access access = new Access();
			access.setEffectiveDate(null);
			access.setExpirationDate(null);
			access.setCompany(cdfi);
			sub.getCdfis().add(access);
			sub.merge();
		}
		return "OK";
	}

	
	 @RequestMapping(value = "{id}/distribution")
	 public String distribution(@PathVariable("id") Long id, ModelMap model) {

		TreeMap<String, Integer> impactMap = new TreeMap<String, Integer>();
		TreeMap<String, Integer> financialMap = new TreeMap<String, Integer>();

		List<Company> list = Company.findRatedCDFIs();

		for (Company cdfi : list) {
		    String rating = cdfi.getRating();
		    // rating will be "AA + 2" or "AA 2", need to parse

		    if (rating != null && rating.length() > 2) {
			String[] parts = rating.split(" ");
			String impact = parts[0];
			String financial = parts[parts.length - 1];

			String imp = "";
			if((impact.compareTo("★★★★") == 0) || (impact.compareTo("****") == 0))
				imp = "four_stars";
			else if (impact.compareTo("★★★") == 0 || (impact.compareTo("***") == 0))
				imp = "three_stars";
			else if (impact.compareTo("★★") == 0 || (impact.compareTo("**") == 0))
				imp = "two_stars";
			else if (impact.compareTo("★") == 0 || (impact.compareTo("*") == 0))
				imp = "one_stars";
			Integer impactTotal = impactMap.get(imp);
			if (impactTotal == null)
			    impactTotal = new Integer(0);
			impactTotal++;
			impactMap.put(imp, impactTotal);
			
			String f ="";
			if (financial.compareTo("AAA") == 0)
				f ="AAA";
			else if (financial.compareTo("AA+") == 0)
				f ="AA_plus";
			else if (financial.compareTo("AA") == 0)
				f ="AA";
			else if (financial.compareTo("AA-") == 0)
				f ="AA_minus";
			else if (financial.compareTo("A+") == 0)
				f ="A_plus";
			else if (financial.compareTo("A") == 0)
				f ="A";
			else if (financial.compareTo("A-") == 0)
				f ="A_minus";
			else if (financial.compareTo("BBB+") == 0)
				f ="BBB_plus";
			else if (financial.compareTo("BBB") == 0)
				f ="BBB";
			else if (financial.compareTo("BBB-") == 0)
				f ="BBB_minus";
			else if (financial.compareTo("BB+") == 0)
				f ="BB_plus";
			else if (financial.compareTo("BB") == 0)
				f ="BB";
			else if (financial.compareTo("BB-") == 0)
				f ="BB_minus";
			else if (financial.compareTo("B") == 0)
				f ="B";

			Integer financialTotal = financialMap.get(f);
			if (financialTotal == null)
			    financialTotal = new Integer(0);
			financialTotal++;
			
			financialMap.put(f, financialTotal);
		    }
		}
		
		Company investor = Company.findCompany(id);
    	Person person = UserHelper.getCurrentUser();

    	model.addAttribute("company", investor);
    	model.addAttribute("person", person);
		
		model.addAttribute("impactGraph", impactMap);
		model.addAttribute("financialGraph", financialMap);
		
		return "investor/distribution";
	 }
}

