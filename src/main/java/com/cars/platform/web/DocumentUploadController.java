package com.cars.platform.web;

import static com.cars.platform.domain.DocumentTypeFolder.CARS_RESOURCES;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.DefaultFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.UserHelper;

@RequestMapping("/documentupload/**")
@Controller
public class DocumentUploadController {

    @Autowired
    DocumentDataService documentDataService;

    // @RequestMapping(method = RequestMethod.POST, value = "{id}")
    // public void post(@PathVariable Long id, ModelMap modelMap,
    // HttpServletRequest request, HttpServletResponse response) {
    @RequestMapping(method = RequestMethod.POST)
    public void post(HttpServletRequest request, HttpServletResponse response) {

	System.out.println("POSTED TO DOCUMENTUPLOAD");

	Enumeration enu = request.getParameterNames();

	while (enu.hasMoreElements()) {
	    String fieldName = (String) enu.nextElement();
	    System.out.println("NAME=" + fieldName);
	}

	boolean isMultipart = ServletFileUpload.isMultipartContent(request);

	System.out.println("MULTIPART: " + isMultipart);

	if (isMultipart) {
	    FileItemFactory factory = new DefaultFileItemFactory();

	    ServletFileUpload upload = new ServletFileUpload(factory);

	    try {
		List items = upload.parseRequest(request);

		System.out.println("item count: " + items.size());
		Iterator iterator = items.iterator();
		while (iterator.hasNext()) {
		    FileItem item = (FileItem) iterator.next();

		    System.out.println("file item: " + item.getFieldName()
			    + ", formField:" + item.isFormField());

		    if (!item.isFormField()) {

			String fileName = item.getName();

			System.out.println("FILENAME: " + fileName + ", size: "
				+ item.getSize());

			/*
			 * String root = getServletContext().getRealPath("/");
			 * File path = new File(root + "/uploads"); if
			 * (!path.exists()) { boolean status = path.mkdirs(); }
			 * 
			 * File uploadedFile = new File(path + "/" + fileName);
			 * System.out.println(uploadedFile.getAbsolutePath());
			 * item.write(uploadedFile);
			 */
		    }
		}
		// } catch (FileUploadException e) {
		// e.printStackTrace();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
	uiModel.addAttribute("company", new Company());
	return "documentupload/selector";

    }

    @RequestMapping(method = RequestMethod.GET, value = "/documentupload")
    public String index(
	    @RequestParam(value = "companyId", required = true) Long companyId,
	    Model model) {

	Company company = Company.findCompany(companyId);
	model.addAttribute("company", company);

	Person person = UserHelper.getCurrentUser();
	model.addAttribute("person", person);

	List<DocumentType> allRequiredDocumentTypes = DocumentType.findAllDocumentTypesAlphabetically(company);
	ArrayList<DocumentType> filteredRequiredDocumentTypes = new ArrayList<DocumentType>();

	for (DocumentType docType : allRequiredDocumentTypes) {

	    if (!person.getCompany().isCARS()
		    && !"Historic Financial Data".equalsIgnoreCase(docType
			    .getName())
		    && !docType.getDocumentTypeFolder().getName()
			    .startsWith("AERIS")) {
		filteredRequiredDocumentTypes.add(docType);
	    }

	    else if (person.getCompany().isCARS()
		    && person.getPersonRole() == PersonRole.BASIC
		    && !docType.getDocumentTypeFolder().getName()
			    .startsWith(CARS_RESOURCES)
		    && !"Historic Financial Data".equalsIgnoreCase(docType
			    .getName())) {
		filteredRequiredDocumentTypes.add(docType);
	    }

	    else if (person.getCompany().isCARS()
		    && person.getPersonRole() == PersonRole.ADMIN
		    && !docType.getDocumentTypeFolder().getName()
			    .startsWith(CARS_RESOURCES)) {
		filteredRequiredDocumentTypes.add(docType);
	    }

	}
	model.addAttribute("docTypes", filteredRequiredDocumentTypes);
	
	int fiscalMonthStart = company.getFiscalYearStart();
	int year = DateHelper.getCurrentYear(fiscalMonthStart);
	model.addAttribute("year", year);
	
	return "documentupload/index";
    }

    @RequestMapping(value = "/documentupload/required/{companyId}/{documentTypeId}/{year}/{quarter}")
    public String select(@PathVariable Long companyId,
	    @PathVariable Long documentTypeId, @PathVariable Long year,
	    @PathVariable Long quarter, Model model) {

	Company company = Company.findCompany(companyId);
	model.addAttribute("company", company);

	DocumentType documentType = DocumentType
		.findDocumentType(documentTypeId);
	model.addAttribute("documentType", documentType);

	model.addAttribute("year", year);
	model.addAttribute("quarter", quarter);

	return "documentupload/required";
    }

    @RequestMapping(value = "/documentupload/required/{companyId}/{documentTypeId}")
    public String selectCARSDoc(@PathVariable Long companyId,
	    @PathVariable Long documentTypeId, Model model) {

	Company company = Company.findCompany(companyId);
	model.addAttribute("company", company);

	DocumentType documentType = DocumentType
		.findDocumentType(documentTypeId);
	model.addAttribute("documentType", documentType);

	return "documentupload/required";
    }

    @RequestMapping(value = "/documentupload/map")
    public String map() {
	System.out.println("MAP");
	return "documentupload/index";
    }

    @RequestMapping(value = "/documentupload/delete/{id}")
    public String delete(@PathVariable Long id, Model model) {
	Document document = Document.findDocument(id);
	long companyId = document.getCompany().getId();

	document.remove();

	documentDataService.deleteDocumentData(companyId, id);
	return "redirect:/cdfi/" + companyId + "/library";
    }

    @RequestMapping(value = "/documentupload/checkout/{id}")
    public String checkout(@PathVariable Long id, Model model) {
	Person person = UserHelper.getCurrentUser();
	Document document = Document.findDocument(id);
	long companyId = document.getCompany().getId();

	if (person.isAdmin()
		|| (document.getCompany() != null
			&& document.getCompany().getAnalysts() != null && document
			.getCompany().getAnalysts().contains(person))) {
	    document.setCheckedOutBy(person);
	    document.setCheckedOutDate(new Date());
	    document.merge();
	    model.addAttribute("notice", "file checked out");
	}
	model.addAttribute("notice", "file \"" + document.getFileName()
		+ "\" checked out");
	return "redirect:/cdfi/" + companyId + "/library";
    }

    @RequestMapping(value = "/documentupload/checkin/{id}")
    public String checkin(@PathVariable Long id, Model model) {
	Person person = UserHelper.getCurrentUser();
	Document document = Document.findDocument(id);
	long companyId = document.getCompany().getId();

	if (person.isAdmin()
		|| (document.getCompany() != null
			&& document.getCompany().getAnalysts() != null && document
			.getCompany().getAnalysts().contains(person))) {
	    document.setCheckedOutBy(null);
	    document.setCheckedOutDate(null);
	    document.merge();
	    model.addAttribute("notice", "file \"" + document.getFileName()
		    + "\" checked in");

	}
	return "redirect:/cdfi/" + companyId + "/library";
    }

    @RequestMapping(value = "/documentupload/approve/{id}")
    public String approve(@PathVariable Long id, Model model) {
	Person person = UserHelper.getCurrentUser();
	Document document = Document.findDocument(id);
	if (person.isAdmin()) {
	    document.setApprovedBy(person);
	    document.setApprovedDate(new Date());
	    document.merge();
	    model.addAttribute("notice", "file \"" + document.getFileName()
		    + "\" approved");

	} else {
	    model.addAttribute("error",
		    "only an operations user can approve a ratings report");
	}
	long companyId = document.getCompany().getId();
	return "redirect:/cdfi/" + companyId + "/library";
    }

    @RequestMapping(value = "/documentupload/unapprove/{id}")
    public String unapprove(@PathVariable Long id, Model model) {
	Person person = UserHelper.getCurrentUser();
	Document document = Document.findDocument(id);
	if (person.isAdmin()) {
	    document.setApprovedBy(null);
	    document.setApprovedDate(null);
	    document.merge();
	    model.addAttribute("notice", "file \"" + document.getFileName()
		    + "\" unapproved");

	} else {
	    model.addAttribute("error",
		    "only an operations user can unapprove a ratings report");
	}
	long companyId = document.getCompany().getId();
	return "redirect:/cdfi/" + companyId + "/library";
    }

}
