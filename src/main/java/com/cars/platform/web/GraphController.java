package com.cars.platform.web;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Graph;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.UnitType;
import com.cars.platform.service.graph.GraphService;
import com.cars.platform.util.SelectOptionHelper;
@RequestMapping("/graphs")
@Controller
public class GraphController {
	
	static Company nullCompany = new Company();
	static{
     nullCompany.setName("All CDFIs");
     nullCompany.setId(0L);
	}

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Graph graph, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, graph);
            return "graphs/create";
        }
        uiModel.asMap().clear();
        graph.persist();
        
        if(graph.getCompany()!=null){
        	ReportDataCache.remove(graph.getCompany());
        	return "redirect:/graphs?companyId="+ graph.getCompany().getId();
        }else{
        	ReportDataCache.removeAll();
        }
        return "redirect:/graphs";        
    }   
    
    @RequestMapping(value = "cdfi/{id}",params = "form", produces = "text/html")
    public String createForCDFI(@PathVariable("id") Long id,  Model uiModel) {
    	Company company = Company.findCompany(id);
    	Graph graph = new Graph();
    	graph.setCompany(company);
        populateEditForm(uiModel,graph);
        return "graphs/create";
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
    	Graph graph = new Graph();
    	graph.setCompany(nullCompany);	
    	populateEditForm(uiModel, graph);
        return "graphs/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("graph", Graph.findGraph(id));
        uiModel.addAttribute("itemId", id);
        return "graphs/show";
    }
    
    @RequestMapping(value = "override/{id}", produces = "text/html")
    public String override(@PathVariable("id") Long id, @RequestParam(value = "graphId", required = false) Long graphId, Model uiModel) {
    	Company company = Company.findCompany(id);
    	Graph graph = new Graph();
    	if (graphId != null) {
    		Graph original = Graph.findGraph(graphId);
    		if (original!=null)
    			graph = original.cloneGraph();
    	} 
    	graph.setCompany(company);
    	graph.persist();
        return "redirect:/graphs?companyId=" + id;
    }
    
    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "companyId", required = false) Long companyId, Model uiModel) {
       
    	Company company = null; 
    	List<Graph> graphs = null;
    	
    	
    	if(companyId!=null){
    		company = Company.findCompany(companyId);
    	}
    	 
    	if(company!=null){
    		uiModel.addAttribute("company",Company.findCompany(companyId));
    		graphs = Graph.findAllGraphsByCompany(company);
    		uiModel.addAttribute("graphs", graphs);
    		return "graphs/listcdfi";
    	}else{
	    	graphs = Graph.findGlobalGraphs();	
	    	uiModel.addAttribute("graphs", graphs);
	    	return "graphs/list";
    	} 
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Graph graph, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, graph);
            return "graphs/update";
        }
        uiModel.asMap().clear();
        
        // calling merge on the Graph passed in here causes the following error 
        //org.hibernate.HibernateException: A collection with cascade="all-delete-orphan" was no longer referenced by the owning entity instance
        // something to do with the OneToMany relationship to GraphEquation.
        // Fetching the existing entity and modifying it works around the issue

        //graph.merge();
        Graph graph1 = Graph.findGraph(graph.getId());
        graph1.setCode(graph.getCode());
        graph1.setGraphType(graph.getGraphType());
        graph1.setNotes(graph.getNotes());
        graph1.setShowInterim(graph.isShowInterim());
        graph1.setShowYears(graph.getShowYears());
        graph1.setTitle(graph.getTitle());
        graph1.setUnitType(graph.getUnitType());
        
        graph1.merge();

    
        
        if(graph.getCompany()!=null){
        	ReportDataCache.remove(graph.getCompany());
        
        	return "redirect:/graphs?companyId="+ graph.getCompany().getId();
        }else{
        	ReportDataCache.removeAll();
        }

        return "redirect:/graphs";
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Graph.findGraph(id));
        return "graphs/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Graph graph = Graph.findGraph(id);
    	Company company = graph.getCompany();
    	
    	
    	try{
    	
        graph.remove();
        uiModel.asMap().clear();
        if(company!=null){
        	ReportDataCache.remove(company);
        	return "redirect:/graphs?companyId="+ company.getId();
        }else{
        	ReportDataCache.removeAll();
        }
        
		}catch(Exception e){
			
			String errorString = "Unable to delete graph";

			if (e.getMessage().contains("ConstraintViolationException")){
				errorString += " due to related data that requires this record";
			}
			
			uiModel.addAttribute("error", errorString);		
		}
        
        
        if(company!=null){
        	ReportDataCache.remove(company);
        	return "redirect:/graphs?companyId="+ company.getId();
        }

    	
        return "redirect:/graphs";
    }
    
    void populateEditForm(Model uiModel, Graph graph) {
        uiModel.addAttribute("graph", graph);
        
        List<Equation> equations = null;
        List<Graph> graphList = null;
		if(graph.getCompany()!=null && graph.getCompany().getId() != null && graph.getCompany().getId() > 0L){
        	equations = Equation.findAllEquationsByCompanySortedByName(graph.getCompany());
        	graphList = Graph.findAllGraphsByCompany(graph.getCompany());
        }else{
        	equations = Equation.findGlobalEquations();
        	graphList = Graph.findGlobalGraphs();
        }
		
		
		uiModel.addAttribute("graphList", graphList);
        uiModel.addAttribute("equations", equations);
        uiModel.addAttribute("graphtypes", Arrays.asList(GraphType.values()));
        uiModel.addAttribute("unittypes", Arrays.asList(UnitType.values()));
        uiModel.addAttribute("showYears", SelectOptionHelper.getShowYears());
        
        Company company = null;
        if (null != graph.getCompany()) {
        	company = graph.getCompany();
        }
        else {
           company = nullCompany;	
        }
        uiModel.addAttribute("company", company);	
        uiModel.addAttribute("companys", Arrays.asList(company));           
    }
    
    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
    
    
    //----------------
    // REST methods
    //----------------
    
    /**
     * GET method for returning the viewmodel Equations for the given graph
     * @param id is the id of the graph to get the equations for
     * @return list of Equation viewModel objects
     */
    @RequestMapping(value = "/{id}/equations", method = RequestMethod.GET)
    public @ResponseBody com.cars.platform.viewmodels.report.Equation[] getEquations(
    		@PathVariable("id") Long id,
    		Model model, HttpServletRequest request, HttpServletResponse response)
    {
    	Graph graph = Graph.findGraph(id);
    	if (null == graph) {
			throw new IllegalArgumentException("There is no graph with ID of: " + id);   		
    	}
    	
    	List<Equation> equations = graph.getEquations(graph.getCompany());
    	
    	com.cars.platform.viewmodels.report.Equation[] viewModelEquations =
    			new com.cars.platform.viewmodels.report.Equation[equations.size()];
    	
    	for (int i=0; i<equations.size(); i++) {
    		viewModelEquations[i] = new com.cars.platform.viewmodels.report.Equation(equations.get(i));
    	}

 	    return viewModelEquations;
 	}

    
    
    /**
     * PUT method for updating the equations associated to the graph
     * @param id is the id of the graph to set the equations for
     * @param equationIds The IDs of all the equation in rank order.
     */
	@RequestMapping(value = "/{id}/equations", method = RequestMethod.PUT, produces = "text/html")
    public @ResponseBody String setEquations(
    		@PathVariable("id") Long id,
       		@RequestBody List<Integer> equationIds, // making this List<Long> doesn't work... you actually get ints inside
    		Model uiModel, HttpServletRequest httpServletRequest)
	{
//        try { // TODO get the server to log exceptions thrown by controllers
		if (null == equationIds) {
			throw new IllegalArgumentException("The list of equations for this graph is required.");
		}
    	Graph graph = Graph.findGraph(id);
    	if (null == graph) {
			throw new IllegalArgumentException("There is no graph with ID of: " + id);   		
    	}

		List<Equation> equations = new ArrayList<Equation>();
		for (Integer equationId : equationIds) {
			Long longId = Long.valueOf(equationId.longValue());
			Equation equation = Equation.findEquation(longId);
			if (null == equation) {
				throw new IllegalArgumentException("There is no equation with ID: " + longId);
			}
			// If the equation is a CDFI override we need to replace it with the global one it is overriding
			if (null != equation.getOverride()) {
				equation = equation.getOverride();
			}
			equations.add(equation);
		}

		graph.updateEquations(equations);
		graph.persist();
        
		return "OK";
	}
	
	
    // REST service which provides data for the  graphs for the provided company
    @RequestMapping(value = "/report/{companyId}")
    public @ResponseBody List<com.cars.platform.viewmodels.graph.Graph> report(@PathVariable Long companyId, Model model,
	    HttpServletRequest request, HttpServletResponse response) {

    	Company company = Company.findCompany(companyId);
    	
    	GraphService gs = new GraphService(company);
    	
    	List<com.cars.platform.viewmodels.graph.Graph> graphs = gs.getGraphs(true, true);

 	    return graphs;
    }
    
    // REST service which provides the performance view graphs for the provided company
    @RequestMapping(value = "/performance/{companyId}")
    public @ResponseBody List<com.cars.platform.viewmodels.graph.Graph> getGraphs(@PathVariable Long companyId, Model model,
	    HttpServletRequest request, HttpServletResponse response) {

    	Company company = Company.findCompany(companyId);
    	
    	GraphService gs = new GraphService(company);
    	  	
    	List<com.cars.platform.viewmodels.graph.Graph> graphs = gs.getPerformanceViewGraphs();

 	    return graphs;
    }

}
