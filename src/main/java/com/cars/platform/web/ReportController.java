package com.cars.platform.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.viewmodels.EquationPOJO;
import com.cars.platform.viewmodels.MetricPOJO;
import com.cars.platform.viewmodels.MetricValuePOJO;
import com.cars.platform.viewmodels.PeriodPOJO;

@Controller
public class ReportController {

	@RequestMapping(value = "reports/{companyId}", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getReport(@PathVariable long companyId) {

		Mapper mapper = new DozerBeanMapper();

		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("companyId", Long.toString(companyId));

		Company company = Company.findCompany(companyId);

		List<MetricPOJO> metricPOJOs = new ArrayList<MetricPOJO>();
		for (Metric metric : Metric.findAllCompanyMetrics(companyId)) {
			metricPOJOs.add(mapper.map(metric, MetricPOJO.class));
		}

		List<MetricValuePOJO> metricValuePOJOs = new ArrayList<MetricValuePOJO>();
		for (MetricValue metricValue : MetricValue.findMetricValues(company)) {
			metricValuePOJOs
					.add(mapper.map(metricValue, MetricValuePOJO.class));
		}

		List<EquationPOJO> equationPOJOs = new ArrayList<EquationPOJO>();
		for (Equation equation : Equation.findAllEquationsByCompany(company)) {
			equationPOJOs.add(mapper.map(equation, EquationPOJO.class));
		}

		List<PeriodPOJO> periodPOJOs = new ArrayList<PeriodPOJO>();
		for (Period period : Period.findPeriodsByCompanySorted(company)) {
			periodPOJOs.add(mapper.map(period, PeriodPOJO.class));
		}

		map.put("metrics", metricPOJOs);
		map.put("metricValues", metricValuePOJOs);
		map.put("equations", equationPOJOs);
		map.put("periods", periodPOJOs);

		return map;
	}
}