package com.cars.platform.web;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;

@RequestMapping("/metricbreakdowns")
@Controller
//@RooWebScaffold(path = "metricbreakdowns", formBackingObject = MetricBreakdown.class)
public class MetricBreakdownController {
	
	

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid MetricBreakdown metricBreakdown, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, metricBreakdown);
            return "metricbreakdowns/update";
        }
        uiModel.asMap().clear();
        
        // calling merge on the reportCategory passed in here causes the following error 
        // org.hibernate.HibernateException: A collection with cascade="all-delete-orphan" was no longer referenced by the owning entity instance
        // something to do with the OneToMany relationship to ReportCategoryEquation.
        // Fetching the existing entity and modifying it works around the issue

        MetricBreakdown metricBreakdown1 = MetricBreakdown.findMetricBreakdown(metricBreakdown.getId());
        metricBreakdown1.setName(metricBreakdown.getName());
        metricBreakdown1.setLabel(metricBreakdown.getLabel());

        metricBreakdown1.merge();
        return "redirect:/metricbreakdowns/" + encodeUrlPathSegment(metricBreakdown.getId().toString(), httpServletRequest);
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid MetricBreakdown metricBreakdown, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, metricBreakdown);
            return "metricbreakdowns/create";
        }
        uiModel.asMap().clear();
        metricBreakdown.persist();
        return "redirect:/metricbreakdowns/" + encodeUrlPathSegment(metricBreakdown.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new MetricBreakdown());
        return "metricbreakdowns/create";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("metricbreakdown", MetricBreakdown.findMetricBreakdown(id));
        uiModel.addAttribute("itemId", id);
        return "metricbreakdowns/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("metricbreakdowns", MetricBreakdown.findMetricBreakdownEntries(firstResult, sizeNo));
            float nrOfPages = (float) MetricBreakdown.countMetricBreakdowns() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("metricbreakdowns", MetricBreakdown.findAllMetricBreakdowns());
        }
        return "metricbreakdowns/list";
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, MetricBreakdown.findMetricBreakdown(id));
        return "metricbreakdowns/update";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        MetricBreakdown metricBreakdown = MetricBreakdown.findMetricBreakdown(id);
        try {
	        metricBreakdown.remove();
	        uiModel.asMap().clear();
        }
		// TODO do this architecturally?
		catch(Throwable t) {
			uiModel.asMap().clear();
			uiModel.addAttribute("error", "Unable to delete the Metric Breakdown. All other items referencing it must be deleted first. This can include: Metrics, Metric Breakdown Items, Breakdown Columns, and Breakdown Graphs.");
		}
        
        return "redirect:/metricbreakdowns";
    }


	void populateEditForm(Model uiModel, MetricBreakdown metricBreakdown) {
        uiModel.addAttribute("metricBreakdown", metricBreakdown);
        uiModel.addAttribute("metricbreakdownitems", MetricBreakdownItem.findAllMetricBreakdownItems());
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
