package com.cars.platform.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Activity;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Message;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.domain.State;
import com.cars.platform.domain.Subscription;
import com.cars.platform.util.UserHelper;

@RequestMapping("/subscriber/**")
@Controller
public class SubscriberController {
	
	@RequestMapping(value = "/subscriber/{id}")
	public String show(@PathVariable Long id, Model model) {

		Company company = Company.findCompany(id);
			
		List<Person> employees = Person.findPeopleByCompany(company).getResultList();
			
		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", UserHelper.getCurrentUser());
		model.addAttribute("company", company);	
				
		model.addAttribute("employees", employees);	
		
		model.addAttribute("activity",Activity.findRecentActivityByCompanyEmployees(company));
		
		List<Subscription> subscriptions = Subscription.findSubscriptionsByOwner(company).getResultList();
		model.addAttribute("subscriptions",subscriptions);
		
		//List<Message> messages = Message.findRecentMessagesByRecipientAndSenderCompany(person, company).getResultList();
		List<Message> messages = Message.findRecentMessagesByRecipient(person).getResultList();
		
		model.addAttribute("messages", messages);				

        return "subscriber/show";
    }
			

    @RequestMapping
    public String index() {
        return "subscriber/show";
    }
    
	@RequestMapping(params = "form", produces = "text/html")
	public String createForm(Model uiModel) {
		Company company = new Company();
		//company.setCompanyType(CompanyType.INVESTOR);
		uiModel.addAttribute("company", company);
		return "subscriber/create";
	}    
	
	
	@RequestMapping(value = "/subscriber/{id}/edit", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		Company company = Company.findCompany(id);
		uiModel.addAttribute("company",company);
		
		
		uiModel.addAttribute("states", State.findAllStates());
        //populateEditForm(uiModel, Graph.findGraph(id));
        return "subscriber/update";
    }	
	
	
	
	@RequestMapping(method = RequestMethod.POST)
	public String update(
			Model model,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "address2", required = true) String address2,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state,
			@RequestParam(value = "zip", required = true) String zip,
			@RequestParam(value = "phone", required = true) String phone,
			@RequestParam(value = "fax", required = false) String fax,
			@RequestParam(value = "active", required = false) String active,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "firstName", required = true) String firstName,
			@RequestParam(value = "lastName", required = true) String lastName,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "hideActivity", required = false) String hideActivity,
			@RequestParam(value = "allRows", required = false) String allRows	
			){


		String error = null;

		// check for problems

		// see if user already exists
		Person existingPerson = null;

		try {
			existingPerson = Person.findPeopleByUsernameEquals(username);
		} catch (Exception ignore) {
		}

		if (existingPerson != null) {
			System.out.println("USER ALREADY EXISTS, USERNAME: " + username);
			error = "user with name [" + username + "] already exists";
		}

		if (error != null) {
			model.addAttribute("name", name);			
			model.addAttribute("address", address);
			model.addAttribute("address2", address2);
			model.addAttribute("city", city);
			model.addAttribute("state", state);
			model.addAttribute("zip", zip);
			model.addAttribute("phone", phone);
			model.addAttribute("fax", fax);
			model.addAttribute("username", username);
			model.addAttribute("firstName", firstName);
			model.addAttribute("lastName", lastName);
			model.addAttribute("email", email);

			if(active!=null)model.addAttribute("active", active);
			model.addAttribute("title", title);
			
			model.addAttribute("error", error);
			return "subscriber/create";
		}

		// if all is well, persist data

		// create company

		Company company = new Company();
		company.setName(name);

		//company.setRated(rated);
		company.setAddress(address);
		company.setAddress2(address2);
		company.setCity(city);
		company.setState(state);
		company.setZip(zip);
		company.setPhone(phone);

		company.setCompanyType(CompanyType.INVESTOR);
		company.setFax(fax);
		company.setActive(active!=null);
		company.setHideActivity(hideActivity!=null);
		company.setAllRows(allRows!=null);
		company.persist();
		long companyId = company.getId();

		// create user

		Person person = new Person();

		person.setUsername(username);
		person.setPassword(password);
		person.setFirstName(firstName);
		person.setLastName(lastName);
		person.setEmail(email);
		person.setTitle(title);
		person.setCompany(company);
		
		person.setPersonRole(PersonRole.BASIC);

		person.persist();

		return "redirect:subscriber/" + companyId;
	}
	
	@RequestMapping(method = RequestMethod.POST,value = "/subscriber/message")
	public @ResponseBody String message(
			Model model,
			
			@RequestParam(value = "recipient", required = false) Long recipientId,
			@RequestParam(value = "text", required = false) String text){
		
		Person sender = UserHelper.getCurrentUser();
		Person recipient = Person.findPerson(recipientId);
		Date date = new Date();
		
		Message message = new Message();
		message.setArchived(false);
		message.setRecipient(recipient);
		message.setSender(sender);
		message.setText(text);
		message.setSent(date);
		
		message.persist();

		SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
				
		return "Your message has been sent to " + recipient.getFirstName() + " " + recipient.getLastName() + " at " + timeFormat.format(date)  + " on " + dateFormat.format(date) + ".";				
	}
	
	
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Company company, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            //populateEditForm(uiModel, graph);
            return "subscriber/update";
}
        uiModel.asMap().clear();
       // company.merge();

        Company existingCompany = Company.findCompany(company.getId());
        if(existingCompany!=null){
        	existingCompany.setName(company.getName());
        	existingCompany.setAddress(company.getAddress());
           	existingCompany.setAddress2(company.getAddress2());
           	existingCompany.setCity(company.getCity());
           	existingCompany.setState(company.getState());
           	existingCompany.setZip(company.getZip());
           	existingCompany.setPhone(company.getPhone());
           	existingCompany.setFax(company.getFax());
           	existingCompany.setAllRows(company.isAllRows());
           	existingCompany.setActive(company.isActive());
           	existingCompany.setHideActivity(company.isHideActivity());
           	existingCompany.merge();
        }

        return "redirect:/subscriber/"+company.getId();
    }
    	
    
	@RequestMapping(value = "/subscriber/delete/{id}")
	public String delete(@PathVariable Long id, Model model) {
		Company company = null;
		try{
		
		company = Company.findCompany(id);
		
		company.remove();
		
		}catch(Exception e){
			
			String errorString = "Unable to delete subscriber";

			if (e.getMessage().contains("ConstraintViolationException")){
				errorString += " due to related data that requires this record";
			}
			
			model.addAttribute("error", errorString);
			return "redirect:/subscriber/"+id;
		}
	
		return  "redirect:/";
	}	
	
	
}
