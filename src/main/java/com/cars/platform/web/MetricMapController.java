package com.cars.platform.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Person;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.service.SpreadService2;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.MappedPage;
//import com.cars.platform.service.SpreadService;

@RequestMapping("/metricmaps/**")
@Controller
public class MetricMapController {
	
    @Autowired
    private DocumentDataService documentDataService;
   
	@RequestMapping(value = "/metricmaps/{id}")
	public String show(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		Document doc = Document.findDocument(id);
		
		Company company = Company.findCompany(doc.getCompany().getId());
		model.addAttribute("company", company);
	
		InputStream is = documentDataService.getDocumentData(doc.getCompany().getId(), doc.getId());
				
		//SpreadService spreadService = new SpreadService();
		SpreadService2 spreadService2 = new SpreadService2();
		
		//String html = spreadService.getSheetAsHTML(is,0);
		String html = spreadService2.getSheetAsHTML(doc.getFileName(), is,0);
			
		model.addAttribute("spread", html);

		Person person = UserHelper.getCurrentUser();
	    model.addAttribute("person", person);
		return "metricmaps/show";
	}
	  
	@RequestMapping(value = "/metricmaps/source/{companyId}/{year}/{quarter}", method = RequestMethod.GET)
	public @ResponseBody
	List<MappedPage> getSource(
			@PathVariable(value = "companyId") Long companyId,
			@PathVariable(value = "year") Integer year,
			@PathVariable(value = "quarter") Integer quarter) throws IOException {

		System.out.println("getting company " + System.currentTimeMillis());
		Company company = Company.findCompany(companyId);
		System.out.println("getting doc type " + System.currentTimeMillis());
		DocumentType documentType = DocumentType.findDocumentType(DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID);
		System.out.println("getting doc" + System.currentTimeMillis());
		
		Document document = Document.findDocument(company, documentType, year, quarter).get(0);
		System.out.println("getting doc data" + System.currentTimeMillis());
		
		InputStream is = documentDataService.getDocumentData(document.getCompany().getId(), document.getId());
          
		
		//SpreadService spreadService = new SpreadService();
		SpreadService2 spreadService2 = new SpreadService2();
		
		List<MappedPage> source = null;
			
		try {
			System.out.println("spreading" + System.currentTimeMillis());

			//source = spreadService.getWorkbookAsMappedPages(is);
			source = spreadService2.getWorkbookAsMappedPages(document.getFileName(), is);
			System.out.println("done spreading" + System.currentTimeMillis());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return source;
	}	


	
}
