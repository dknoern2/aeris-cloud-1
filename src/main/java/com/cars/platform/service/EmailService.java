package com.cars.platform.service;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cars.platform.util.PropertiesHelper;

@Service
public class EmailService {

	private static final Logger logger = LoggerFactory
			.getLogger(ReminderService.class);

	@Autowired
	PropertiesHelper propertyHelper;

	private Boolean isEmailSending = false;

	@PostConstruct
	private void init() {
		isEmailSending = propertyHelper.getBooleanProperty("email.enabled");

		if (isEmailSending)
		{
			String toAddressOverride = propertyHelper.getProperty("email.toAddressOverride");
			if (null != toAddressOverride) {
				System.out.println("Emails will be sent to " + toAddressOverride);        		
			}
			else {
				System.out.println("Emails will be sent");
			}
		}
		else{
			System.out.println("Emails will not send");
		}

	}

	public static void main(String[] args) {
		EmailService emailService = new EmailService();
		emailService.sendEmail("david@seattleweb.com", "Test", "Hi there", false);
	}

	public void sendEmail(String to, String subject, String body, boolean isHtml) {

		if (isEmailSending){

			/*	        final String username = "corp\\garage";
	        final String password = "park150.CARS";
			 */	        
			final String username = propertyHelper.getProperty("email.username");
			final String password = propertyHelper.getProperty("email.password");

			//Properties props = new Properties();
			//props.put("mail.smtp.auth", "true");
			//props.put("mail.smtp.starttls.enable", "true");
			//props.put("mail.smtp.host", "exchange.corp.communitycapital.org");

			//props.put("mail.smtp.host", "108.16.232.150");
			//props.put("mail.smtp.port", "25");

			//		props.put("mail.smtp.auth", "true");
			//		props.put("mail.smtp.starttls.enable", "true");
			//		props.put("mail.smtp.host", "smtp.gmail.com");
			//		props.put("mail.smtp.port", "587");


			// we only want to send to real recipients in production
			String toAddressOverride = propertyHelper.getProperty("email.toAddressOverride");
			if (null != toAddressOverride) {
				to = toAddressOverride;
			}
			else if (!propertyHelper.isProduction()) {
				logger.error("Not sending email due to missing property email.toAddressOverride");
			}

			Session session = Session.getInstance(propertyHelper.getProperties(),
					new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(propertyHelper.getProperty("email.fromAddress")));
				message.setReplyTo(InternetAddress.parse(propertyHelper.getProperty("email.replyToAddress")));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(to));
				message.setSubject(subject);
				if (isHtml) {
					message.setContent(body, "text/html");				
				}
				else {
					message.setText(body);
				}

				Transport.send(message);

			} catch (MessagingException e) {
				logger.error("Unable to send email to: " + to + ". reason: " + e.getMessage());
			}
		}
		else{
			System.out.println("Email disabled: would be sent to: " + to + " with subject: " + subject);
		}
	}
}
