package com.cars.platform.service.export;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricValue;
import com.smartxls.WorkBook;

public class MetricReportService {
	
	private OutputStream outputStream;
	private WorkBook wb;
	private Map<Metric, Map<Company, Integer>> countByCompanyByMetric = new HashMap<Metric, Map<Company, Integer>>();
	private TreeSet<Metric> allMetrics = new TreeSet<Metric>();
	private TreeSet<Company> allCompanies = new TreeSet<Company>();

	public MetricReportService(OutputStream outputStream) {
		this.outputStream = outputStream;
		wb = new WorkBook();
	}
	
	public void export() throws Exception {
	   	List<MetricValue> allValues = MetricValue.findAllMetricValues();
	
		for (MetricValue mv : allValues) {
			Metric m = mv.getMetric();
			allMetrics.add(m);
			Company c = mv.getCompany();
			allCompanies.add(c);
			
			Map<Company, Integer> countByCompany = countByCompanyByMetric.get(m);
			if (null == countByCompany) {
				countByCompany = new HashMap<Company, Integer>();
				countByCompanyByMetric.put(m, countByCompany);
			}
			Integer count = countByCompany.get(c);
			if (null == count) {
				count = 0;
			}
			countByCompany.put(c, count+1);
		}
		
		generateSpread(); 
	}
	
	private void generateSpread() throws Exception {
		wb.setSheet(0);
		wb.setSheetName(0, "Metric use by company");
		int irow = 0;
		headerRow();
		irow++;
		for (Metric m : allMetrics) {
			metricRow(irow++, m);
		}
		
		wb.write(outputStream);
	}
	
	private void headerRow() throws Exception {
		int icol = 0;
		wb.setText(0, icol++, "Metric Name");
		wb.setText(0, icol++, "Metric Code");
		for (Company c : allCompanies) {
			wb.setText(0, icol++, c.getName());
		}
	}
	
	
	private void metricRow(int irow, Metric m) throws Exception {
		int icol = 0;
		wb.setText(irow, icol++, m.getName());
		wb.setText(irow, icol++, m.getFullAccountCode());
		Map<Company, Integer> countByCompany = countByCompanyByMetric.get(m);
		for (Company c : allCompanies) {
			Integer count = countByCompany.get(c);
			if (null == count) {
				count = 0;
			}
			wb.setText(irow, icol++, count.toString());
		}
	}
}


