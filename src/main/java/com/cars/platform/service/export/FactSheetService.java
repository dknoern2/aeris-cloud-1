package com.cars.platform.service.export;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletContext;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Graph;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.Person;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.service.graph.GraphService;
import com.cars.platform.util.LogoHelper;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.awt.PdfGraphics2D;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;



public class FactSheetService {
	private static final Logger logger = LoggerFactory.getLogger(FactSheetService.class);
	private Company company;
	private ServletContext servletContext;
	private GraphService graphService;
	
	private FontFamily fontFamily = FontFamily.HELVETICA;
	private Font defaultFont;
	private Document document;
	
	
	public FactSheetService(Company company, ServletContext servletContext) {
		this.company = company;
		this.servletContext = servletContext;
		this.graphService = new GraphService(company);
		this.defaultFont = new Font(fontFamily, 8, Font.NORMAL, BaseColor.BLACK);
	}
	
	public void export(OutputStream outputStream) throws Exception {
	    document = new Document();
        // step 2
		PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        // step 3
        document.open();

             
        PdfPTable tbl = new PdfPTable(2);
        tbl.setWidthPercentage(100);
        PdfPCell cell;
        Image logo = getCompanyLogo();
	    if (logo != null) {
	    	logo.scaleToFit(100, 100); 
	    	cell = new PdfPCell(logo);
        } else {
        	cell = new PdfPCell();
        }
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.disableBorderSide(Rectangle.BOX);
        tbl.addCell(cell);
	    
	    Image img = getLogo();
        if (img != null) {     	
        	img.scaleToFit(100, 100);
        	
        	cell = new PdfPCell(img);
        } else {
        	cell = new PdfPCell();
        }
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.disableBorderSide(Rectangle.BOX);
        tbl.addCell(cell);
        
        Paragraph p = new Paragraph();
        p.add(setText(company.getName(), new Font(fontFamily, 14, Font.BOLD, BaseColor.BLACK))); 
        if (null != company.getUrl()) {
        	p.add(Chunk.NEWLINE);
        	p.add(setText(company.getUrl(), new Font(fontFamily, 8, Font.ITALIC, BaseColor.BLACK)));
        }
        cell = new PdfPCell(p);
        cell.disableBorderSide(Rectangle.BOX);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tbl.addCell(cell);
           
	    if (company.isRated()) {
	    	BaseFont bf = BaseFont.createFont(servletContext.getRealPath("/fonts/arial.ttf"),BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
	    	p = new Paragraph();
	    	//p.add(setText("CARS\u00ae Rated", new Font(bf, 14, Font.BOLD, new BaseColor(153, 27, 30))));
	    	p.add(setText("Aeris Rated, Powered by CARS\u00ae", new Font(bf, 12, Font.BOLD, new BaseColor(8, 128, 150)))); //088096
	    	p.add(Chunk.NEWLINE);
	    	p.add(setText("Ratings and Analysis available with subscription", new Font(fontFamily, 8, Font.ITALIC, BaseColor.BLACK)));
	    	cell = new PdfPCell(p);
	    	cell.disableBorderSide(Rectangle.BOX);
	        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        tbl.addCell(cell);
	    } else {
	    	cell = new PdfPCell();
	    	cell.disableBorderSide(Rectangle.BOX);
	        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        tbl.addCell(cell);
	    }
        
	    cell = new PdfPCell(setText("", null));
    	cell.disableBorderSide(Rectangle.BOX);
    	cell.setMinimumHeight(20);
    	cell.setColspan(2);
        tbl.addCell(cell);
        document.add(tbl);
         
        String description = company.getDescription();
        if (null != description && !StringUtils.isBlank(description)) {
        	p = new Paragraph();
        	p.setLeading(10);
			p.add(new Phrase("Description:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
			p.add(new Phrase(description, defaultFont));	
        	document.add(p);
        	document.add(Chunk.NEWLINE);
        }
                
        renderAttributes();
        document.add(Chunk.NEWLINE);
        
        PdfPTable table = renderTable("Impact Highlights", 22000);
        if (table != null) {
        	document.add(table);
	        document.add(Chunk.NEWLINE);
	        document.add(Chunk.NEWLINE);
	        document.add(Chunk.NEWLINE);
        }
        table = renderTable("Financial Highlights", 21000);
        if (table != null) {
        	document.add(table);
        	document.add(Chunk.NEWLINE);
        	document.add(Chunk.NEWLINE);
        }
        
        renderGraph("Test", 20000, writer);
        
        p = new Paragraph();
        document.add(Chunk.NEWLINE);
		p.setLeading(10);
		p.add(new Phrase("Information presented is not in lieu of investor due diligence. Contact info@AerisInsight.com for additional data and analytics.", 
				new Font(fontFamily, 8, Font.BOLDITALIC, BaseColor.RED)));
		document.add(p);
        
        
        // step 5
        document.close();

	}
	
	private Paragraph setText(String text, Font font) {
		Paragraph p = new Paragraph();
		p.setLeading(10);
		if (font == null) {
			font = this.defaultFont;
		}
    	p.setFont(font);
    	p.add(text);
    	return p;
	}
	
	private PdfPTable renderTable(String title, int graphCode) throws Exception {
		Graph domainTable = Graph.findGraphByCodeAndCompany(graphCode, company);
		if (null == domainTable)
			return null;
		
		if (domainTable.getGraphType() != GraphType.TABLE) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainTable.getId() + " is not of type TABLE");
			return null;
		}
		
		domainTable.setShowInterim(false);
		domainTable.setShowYears(5);
		domainTable.setSupressExtraFYE(true);
		domainTable.setForFactSheet(true);
		com.cars.platform.viewmodels.graph.Graph vmGraph = graphService.getGraph(domainTable, false);
		if (vmGraph == null)
			return null;
		if (!(vmGraph instanceof com.cars.platform.viewmodels.graph.Table)) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainTable.getId() + " does not render as a table");
			return null;
		}
		
		document.add(new Phrase(5, title, new Font(fontFamily, 10, Font.BOLD, BaseColor.BLACK)));
		
		Table table = (Table)vmGraph;
		
		final int columnCount = table.getColumnHeaders().getValues().size() + 1;
		float[] columnWidths = new float[columnCount];
		columnWidths[0] = 60f;
		for (int i=1; i<columnCount; i++) {
			columnWidths[i] = 30f;
		}
		
		PdfPTable t = new PdfPTable(columnCount);
		
	    t.setWidthPercentage(100);
		t.setWidths(columnWidths);

		//Building Column Header	
		Row header = table.getColumnHeaders();
		Font font = new Font(this.defaultFont);
		font.setStyle("bold");
		fillRow(t, header, font, BaseColor.LIGHT_GRAY);
		// data rows
		t.getDefaultCell().setBackgroundColor(null);
		for (Row dataRow : table.getDatas()) {
			fillRow(t, dataRow, this.defaultFont, null);
		}
		return t;
	}
	
	private void fillRow(PdfPTable table, Row row, Font font, BaseColor color) throws Exception {
		Phrase p = new Phrase(row.getLabel(), font);
		PdfPCell cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBackgroundColor(color);
        table.addCell(cell);
		
        // The graph processor should always give us 5 values but in case it doesn't we'll show only the last 5 values
        // better than the tear sheet being mal-formed
        List<Value> values = row.getValues();
        int startIndex = Math.max(0, values.size() - 5);
		for (int i = startIndex; i< values.size(); i++) {
			Value value = values.get(i);
			if (null != value.getValue()) { // TODO what happens if the row has 1 null value? shouldn't we add a 'blank' cell anyway
				p = new Phrase(value.getValue(), font);
				cell = new PdfPCell(p);
		        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		        cell.setBackgroundColor(color);
		        table.addCell(cell);
			}				
		}
	}
	
	private void renderGraph(String title, int graphCode, PdfWriter writer) throws Exception {
		Graph domainGraph = Graph.findGraphByCodeAndCompany(graphCode, company);
		if (null == domainGraph)
			return;
		
		if (domainGraph.getGraphType() != GraphType.COLUMN && domainGraph.getGraphType() != GraphType.LINE) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainGraph.getId() + " is not of type COLUMN or LINE");
			return;
		}

		domainGraph.setShowInterim(false);
		domainGraph.setShowYears(5);
		domainGraph.setSupressExtraFYE(true);
		com.cars.platform.viewmodels.graph.Graph vmGraph = graphService.getGraph(domainGraph, false);
		if (!(vmGraph instanceof com.cars.platform.viewmodels.graph.Chart)) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainGraph.getId() + " does not render as a chart");
			return;
		}
		

		if (vmGraph.getType() == GraphType.COLUMN) {
			DefaultCategoryDataset  dataset = new DefaultCategoryDataset();
			List<Row> rows = vmGraph.getDatas();
			for (int i=0; i<rows.size(); i++) {
				List<Value> values = rows.get(i).getValues();
				for (int j=0; j<values.size(); j++) {
					dataset.setValue(values.get(j).getAmount(), rows.get(i).getLabel(), vmGraph.getColumnHeaders().getValues().get(j).getValue());
				}
			}
			
			JFreeChart chart = ChartFactory.createBarChart(vmGraph.getTitle(), null, null, dataset);
			chart.getPlot().setBackgroundPaint(Color.white);
			
			chart.getLegend().setFrame(BlockBorder.NONE);
			chart.getLegend().setItemFont(new java.awt.Font("Helvetica", Font.NORMAL, 6));
			chart.getTitle().setFont(new java.awt.Font("Helvetica", Font.NORMAL, 10));
			
			final CategoryPlot plot = chart.getCategoryPlot();
			plot.setRangeGridlinePaint(Color.DARK_GRAY);
			Axis xAxis = plot.getDomainAxis();
			xAxis.setLabelFont(new java.awt.Font("Helvetica", Font.NORMAL, 6));
			xAxis.setTickLabelFont(new java.awt.Font("Helvetica", Font.NORMAL, 6));
			Axis yAxis = plot.getRangeAxis();
			yAxis.setLabelFont(new java.awt.Font("Helvetica", Font.NORMAL, 6));
			yAxis.setTickLabelFont(new java.awt.Font("Helvetica", Font.NORMAL, 6));
	        plot.setNoDataMessage("NO DATA!");
	       // disable bar outlines...
	        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
	        renderer.setItemMargin(0.04);
	        renderer.setBarPainter(new StandardBarPainter());
	        renderer.setDrawBarOutline(false);
	        renderer.setSeriesPaint(0, new Color(192, 154, 77));
	        renderer.setSeriesPaint(1, new Color(8, 128, 150));
	        renderer.setSeriesPaint(2, new Color(158, 181, 121));
	        renderer.setSeriesPaint(3, new Color(13, 46, 61));
	        renderer.setSeriesPaint(4, new Color(203, 226, 168));
	        renderer.setSeriesPaint(5, new Color(72, 72, 72));
	        renderer.setSeriesPaint(6, new Color(154, 153, 152));
	        renderer.setSeriesPaint(7, new Color(188, 187, 185));
	        
	        PdfContentByte cb = writer.getDirectContent();
	        float width = PageSize.A4.getWidth()-60;
	        float height = 200;
	        PdfTemplate bar = cb.createTemplate(width, height);
	        Graphics2D g2d2 = new PdfGraphics2D(bar, width, height);
	        Rectangle2D r2d2 = new Rectangle2D.Double(0, 0, width, height); 
	       	if (chart!=null)
	        	chart.draw(g2d2, r2d2);	        

			Image chartImage = Image.getInstance(bar);
			document.add(chartImage);
			g2d2.dispose();
			
			String notes = domainGraph.getNotes();
			if (!StringUtils.isBlank(notes)) {
				Paragraph p = new Paragraph();
				p.setLeading(10);
				p.add(new Phrase(notes, defaultFont));	
				document.add(p);
			}
		}
	}
	
	private Image getLogo() {
		String fileName = "/images/logo/logo24.png";
    	String pathname = servletContext.getRealPath(fileName);
        Image img = null;
        try {
        	img = Image.getInstance(pathname);      	
        } catch(Exception ex) {
        	logger.error("Can't read logo: " + pathname);
			return  null;
        }
        return img;
	}
	
	private Image getCompanyLogo() {    
		com.cars.platform.domain.Document logo = LogoHelper.getCorporateLogo(company);
		if (null == logo) {
			return null;
		}
		
		DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
		
		byte[] bytes;
		Image img=null;
		try {
	        InputStream is = documentDataService.getDocumentData(company.getId(), logo.getId());  
			bytes = IOUtils.toByteArray(is);
			 img =  Image.getInstance(bytes);
		} catch (Exception e) {
			logger.error("Can't read company logo: " + logo.getFileName());
			return  null;
		}
		
		return img;
	}
	
	private void renderAttributes() throws Exception {
		String value = company.getFiscalYearEndString();
		if (!StringUtils.isBlank(value)) {
			Paragraph p = new Paragraph();
			p.setLeading(10);
			p.add(new Phrase("FYE:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
			p.add(new Phrase(value, defaultFont));
			document.add(p);
		}
		
		if (null != company.getTaxType()) {
			Paragraph p = new Paragraph();
			p.setLeading(10);
			p.add(new Phrase("TaxType:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
			p.add(new Phrase(company.getTaxType().getValue(), defaultFont));	
			document.add(p);
		}
		
	    value = company.getAreaServedString();
		if (!StringUtils.isBlank(value)) {
			Paragraph p = new Paragraph();
			p.setLeading(10);
			p.add(new Phrase("Areas Served:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
			p.add(new Phrase(value, defaultFont));
			document.add(p);
		}
		
		value = company.getLendingTypeString();
		if (!StringUtils.isBlank(value)) {
			Paragraph p = new Paragraph();
			p.setLeading(10);
			p.add(new Phrase("Lending Types:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
			p.add(new Phrase(value, defaultFont));	
			document.add(p);
		}
		
		value = company.getSectoralFocusString();
		if (!StringUtils.isBlank(value)) {
			Paragraph p = new Paragraph();
			p.setLeading(10);
			p.add(new Phrase("Impact Areas:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
			p.add(new Phrase(value, defaultFont));	
			document.add(p);
		}
		
		List<Person> contacts = Person.findPeopleByCompany(company).getResultList();
		if (null != contacts && contacts.size() > 0) {
			for (Person person : contacts) {
				if (person.isPublicContact()) {
					String info = person.getFullName();
					if (StringUtils.isNotBlank(person.getEmail())) {
						info = info + ", " + person.getEmail();
					}
					if (StringUtils.isNotBlank(person.getPhone())) {
						info = info + ", " + person.getPhone();
					}
					Paragraph p = new Paragraph();
					p.setLeading(10);
					p.add(new Phrase("Contact:  ", new Font(fontFamily, 8, Font.BOLD, BaseColor.BLACK)));
					p.add(new Phrase(info, defaultFont));	
					document.add(p);
					break;
				}
			}
		}	
		
	}
}
