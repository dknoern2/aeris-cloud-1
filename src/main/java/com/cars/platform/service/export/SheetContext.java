package com.cars.platform.service.export;

/**
 * Class holding various indexes necessary to position and number graphs and figures
 * @author kurtl
 *
 */
class SheetContext {
	int sheetNumber;
	int tableNumber;
	int figureNumber;
	int irow;
	
	public SheetContext(int sheetNumber, int tableNumber, int figureNumber, int irow)
	{
		super();
		this.sheetNumber = sheetNumber;
		this.tableNumber = tableNumber;
		this.figureNumber = figureNumber;
		this.irow = irow;
	}
}
