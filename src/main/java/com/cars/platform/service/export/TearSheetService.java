package com.cars.platform.service.export;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.Graph;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.Person;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.service.graph.GraphService;
import com.cars.platform.util.ExcelHelper;
import com.cars.platform.util.LogoHelper;
import com.cars.platform.util.SmartXLSHelper;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.viewmodels.graph.Chart;
import com.cars.platform.viewmodels.graph.Table;
import com.smartxls.ChartShape;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;
import com.smartxls.PictureShape;

/**
 * This was a failed attempt to use smartxls export to pdf capability. Too many shortfalls so we switched to itext
 * see FactSheetService
 * @author kurtl
 */
@Deprecated
public class TearSheetService extends ExportService {
	
    private static final Logger logger = LoggerFactory.getLogger(TearSheetService.class);

	private static int CARS_COLOR = 0x00991B1E;
    
	private static int NUM_COLUMNS = 7;
	private static int COL_WIDTH = 3650; //3350; // 1.0 inches, widths are 3350*inches
	private static int COL_0_WIDTH = 120;
	private static int BLANK_ROW_HEIGHT = 180;
	
	private Company company;
	
	private ServletContext servletContext;
	
	private GraphService graphService;
	
	private SmartXLSHelper smartXlsHelper;
	
	private WorkBook wb;
	
	private int irow;
	
	private int autosizeSheet = 1;
	
	private int columnWidth;
	
	private Integer printScale;
	
	private boolean xls;
		
	public TearSheetService(Company company, ServletContext servletContext, Integer exportParam) {
		this.company = company;
		this.servletContext = servletContext;
		this.graphService = new GraphService(company);
		this.smartXlsHelper = new SmartXLSHelper();
		
		if (null == exportParam) {
			this.columnWidth = COL_WIDTH;
		}
		else {
			this.columnWidth = exportParam;
		}
		//this.columnWidth = COL_WIDTH;
		
//		this.printScale = exportParam;
		this.printScale = null;
	}
	
	public void export(OutputStream outputStream, boolean xls) throws Exception {
		this.xls = xls;
		wb = new WorkBook();
		wb.setNumSheets(11); // separate sheets for each autosize and the graph image. these are not displayed
		wb.setSheetName(0, "Fact Sheet");		
		wb.setDefaultFontName("Calibri");
		wb.setPrintHeader("");
		wb.setPrintFooter("");
//		System.out.println("left margin: " + wb.getPrintLeftMargin());// .75 inches
//		System.out.println("right margin: " + wb.getPrintRightMargin());// .75 inches
//		System.out.println("top margin: " + wb.getPrintTopMargin()); // 1.0
//		System.out.println("bottom margin: " + wb.getPrintBottomMargin()); // 1.0
		wb.setPrintTopMargin(0.25);
		wb.setPrintBottomMargin(0.25);
		wb.setPrintRightMargin(0.25);
		wb.setPrintPaperSize(11906, 16838); // A4
		wb.setColWidth(0, COL_0_WIDTH); // to align text with left border of tables
		wb.setColWidth(1, 2*columnWidth);
		for (int icol = 2; icol < NUM_COLUMNS; icol++) {
			wb.setColWidth(icol, columnWidth);
		}
		
				
//		int totalWidth = 0;
//		for (int i=0; i< NUM_COLUMNS; i++) {
//			totalWidth += wb.getColWidth(i);
//		}	
//		int shouldBeWidth = COL_WIDTH*(NUM_COLUMNS) + COL_0_WIDTH;
		
		RangeStyle rangeStyle;

		wb.setSheet(0);
		irow = 0;
		
		// Add the CARS logo
		byte[] bytes = getLogoBytes();
		if (null != bytes) {
			PictureShape ps = wb.addPicture(5.0, 0.0, 7.0, 3.0, bytes);			
		}

		// Add the Company's logo
		bytes = getCompanyLogoBytes();
		if (null != bytes) {
			PictureShape ps = wb.addPicture(0.0, 0.0, 1.4, 3.0, bytes);			
		}
		
		irow = 4;
		
		// Can't wrap Company name due to large font, in pdf export it never works out correctly
		wb.setText(irow,  0, company.getName());
		if (company.getName().length() > 50) {
		    setStyleForCell(irow, 0, true, false, 13, null);
		}
		else {
		    setStyleForCell(irow, 0, true, false, 14, null);
		}
		//mergeAndWrapRow(irow, NUM_COLUMNS-3);
		rangeStyle = wb.getRangeStyle(irow, 0, irow, NUM_COLUMNS-3);
		rangeStyle.setMergeCells(true);
		rangeStyle.setIndent(0);
		rangeStyle.setHorizontalAlignment(RangeStyle.HorizontalAlignmentLeft);
		rangeStyle.setVerticalAlignment(RangeStyle.VerticalAlignmentCenter);
		wb.setRangeStyle(rangeStyle, irow, 0, irow, NUM_COLUMNS-3);
		if (company.isRated()) {
			wb.setText(irow, NUM_COLUMNS-1, "CARS� Rated");
			rangeStyle = wb.getRangeStyle(irow, NUM_COLUMNS-1, irow, NUM_COLUMNS-1);
			rangeStyle.setFontBold(true);
			rangeStyle.setFontColor(CARS_COLOR);
			rangeStyle.setHorizontalAlignment(RangeStyle.HorizontalAlignmentRight);
			rangeStyle.setFontSize(14*20);
			wb.setRangeStyle(rangeStyle, irow, NUM_COLUMNS-1, irow, NUM_COLUMNS-1);
		}
		irow++;
		
		boolean rowRendered = false;
		if (null != company.getUrl()) {
			wb.setText(irow, 0, company.getUrl());
			rangeStyle = wb.getRangeStyle(irow, 0, irow, NUM_COLUMNS-4);
			rangeStyle.setMergeCells(true);
			rangeStyle.setIndent(0);
			rangeStyle.setHorizontalAlignment(RangeStyle.HorizontalAlignmentLeft);
			rangeStyle.setVerticalAlignment(RangeStyle.VerticalAlignmentCenter);
			wb.setRangeStyle(rangeStyle, irow, 0, irow, NUM_COLUMNS-4);
			//mergeAndWrapRow(irow, NUM_COLUMNS-4);
			rowRendered = true;
		}
		if (company.isRated()) {
			if (xls) {
				wb.setText(irow, NUM_COLUMNS-3, "Ratings and Analysis available with subscription");
			}
			else {
				//right alignment not working properly for PDF export... pad with 4 spaces to get close
				wb.setText(irow, NUM_COLUMNS-3, "Ratings and Analysis available with subscription    ");
			}
			rangeStyle = wb.getRangeStyle(irow, NUM_COLUMNS-3, irow, NUM_COLUMNS-1);
			rangeStyle.setMergeCells(true);
			rangeStyle.setHorizontalAlignment(RangeStyle.HorizontalAlignmentRight);
			rangeStyle.setFontItalic(true);
			wb.setRangeStyle(rangeStyle, irow, NUM_COLUMNS-3, irow, NUM_COLUMNS-1);
			rowRendered = true;
		}
		if (rowRendered) {
		    irow++;
		}

		
		skipRow();
		
		if (null != company.getMission()) {
			wb.setText(irow, 0, "Mission:  " + company.getMission());
			boldCellPrefix(irow, 0, 8);
			mergeAndWrapRow(irow);
			irow++;
			skipRow();
		}
		
		wb.setText(irow, 0, "Description:   " + company.getDescription());
		boldCellPrefix(irow, 0, 12);
		mergeAndWrapRow(irow);
		irow++;
		skipRow();
		
		renderAttributes();
		irow++;
		
		renderTable("Impact Highlights", 1000);
		
		renderTable("Financial Highlights", 1000);

		renderGraph("Expenses", 18000);
		
		wb.setText(irow, 0, "Information presented is not in lieu of investor due diligence. Contact info@AerisInsight.com for additional data and analytics.");
		setStyleForCell(irow, 0, true, true, 10, 0x00c00000);
		mergeAndWrapRow(irow);
		irow++;

		wb.setPrintArea(ExcelHelper.getRange(0, 0, irow+2, NUM_COLUMNS-1));
		
// This fits to a single page but also seems to maintain the ratio between hight and width. at any rate it didn't work
//		wb.setPrintScaleFitHPages(1);
//		wb.setPrintScaleFitVPages(1);
//		wb.setPrintScaleFitToPage(true);
		if (null != printScale) {
			wb.setPrintScale(printScale);
		}
		
		// uncomment to see where borders lie
//		RangeStyle rs = wb.getRangeStyle(0, 0, irow, NUM_COLUMNS-1);
//		rs.setRightBorder(RangeStyle.BorderHair);
//		rs.setLeftBorder(RangeStyle.BorderHair);
//		wb.setRangeStyle(rs, 0, 0, irow, NUM_COLUMNS-1);
//		wb.setPrintGridLines(true);
		wb.setShowGridLines(false);

		try {
			
			wb.setNumSheets(1);
			if (xls) {
				wb.write(outputStream);
			}
			else {
				exportToPDF(outputStream);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void exportToPDF(OutputStream outputStream) throws Exception {
		wb.exportPDF(outputStream);
	}

	private void skipRow() throws Exception {
		wb.setRowHeight(irow, BLANK_ROW_HEIGHT);
		irow++;
	}


	
	private byte[] getLogoBytes() throws Exception {
		String fileName = "/images/logo/logo24.png";
    	String pathname = servletContext.getRealPath(fileName);
    	InputStream is;
		try {			
			is = new FileInputStream(pathname);

		} catch (FileNotFoundException e) {
			logger.error("Can't find logo: " + fileName);
			return null;
		}
		
		byte[] bytes;
		try {
			bytes = IOUtils.toByteArray(is);
		} catch (IOException e) {
			logger.error("Can't read logo: " + fileName);
			return  null;
		}
		
		return bytes;
	}
	
	
	private byte[] getCompanyLogoBytes() {
		Document logo = LogoHelper.getCorporateLogo(company);
		if (null == logo) {
			return null;
		}
		
		DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
		
		byte[] bytes;
		try {
	        InputStream is = documentDataService.getDocumentData(company.getId(), logo.getId());  
			bytes = IOUtils.toByteArray(is);
		} catch (IOException e) {
			logger.error("Can't read company logo: " + logo.getFileName());
			return  null;
		}
		
		return bytes;
	}
	
	
	private void renderAttributes() throws Exception {
		String value = company.getAreaServedString();
		if (!StringUtils.isBlank(value)) {
			wb.setText(irow, 0, "Areas Served:  " + value);
			boldCellPrefix(irow, 0, 14);
			mergeAndWrapRow(irow);
			irow ++;
		}
		
		value = company.getLendingTypeString();
		if (!StringUtils.isBlank(value)) {
			wb.setText(irow, 0, "Lending Types:  " + value);
			boldCellPrefix(irow, 0, 14);
			mergeAndWrapRow(irow);
			irow ++;
		}
		
		value = company.getSectoralFocusString();
		if (!StringUtils.isBlank(value)) {
			wb.setText(irow, 0, "Sectoral Focus:  " + value);
			boldCellPrefix(irow, 0, 15);
			mergeAndWrapRow(irow);
			irow ++;
		}
		
		if (null != company.getTaxType()) {
			wb.setText(irow, 0, "TaxType:  " + company.getTaxType());
			boldCellPrefix(irow, 0, 8);
			mergeAndWrapRow(irow);
			irow ++;
		}
		
		List<Person> contacts = Person.findPeopleByCompany(company).getResultList();
		if (null != contacts && contacts.size() > 0) {
			for (Person person : contacts) {
				if (person.isPublicContact()) {
					String info = person.getFullName();
					if (StringUtils.isNotBlank(person.getEmail())) {
						info = info + ", " + person.getEmail();
					}
					if (StringUtils.isNotBlank(person.getPhone())) {
						info = info + ", " + person.getPhone();
					}
					wb.setText(irow, 0, "Contact:  " + info);
					boldCellPrefix(irow, 0, 8);
					mergeAndWrapRow(irow);
					irow ++;
					break;
				}
			}
		}	
	}

	private void renderTable(String title, int graphCode) throws Exception {
		Graph domainTable = Graph.findGraphByCodeAndCompany(graphCode, company);
		if (null == domainTable)
			return;
		
		if (domainTable.getGraphType() != GraphType.TABLE) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainTable.getId() + " is not of type TABLE");
			return;
		}
		
		domainTable.setShowInterim(false);
		domainTable.setShowYears(5);
		com.cars.platform.viewmodels.graph.Graph vmGraph = graphService.getGraph(domainTable, false);
		if (!(vmGraph instanceof com.cars.platform.viewmodels.graph.Table)) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainTable.getId() + " does not render as a table");
			return;
		}
		
		wb.setText(irow,  0, title);
		setStyleForCell(irow, 0, true, true, 12, null);
		irow++;
		
		Table table = (Table)vmGraph;
		
		SheetContext sheetContext = new SheetContext(0, 0, 0, irow);
		TableProcessor processor = new TableProcessor(table, wb, sheetContext);
		processor.setRenderTitle(false);
		processor.setStartCol(1);
		sheetContext = processor.process();
		irow = sheetContext.irow-1;
	}

	
	private void renderGraph(String title, int graphCode) throws Exception {
		Graph domainGraph = Graph.findGraphByCodeAndCompany(graphCode, company);
		if (null == domainGraph)
			return;
		
		if (domainGraph.getGraphType() != GraphType.COLUMN && domainGraph.getGraphType() != GraphType.LINE) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainGraph.getId() + " is not of type COLUMN or LINE");
			return;
		}

		com.cars.platform.viewmodels.graph.Graph vmGraph = graphService.getGraph(domainGraph, false);
		if (!(vmGraph instanceof com.cars.platform.viewmodels.graph.Chart)) {
			logger.warn("Skipping rendering of " + title + " as graph id " + domainGraph.getId() + " does not render as a chart");
			return;
		}
		
		wb.setText(irow,  0, title);
		setStyleForCell(irow, 0, true, true, 12, null);
		irow++;
		
		Chart chart = (Chart)vmGraph;
		
		wb.setSheet(autosizeSheet++);
		SheetContext sheetContext = new SheetContext(0, 0, 0, 0);
		@SuppressWarnings("rawtypes")
		ChartProcessor processor = (ChartProcessor)(GraphProcessor.getProcessor(chart, wb, sheetContext));
		processor.setRenderTitle(false);
		sheetContext = processor.process();
		
		ChartShape chartShape = processor.getChart();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		chartShape.writeChartAsPNG(wb, baos);
		byte[] chartBytes = baos.toByteArray();
		
		wb.setSheet(0);
		
		PictureShape ps = wb.addPicture(1.0, irow, NUM_COLUMNS, irow+14, chartBytes);			

		irow += 15;
	}
	
	private void setStyleForCell(int irow, int icol, boolean bold, boolean italic, int fontSize, Integer color) {
		RangeStyle rangeStyle;
		try {
			rangeStyle = wb.getRangeStyle(irow, icol, irow, icol);
			rangeStyle.setFontBold(bold);
			rangeStyle.setFontItalic(italic);
			rangeStyle.setFontSize(fontSize*20);
			if (null != color) {
				rangeStyle.setFontColor(color);
			}
			wb.setRangeStyle(rangeStyle, irow, icol, irow, icol);
		} catch (Exception ignore) {
		}
	}

	private void boldCellPrefix(int row, int col, int length) throws Exception {
		smartXlsHelper.boldCellPrefix(wb, row, col, length);
	}
	
	private void mergeAndWrapRow(int row) throws Exception {
		// This should be NUM_COLUMNS-1 but when exporting to PDF the text bleeds over to the next cell
		// TODO remove xls check at some point
		if (xls) {
			mergeAndWrapRow(row, NUM_COLUMNS-1);
		}
		else {
			mergeAndWrapRow(row, NUM_COLUMNS-2);
		}
	}
	
	private void mergeAndWrapRow(int row, int lastCol) throws Exception {
		RangeStyle rangeStyle = wb.getRangeStyle(row, 0, row, lastCol);
		rangeStyle.setMergeCells(true);
		rangeStyle.setIndent(0);
		rangeStyle.setWordWrap(true);
		rangeStyle.setHorizontalAlignment(RangeStyle.HorizontalAlignmentLeft);
		rangeStyle.setVerticalAlignment(RangeStyle.VerticalAlignmentCenter);
		wb.setRangeStyle(rangeStyle, row, 0, row, lastCol);
		// Autosize doesn't work for merged or wrapped cells
		//wb.setRowHeightAutoSize(row, true);
		wb.setRowHeight(row, getRowHeight(row, lastCol));
	}

	// copies the text and range style to a separate sheet which is set to autosize but does not have merged cells
	// autosize does not work on merged cells
	private int getRowHeight(int row, int lastCol) throws Exception {
		String rowText = wb.getText(row, 0); // this is a merged cell in sheet 0
		RangeStyle rangeStyle = wb.getRangeStyle(row, 0, row, 0);
		
		wb.setSheet(autosizeSheet);
		rangeStyle.setMergeCells(false);		
		wb.setColWidth(0, columnWidth*(lastCol+1) + COL_0_WIDTH); // column 1 is 2x width
		rangeStyle.setWordWrap(true);
		wb.setRangeStyle(rangeStyle, 0, 0, 0, 0);
		wb.setText(0, 0, rowText);
		wb.setRowHeightAutoSize(0, true);
		
		int rowHeight = wb.getRowHeight(0);
		
		autosizeSheet++;
		
		/*
		 * Yes this is incredibly heinous. the autosizing gets you kind of close. These values
		 * were arrived at empirically yet surely there are cases where text still gets truncated.
		 * 
		 * There are 15 twips in a pixel, A typical 'line' is 17 pixels at the default font
		 */
		double lines = (rowHeight / 15.0) / 17;
		int iPart = (int) lines;
		double dPart = lines - iPart;
		int finalHeight;
		if (lines > 2) { // Description or Mission (typically)
			if (dPart < 0.5) {
				finalHeight = ((iPart+1) * 15 * 17) + 40;
			}
			else { // 
				finalHeight = ((iPart+2) * 15 * 17) + 40;
			}			
		}
		else { // for 1 or 2 line entries rounding up two causes too much white space
			if (dPart > 0.25) {
				finalHeight = ((iPart+1) * 15 * 17) + 40;
			}
			else {
				finalHeight = rowHeight + 40;
			}
		}
		
//		System.out.println("text: " + rowText);
//		System.out.println("height: " + rowHeight + " finalHeight: " + finalHeight + " dPart: " + dPart + " colWidth: " + wb.getColWidth(0) + " col: " + lastCol);
		
		wb.setSheet(0);
	
		// TODO decide on xls -vs- pdf
		if (xls) {
			return rowHeight;
		}
		else {
			return finalHeight;
		}
	}

}
