package com.cars.platform.service.export;

import java.awt.Color;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.util.ExcelHelper;
import com.cars.platform.viewmodels.graph.BarChart;
import com.cars.platform.viewmodels.graph.Graph;
import com.cars.platform.viewmodels.graph.LineChart;
import com.cars.platform.viewmodels.graph.PieChart;
import com.cars.platform.viewmodels.graph.Table;
import com.smartxls.ChartShape;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;

abstract public class GraphProcessor<T extends Graph> {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(GraphProcessor.class);

		public GraphProcessor(T graph, WorkBook wb, SheetContext sheetContext)		{
			this.graph = graph;
			this.wb = wb;
			this.sheetContext = sheetContext;
		}
		
		protected SheetContext sheetContext;
		protected T graph;
		protected WorkBook wb;
		private Range tableRange = null;
		private boolean renderTitle = true;
		private int startCol = 1;
		private ChartShape chart = null;
		
		
		//static GraphProcessor<Graph> getProcessor(BreakdownGraph breakdownGraph,
		@SuppressWarnings("rawtypes")
		static GraphProcessor getProcessor(Graph graph, WorkBook wb, SheetContext sheetContext)
		{
			if (graph instanceof Table) {
				return new TableProcessor((Table)graph, wb, sheetContext);
			}
			else if (graph instanceof BarChart) {
				return new BarChartProcessor((BarChart)graph, wb, sheetContext);
			}
			else if (graph instanceof PieChart) {
				return new PieChartProcessor((PieChart)graph, wb, sheetContext);
			}
			else if (graph instanceof LineChart) {
				return new LineChartProcessor((LineChart)graph, wb, sheetContext);
			}
			else {
				throw new UnsupportedOperationException("Can't handle this type of graph: " + graph.getClass().getName());
			}
		}
		
		
		public boolean isRenderTitle() {
			return renderTitle;
		}


		public void setRenderTitle(boolean renderTitle) {
			this.renderTitle = renderTitle;
		}


		public int getStartCol() {
			return startCol;
		}


		public void setStartCol(int startCol) {
			this.startCol = startCol;
		}


		public ChartShape getChart() {
			return chart;
		}


		SheetContext process() throws Exception {
			buildTable();
			
			if (createsFigure()) {
				// figure's don't render if data rows are hidden.... dorky excel
//				for (int i=tableRange.row1; i<=tableRange.row2; i++) {
//				    wb.setRowHidden(i, true);
//				}
				buildFigure();
			}
			
			return sheetContext;
		}
		
		
		protected void buildTable() throws Exception {
			if (isRenderTitle()) {
				wb.setText(sheetContext.irow, 0, "Table " + sheetContext.tableNumber + ". " + graph.getTitle());
				setBoldForCell(sheetContext.irow, 0);
				sheetContext.irow = sheetContext.irow + 2;
			}
			
			tableRange = fillTableData(getStartCol());
			
			formatTable(tableRange);
			buildNotes();

			sheetContext.tableNumber++;			
		}
		
		
		protected void formatTable(Range tableRange) throws Exception {
			int tableLeft = tableRange.col1;
			int tableTop = tableRange.row1;
			int tableRight = tableRange.col2;
			int tableBottom = tableRange.row2;
				
			// borders for table
			RangeStyle tableStyle = wb.getRangeStyle(tableTop, tableLeft, tableBottom, tableRight);
			tableStyle.setTopBorder(RangeStyle.BorderMedium);
			tableStyle.setBottomBorder(RangeStyle.BorderMedium);
			tableStyle.setLeftBorder(RangeStyle.BorderMedium);
			tableStyle.setRightBorder(RangeStyle.BorderMedium);
			tableStyle.setHorizontalInsideBorder(RangeStyle.BorderThin);
			tableStyle.setVerticalInsideBorder(RangeStyle.BorderThin);
			wb.setRangeStyle(tableStyle, tableTop, tableLeft, tableBottom, tableRight);

			// fill header row
			RangeStyle headerStyle = wb.getRangeStyle(tableTop, tableLeft, tableTop, tableRight);

			headerStyle.setPattern(RangeStyle.PatternSolid);
			headerStyle.setPatternBG(Color.lightGray.getRGB());
			headerStyle.setPatternFG(Color.lightGray.getRGB());
			headerStyle.setHorizontalAlignment(RangeStyle.HorizontalAlignmentCenter);
			headerStyle.setWordWrap(true);
			headerStyle.setFontBold(true);
			wb.setRangeStyle(headerStyle, tableTop, tableLeft, tableTop, tableRight);			
		}
		
		
		protected String getTableRangeAsString() {
			return ExcelHelper.getRange(tableRange.row1, tableRange.col1, tableRange.row2, tableRange.col2);
		}
		
		
		protected Range getTableRange() {
			return tableRange;
		}
		
		
		protected void buildFigure() throws Exception {
			if (isRenderTitle()) {
				wb.setText(sheetContext.irow, 0, "Figure " + sheetContext.figureNumber + ". " + graph.getTitle());
				setBoldForCell(sheetContext.irow, 0);
				sheetContext.irow = sheetContext.irow + 2;
			}

			int chartLeft = 1;
			int chartRight = 10;
			int chartTop = sheetContext.irow;
			int chartBottom = sheetContext.irow + 16;

			sheetContext.irow = sheetContext.irow + 16;

			chart = wb.addChart(chartLeft, chartTop, chartRight, chartBottom);
			formatChart(chart);
			buildNotes();

			// chart.setAxisTitle(ChartShape.XAxis, 0, "X-axis data");
			// chart.setAxisTitle(ChartShape.YAxis, 0, "Y-axis data");
			
			sheetContext.figureNumber++;			
		}
		
		protected void buildNotes() throws Exception {
			if (graph.getNotes() != null && !graph.getNotes().isEmpty()) {
				wb.setText(sheetContext.irow, 1, "* " + graph.getNotes());			
			}
			sheetContext.irow = sheetContext.irow + 2;
		}
		

		abstract boolean createsFigure();
		
		abstract Range fillTableData(int startCol) throws Exception;
		
		abstract void formatChart(ChartShape chart) throws Exception;
		
		
		protected void setBoldForCell(int irow, int icol) {
			RangeStyle rangeStyle;
			try {
				rangeStyle = wb.getRangeStyle(irow, icol, irow, icol);
				rangeStyle.setFontBold(true);
				wb.setRangeStyle(rangeStyle, irow, icol, irow, icol);
			} catch (Exception ignore) {
			}
		}

	
}
