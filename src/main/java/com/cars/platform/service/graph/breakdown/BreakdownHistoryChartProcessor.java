package com.cars.platform.service.graph.breakdown;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownHistory;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.viewmodels.graph.BarChart;
import com.cars.platform.viewmodels.graph.Chart;
import com.cars.platform.viewmodels.graph.Graph;
import com.cars.platform.viewmodels.graph.LineChart;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Value;

public class BreakdownHistoryChartProcessor<U extends Graph> extends BreakdownProcessor<BreakdownHistory, Graph> {
	
    private static final Logger logger = LoggerFactory.getLogger(BreakdownHistoryChartProcessor.class);
    
    protected TreeSet<Period> periodsToShow;


	public BreakdownHistoryChartProcessor(
			Company company, BreakdownHistory breakdownGraph,
			Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod,
			MetricBreakdownItem summationItem, boolean showRatingPeriod)
	{
		super(company, breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriod);
		
		periodsToShow = getPeriodsToShow();
	}



    @Override
	protected boolean showInterim() {
    	return breakdownGraph.isShowInterim();
	}

	@Override
	protected int showYears() {
		return breakdownGraph.getShowYears();
	}

	@Override
	protected Graph createOutputGraph() {
		switch (breakdownGraph.getGraphType()) {
		case LINE : return new LineChart();
		case COLUMN : return new BarChart();
		default:
			logger.error("Unexpected Graph type: " + breakdownGraph.getGraphType());
			return null;
		}
	}


	protected Period getLatestPeriod() {
		return periodsToShow.last();
	}
    
    

	@Override
	protected List<Value> getColumnHeaderValues() {
		List<Value> columnHeaders = new ArrayList<Value>();
		for (Period period : periodsToShow) {
			String dateString = getDateString(period);
			Value value = new Value(dateString);
			columnHeaders.add(value);
		}
		return columnHeaders;
	}


	@Override
	protected List<Row> getDataRows() {
		List<Row> dataRows = new ArrayList<Row>();
		BreakdownColumn bc = breakdownGraph.getEquation();

		for (MetricBreakdownItem mbi : breakdownGraph.getBreakdown().getItems()) {
			Row row = new Row();
			row.setLabel(mbi.getName());
			ArrayList<Value> values = new ArrayList<Value>();
			boolean valueFound = false;
			for (Period period : periodsToShow) {
				Value value;
				
				Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem =
						valueByCodeByItemByPeriod.get(period);
				if (null == valueByCodeByItem.get(mbi)) {
					value = new Value("-");
				}
				else {	
					CellValue cellValue = getCellValue(bc, mbi, valueByCodeByItem, false);
					// TODO check for evaluation exception... what to do?
					value = new Value(cellValue.amount, bc, cellValue.value);
					valueFound = true;
				}
				values.add(value);
	
			}
			// don't include breakdown items if there is no data
			if (valueFound) {
				row.setValues(values);
				dataRows.add(row);
			}
		}
		
		return dataRows;
	}
	
	@Override
	protected void postProcess() {
		((Chart)outputGraph).setUnitType(breakdownGraph.getEquation().getUnitType());
		((Chart)outputGraph).setDecimalPlaces(breakdownGraph.getEquation().getDecimalPlaces());
	}


}
