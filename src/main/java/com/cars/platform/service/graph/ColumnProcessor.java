package com.cars.platform.service.graph;

import java.util.Map;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.viewmodels.graph.BarChart;

class ColumnProcessor extends ChartProcessor<BarChart> {

	public ColumnProcessor(com.cars.platform.domain.Graph domainGraph, Company company,
			Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod, boolean showRatingPeriods)
	{
		super(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
	}

    @Override
	protected BarChart createOutputGraph() {
        return new BarChart();
	}
}
