package com.cars.platform.service.graph.breakdown;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownHistory;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;

public class BreakdownHistoryTableProcessor extends BreakdownHistoryChartProcessor<Table> {
	
    private static final Logger logger = LoggerFactory.getLogger(BreakdownHistoryTableProcessor.class);

	public BreakdownHistoryTableProcessor(
			Company company, BreakdownHistory breakdownGraph,
			Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod,
			MetricBreakdownItem summationItem, boolean showRatingPeriods)
	{
		super(company, breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
	}
	
	@Override
	protected Table createOutputGraph() {
		switch (breakdownGraph.getGraphType()) {
		case TABLE : return new Table();
		default:
			logger.error("Unexpected Graph type: " + breakdownGraph.getGraphType());
			return null;
		}
	}
	
	
	@Override
	protected void postProcess() {
		BreakdownColumn bc = breakdownGraph.getEquation();

		boolean valueFound = false;
		List<Value> totals = new ArrayList<Value>();
		for (Period period : periodsToShow) {
			Value value;
			
			Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem =
					valueByCodeByItemByPeriod.get(period);
			if (null == valueByCodeByItem.get(summationItem)) {
				value = new Value("-");
			}
			else {	
				CellValue cellValue = getCellValue(bc, summationItem, valueByCodeByItem, true);
				// TODO check for evaluation exception... what to do?
				value = new Value(cellValue.amount, bc, cellValue.value);
				valueFound = true;
			}
			totals.add(value);
		}
		
		// TODO should probably not even get here if there is no total data
		// don't include breakdown items if there is no data
		if (valueFound) {
			Row totalRow = new Row();
			totalRow.setLabel("Total");
			totalRow.setValues(totals);
			((Table)outputGraph).setTotals(totalRow);
		}

	}

}
