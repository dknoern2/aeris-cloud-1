package com.cars.platform.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Person;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.util.UserHelper;

public class ZipFileService {

   
    private DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
    
	final int MAXFILELENGTH=255;
	public static void main(String[] args) {
		ZipFileService zipFileService = new ZipFileService();
	}

	public String createZipFile(
			LinkedHashMap<DocumentType, ArrayList<Document>> documents, File dir) {
		dir.mkdirs();
		if (dir.isDirectory()) {
			File file = new File(dir.toString() + "/library.zip");
			Person person = UserHelper.getCurrentUser();
			try {
				ZipOutputStream zipFile = new ZipOutputStream(
						new FileOutputStream(file.toString()));
				if (zipFile != null) {
					for (DocumentType documentType : documents.keySet()) {						
						for (Document document : documents.get(documentType)) {
							String filename = "";
							InputStream in = null;
							try {
								in = documentDataService.getDocumentData(document.getCompany().getId(), document.getId());
							} catch(Exception e) {
								System.out.println(e);
								continue;
							}

							try {
								// file name sanitization
								String companyname = document.getCompany().getName();
								if(companyname.length() > 50){
									companyname = companyname.substring(0, 50);
								}
								companyname = companyname.replaceAll("[\\\\/:*?\"<>|]", "#") + "/";
								
								
								String folder = documentType.getName();
								if(folder.length() > 50){
									folder = folder.substring(0, 50);
								}
								folder = folder.replaceAll("[\\\\/:*?\"<>|]", "#") + "/";
								
								filename = document.getFileName();								
								if(filename.lastIndexOf("\\") > 0){
									filename = filename.substring(filename.lastIndexOf("\\")+1);
								}else if(filename.lastIndexOf("//") > 0){
									filename = filename.substring(filename.lastIndexOf("//")+1);
								}
								filename = filename.replaceAll("[\\\\/:*?\"<>|]", "#");																
								filename = companyname + folder + filename;
								
								// fix file name length
								if (filename.length() > MAXFILELENGTH + filename.length() - filename.lastIndexOf('.')){
									filename = filename.substring(0, MAXFILELENGTH) 
											 + filename.substring(filename.lastIndexOf('.'), filename.length());
								}
								//System.out.println(filename);
								ZipEntry zipEntry = new ZipEntry(filename);
								zipEntry.setTime(document.getSubmittedDate().getTime());
								zipFile.putNextEntry(zipEntry);	
								
					            byte[] buffer = new byte[1024];
					            int len;
					            while ((len = in.read(buffer)) != -1) {
					                zipFile.write(buffer, 0, len);
					            }
								zipFile.closeEntry();
							} catch (Exception e) {
								//duplicate filename								
								int version = 1;
								while(version < 10){ //give up if more than ten files with the same name									
									try{										
										if(filename.length() >= MAXFILELENGTH + filename.length() - filename.lastIndexOf('.')){
											filename = filename.substring(0, filename.lastIndexOf('.') - 1) + version + filename.substring(filename.lastIndexOf('.'), filename.length());
										}else{
											filename = filename.substring(0, filename.lastIndexOf('.')) + version + filename.substring(filename.lastIndexOf('.'), filename.length());
										}
																				
										zipFile.putNextEntry(new ZipEntry(filename));										
		                                byte[] buffer = new byte[1024];
		                                int len;
		                                while ((len = in.read(buffer)) != -1) {
		                                    zipFile.write(buffer, 0, len);
		                                }
		                                zipFile.closeEntry();
										
										break;
										
									}catch(Exception ee){
										System.out.println(ee);
										++version; 
									}
								}
							}
						}
					}
					zipFile.close();
				}
			} catch (Exception e) {
				System.out.println(e);
			}
			return file.getPath();
		}
		return "";
	}
}