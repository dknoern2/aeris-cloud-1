package com.cars.platform.service;

import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodDim;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.service.FormulaService.FormulaResult;

/**
 * Evaluates the Functions (Equations) for a company and its periods and stores the results in the ValueFact table.
 * @author kurtl
 *
 */
public class ValueFactService {
    private static final Logger logger = LoggerFactory.getLogger(ValueFactService.class);
    
    // We have a separate EntityManager for performance reasons
    // Using the EntityManager on the domain objects resulted in scanning of all
    // the objects fetched during initialization during commit... really slow
    private EntityManager entityManager = null;
    
    private FormulaService formulaService = null;

    private Company company;
	
    private HashMap<String, MetricValue> metricValueMap;

    private SortedMap<String, Period> periodMap;

	
	public ValueFactService(Company company){
		this.company = company;
		
		// It turns our that performance with the shared entity manager went away for reasons unknown
		// having a separate entityManager here turned out to fail (deadlock issues) when a referenced
		// equation was edited in the same request
		//    entityManager = SpringApplicationContext.getEntityManagerFactory().createEntityManager();
		entityManager = ValueFact.entityManager();
		init();
	}
	
	public int updateCompany() {
		long startTime = System.currentTimeMillis();
		final List<Equation> formulas = Equation.findAllEquationsByCompany(company);
		long elapsed = showElapsedTime("   found all equations", startTime);
		
		if ((formulas == null) || (formulas.size() == 0)) {
			return 0;
		}
		
		return updateCompany(formulas);
	}
	

	public int updateCompany(List<Equation> formulas) {
		System.out.println("Begin updating ValueFacts for company: " + company.getId() + ": " + company.getName());
		long startTime = System.currentTimeMillis();
		int factsCreated = 0;
        
		if ((formulas == null) || (formulas.size() == 0)) {
			return 0;
		}

		// Needed this when we had a separate entity manager
		//entityManager.getTransaction().begin();
		
		deleteForCompany(company, formulas);
		
		entityManager.clear(); // removing entities from the 'persitence context' speeds things up considerably

		boolean firstPeriod = true;
		for (Period period : periodMap.values()) {
		    String endDate = period.getEndDate();
			PeriodDim periodDim = PeriodDim.findOrCreateByPeriod(period);

			for (Equation formula : formulas) {
			    String formulaText = formula.getFormula();

			    try {
				    // handle special case where formula contains "P" for prior FYE,
					if (firstPeriod && formulaText.contains("P")) {
					    continue; // skip this period for this formula
					}
					
					FormulaResult result = formulaService.crunchFormulaWithSpread(formulaText, endDate, metricValueMap, periodMap.values(), company);
//					if (formula.getName().equals("Total Assets")) {
//						System.out.println("endDate [" + endDate + "] value [" + value + "] period [" + period.getYear() + "Q" + period.getQuarter() + "] periodDim [" + periodDim.getYear() + "Q" + periodDim.getQuarter() + "]");
//					}
				    ValueFact valueFact = getValueFact(period, periodDim, formula, result);
				    if (null != valueFact) {
				        valueFact.persist();
					    entityManager.clear(); // for performance again
					    factsCreated++;
				    }

			    }
			    catch (Exception e) {
					logger.error("Failed to evaluate formula name [" + formula.getName() + "] id [ " + formula.getId() + "] " + formulaText, e.getMessage());

			    }
			}
			firstPeriod = false;
		}
		
		long elapsed = showElapsedTime("   ValueFact creation took", startTime);
		
		// Needed this when we had a separate entity manager
		//entityManager.getTransaction().commit();
		
		elapsed = showElapsedTime("   ValueFact commit took", elapsed);

		System.out.println("End updating " + factsCreated + " ValueFacts for company: " + company.getId() + " (" + (elapsed - startTime) + " ms)");

		return factsCreated;
	}
	
	
	/**
	 * Creates facts for the provided equation. Does not remove formulas for an existing equation.
	 * Use this ONLY for new equations. 
	 * @param equation
	 */
	public int createforEquation(Equation formula) {
		long startTime = System.currentTimeMillis();
		int factsCreated = 0;
		boolean firstPeriod = true;
		for (Period period : periodMap.values()) {
		    String endDate = period.getEndDate();
			PeriodDim periodDim = PeriodDim.findOrCreateByPeriod(period);

		    String formulaText = formula.getFormula();

		    try {
			    // handle special case where formula contains "P" for prior FYE,
				if (firstPeriod && formulaText.contains("P")) {
					firstPeriod = false;
				    continue; // skip this period for this formula
				}
				
				FormulaResult result = formulaService.crunchFormulaWithSpread(formulaText, endDate, metricValueMap, periodMap.values(), company);
//					if (formula.getName().equals("Total Assets")) {
//						System.out.println("endDate [" + endDate + "] value [" + value + "] period [" + period.getYear() + "Q" + period.getQuarter() + "] periodDim [" + periodDim.getYear() + "Q" + periodDim.getQuarter() + "]");
//					}
			    ValueFact valueFact = getValueFact(period, periodDim, formula, result);
			    if (null != valueFact) {
			        valueFact.persist();
				    entityManager.clear(); // for performance again
				    factsCreated++;
			    }

		    }
		    catch (Exception e) {
				logger.error("Failed to evaluate formula name [" + formula.getName() + "] id [ " + formula.getId() + "] " + formulaText, e.getMessage());

		    }
			factsCreated++;
		}
		
		long elapsed = showElapsedTime("   updating equations took", startTime);
		
		System.out.println("End updating equation, created " + factsCreated + " ValueFacts for company: " + company.getId() + " (" + (elapsed - startTime) + " ms)");

		return factsCreated;
	}
	
	/**
	 * Creates/updates all facts for the given period. Used when the period type is updated
	 * @param equation
	 */
	public void updateEquation(Period period) {
		// TODO implement me
		throw new UnsupportedOperationException();
	}

	
	private void init() {
		int fiscalYearStart = company.getFiscalYearStart();
		
		formulaService = new FormulaService(fiscalYearStart);
		
		long elapsed = System.currentTimeMillis();
		
		periodMap = Period.findPeriodMapByCompany(company);
		
		elapsed = showElapsedTime("   found period map ", elapsed);

		List<MetricValue> metricValues = MetricValue.findMetricValues(company);

		elapsed = showElapsedTime("   found all metric values ", elapsed);

		metricValueMap = new HashMap<String, MetricValue>();
		for (MetricValue metricValue : metricValues) {
		    // create sets and maps for later lookup

		    int year = metricValue.getYear();
		    int quarter = metricValue.getQuarter();

		    String fiscalKey = Integer.toString(year) + "Q" + Integer.toString(quarter);
		    String periodEndDate = null;

		    Period period = periodMap.get(fiscalKey);

		    if (period != null) {
				periodEndDate = period.getEndDate();
	
				String key = periodEndDate + "|" + metricValue.getMetric().getFullAccountCode();
	
				metricValueMap.put(key, metricValue);
		    }
		    else {
		    	logger.warn("can't find period: " + fiscalKey);
		    }
		}
		
		showElapsedTime("  created metricValueMap ", elapsed);
	}


	private ValueFact getValueFact(Period period, PeriodDim periodDim, Equation equation, FormulaResult result) {
		if (null == result)
			return null;
		
		Double value = result.result;

		if (null == value)
			return null; // ValueFact.amount is NotNull
		
		if (value.isNaN()) {
			logger.warn("Can not persist NaN value for formula [" + equation.getId() + "]: " + equation.getFormula());
			return null;

		}
		if (value.isInfinite()) {
			logger.warn("Can not persist infinite value for formula [" + equation.getId() + "]: " + equation.getFormula());
			return null;

		}				
		ValueFact newFact = new ValueFact();
		newFact.setAmount(value);
		newFact.setCompany(company);
		newFact.setEquation(equation);
		newFact.setPeriodDim(periodDim);
		newFact.setPeriodType(period.getPeriodType());
		newFact.setSpreadFormula(result.spreadFormula);
		return newFact;
	}
	
	@Transactional
	private void deleteForCompany(Company company, List<Equation> formulas) {
		for (Equation equation : formulas) {
			Query q = entityManager.createQuery("DELETE FROM ValueFact AS o WHERE o.company = :company AND o.equation = :equation");
			q.setParameter("company", company);
			q.setParameter("equation", equation);
			q.executeUpdate();
		}
	}
    
    
    private static long showElapsedTime(String activity, long millis) {
		long elapsed = System.currentTimeMillis() - millis;
		logger.info(activity + " took " + elapsed + " ms");
		return System.currentTimeMillis();
    }
	
}


