package com.cars.platform.service;

import java.util.ArrayList;
import java.util.List;

import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricType;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;


/**
 * Creates MetricValues with appropriate relationships to MetricBreakdownItems provided a properly formatted xls file
 * @author kurtl
 *
 */
public class BreakdownTemplateService {
	    
    private WorkBook wb = new WorkBook();
    
    private List<MetricBreakdown> allBreakdowns;    
    
    public BreakdownTemplateService() {
    	this(MetricBreakdown.findAllMetricBreakdowns());
    }
    
    public BreakdownTemplateService(List<MetricBreakdown> allBreakdowns) {
    	this.allBreakdowns = allBreakdowns;
    }

    
    
    public WorkBook createTemplate() throws Exception
    {
    	wb = new WorkBook(); // starts with 1 sheet

    	int numSheets = 0;
    	for (MetricBreakdown mb : allBreakdowns) {
    		List<Metric> metrics = Metric.findMetricsForBreakdown(mb);
    		if (null == metrics || metrics.size() == 0 ) {
    			continue; // ignore breakdowns with no metrics defined.
    		}
    		List<MetricBreakdownItem> mbis = mb.fetchActiveItems();
    		if (mbis.size() == 0) {
    			continue; // ignore breakdowns with no metric breakdown items defined
    		}
    		
    		if (numSheets > 0) {
    			wb.insertSheets(0, 1);
    		}
    		createSheet(0, mb, mbis, metrics);
    		numSheets++;
    	}

    	return wb;
    }



	private void createSheet(int sheet, MetricBreakdown mb, List<MetricBreakdownItem> mbis, List<Metric> metrics) throws Exception {
		wb.setSheet(sheet);
		wb.setSheetName(sheet, mb.getName());
		//wb.setText(0,  0, mb.getName());
		
		int numCols = metrics.size();
		int numRows = mbis.size();

		
		// column headers are the Metric names
		for (int j=0; j<numCols; j++) {
			Metric m = metrics.get(j);
			wb.setText(0, j+1, m.getName());
			setColumnFormat(j+1, numRows, m);
		}
		
		// for each row (breakdown item)
		for (int i=0; i<numRows; i++) {
			MetricBreakdownItem mbi = mbis.get(i);
			
			// TODO handle free form and years 
			// column 0 is the breakdown item name
			wb.setText(i+1, 0, mbi.getName());
		}
		
		// autosize the columns
		for (int j=0; j<numCols+1; j++) {
			wb.setColWidthAutoSize(j, true);
		}
		
		// bold and underline first row
		RangeStyle range = wb.getRangeStyle(0,0,0, numCols);
		range.setFontBold(true);
		range.setBottomBorder(RangeStyle.BorderThin);
		wb.setRangeStyle(range, 0,0,0, numCols);
		
		// bold column 1
		range = wb.getRangeStyle(1,0,numRows,0);
		range.setFontBold(true);
		//range.setRightBorder(RangeStyle.BorderMedium);
		wb.setRangeStyle(range, 1,0,numRows,0);
		
		// unlock the data cells
		range = wb.getRangeStyle(1,1,numRows,numCols);
		range.setLocked(false);
		wb.setRangeStyle(range, 1,1,numRows,numCols);
		
		wb.setSheetProtected(sheet, WorkBook.sheetProtectionAllowDefault, null);
	}

	private void setColumnFormat(int column, int numRows, Metric m) throws Exception {
		RangeStyle range = wb.getRangeStyle(1, column, numRows, column);
		String cellFormat = null;
		if (m.getType() == MetricType.TEXT) {
			cellFormat = "@";
		}
		else {
			// Unfortunately Metric does not have UnitType so this is the best we can do
			// to support numeric, currency, and percentage
			cellFormat = "0.00_);(0.00)";
		}
		range.setCustomFormat(cellFormat);
		wb.setRangeStyle(range, 1, column, numRows, column);
	}

}
