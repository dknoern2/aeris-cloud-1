package com.cars.platform.service;

import java.util.ArrayList;
import java.util.List;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.ReportCategory;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.viewmodels.report.Category;
import com.cars.platform.viewmodels.report.ReportCategories;


public class ReportCategoryService extends AbstractReportService {
	
	private Company company;
	
	
	public ReportCategoryService(Company company) {
		this.company = company;
	}
	
	
	public ReportCategories getReportCategories(ReportPage reportPage) {
		ReportCategories reportCategories = new ReportCategories(reportPage);
		reportCategories.setCategories(getCategories(reportPage, null));
		
		return reportCategories;
	}

 

	
	protected List<Category> getCategories(ReportPage reportPage, ReportCategory parent) {
     	List<ReportCategory> reportCategories = ReportCategory.findByReportPageAndParent(reportPage, parent);
     	if (null == reportCategories || reportCategories.size() == 0) {
     		return null;
     	}
     	
	    List<Category> categories = new ArrayList<Category>();
	    
    	for (ReportCategory reportCategory : reportCategories) {
    		Category category = new Category(reportCategory);
    		category.setDatas(getDatas(reportCategory));
    		List<Category> subCategories = getCategories(reportPage, reportCategory);
    		category.setSubCategories(subCategories);	
    		categories.add(category);	
    	}
    	
    	return categories;
	}


	protected List<com.cars.platform.viewmodels.report.Equation> getDatas(ReportCategory reportCategory) {
		List<Equation> equations = reportCategory.getEquations(company, this.company == null);
		List<com.cars.platform.viewmodels.report.Equation> datas = new ArrayList<com.cars.platform.viewmodels.report.Equation>();
		
		for (Equation equation : equations) {
			com.cars.platform.viewmodels.report.Equation data = new com.cars.platform.viewmodels.report.Equation(equation);
			datas.add(data);
		}
		
		return datas;
	}

}
