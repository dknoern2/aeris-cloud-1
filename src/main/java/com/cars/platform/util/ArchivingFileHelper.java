package com.cars.platform.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;

public class ArchivingFileHelper {
	public static final int ALL_PERIODS = -1;
	
	public static int getPeriodsCountByCode(int code) {
		int result;
		switch (code) {
			case 0:
				result = 1;
				break;
			case 1:
				result = 2;
				break;
			case 2:
				result = 5;
				break;
			case 3:
				result = 1000000;
				break;
			default:
				result = 1000000;
				break;
		}
		return result;
	}
	
	public static void filterDocumentsMap(LinkedHashMap<DocumentType, ArrayList<Document>> prunedDocumentMap, int[] periodsCountsCodes) {
		if (prunedDocumentMap.keySet().size() != periodsCountsCodes.length) {
			System.out.println("Documents map filtering: sizes of map and counts array are not equal");
			return;
		}
		
		int k = 0;
		for (DocumentType key : prunedDocumentMap.keySet()) {
			ArrayList<Document> value = prunedDocumentMap.get(key);
			if (value.size() == 0) {
				k++;
				continue;
			}
			int prevFiscalYear = value.get(0).getFiscalYear();
			int prevFiscalQuarter = value.get(0).getFiscalQuarter();
			int count = 1;
			
			ArrayList<Document> documentList = new ArrayList<Document>();
			documentList.add(value.get(0));
			for (int j = 1; j < value.size(); j++) {
				Document doc = value.get(j);
				int fiscalYear = doc.getFiscalYear();
				int fiscalQuarter = doc.getFiscalQuarter();
				if (fiscalYear != prevFiscalYear || fiscalQuarter != prevFiscalQuarter) {
					prevFiscalYear = fiscalYear;
					prevFiscalQuarter = fiscalQuarter;
					count++;
				}
				int periodsCount = ArchivingFileHelper.getPeriodsCountByCode(periodsCountsCodes[k]);
				if (count > periodsCount) {
					break;
				}
				documentList.add(doc);
			}
			prunedDocumentMap.put(key, documentList);
			k++;
		}
	}
}
