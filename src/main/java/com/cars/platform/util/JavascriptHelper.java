package com.cars.platform.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class JavascriptHelper {
	
	
	public static String serialize(Object o) throws IllegalArgumentException{
		
		String s = null;
		
		ObjectMapper mapper = new ObjectMapper();
		 
		 
		StringWriter sw = new StringWriter();
		 
		  try {
			mapper.writeValue(sw, o);
			
			s = sw.toString();
	
		} catch (JsonGenerationException e) {
			throw new IllegalArgumentException();
		} catch (JsonMappingException e) {
			throw new IllegalArgumentException();
		} catch (IOException e) {
			throw new IllegalArgumentException();
		} 
		  
		s = s.replaceAll("\"Infinity\"", "Infinity");
		s = s.replaceAll("\"NaN\"", "NaN");
		
		return s;
	}
	
	
	public static List<List<String>>  deserializeListOfLists(String src) throws IllegalArgumentException{
		
		src = src.replaceAll("'","\"");
		
		
		ObjectMapper mapper = new ObjectMapper();
		List<List<String>> list =null;
		
		try {
			list = mapper.readValue(src, new TypeReference<List<List<String>>>(){});
			//list = mapper.readValue(src, String[].class);
	
		} catch (JsonGenerationException e) {
			throw new IllegalArgumentException();
		} catch (JsonMappingException e) {
			throw new IllegalArgumentException();
		} catch (IOException e) {
			throw new IllegalArgumentException();
		} 
		  	
		return list;
	}

}


