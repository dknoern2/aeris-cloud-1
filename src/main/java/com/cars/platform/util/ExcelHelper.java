package com.cars.platform.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cars.platform.domain.UnitType;
import com.smartxls.WorkBook;

public class ExcelHelper {

	
	
	public static String getEquivColumn(int number){
        String converted = "";
        // Repeatedly divide the number by 26 and convert the
        // remainder into the appropriate letter.
        while (number >= 0){
            int remainder = number % 26;
            converted = (char)(remainder + 'A') + converted;
            number = (number / 26) - 1;
        }

        return converted;
    }
	
	public static String getRange(int row1,int col1,int row2,int col2){
		return "$"+getEquivColumn(col1)+"$"+Integer.toString(row1+1)+":"+"$"+getEquivColumn(col2)+"$"+Integer.toString(row2+1);
	}

	
	
	public static String getCellFormat(UnitType unitType, int decimalPlaces) {

		String decimalFormat = "";
		if (decimalPlaces > 0) {
			StringBuilder sb = new StringBuilder(".");
			for (int i = 0; i < decimalPlaces; i++) {
				sb.append("0");
			}
			decimalFormat = sb.toString();
		}

		String cellFormat = "@"; // Text
		if (unitType == UnitType.DOLLAR
				|| unitType == UnitType.NUMBER) {
			// #,##0.00_);(#,##0.00) --> 2 decimal places
			// #,##0_);(#,##0) --> 0 decimal places
			// rs.setCustomFormat("_($#,##0_);_($(#,##0);_(* \"-\"_);_(@_)");
			cellFormat = "_(#,##0" + decimalFormat + "_);_((#,##0" + decimalFormat + ");_(* \"-\"_);_(@_)";
		}
		// else if (rowData.getUnitType() == UnitType.NUMBER) {
		// //0.00 --> 2 decimal places
		// //0 --> 0 decimal places
		// cellFormat = "0" + decimalFormat;
		// }
		else if (unitType == UnitType.PERCENTAGE) {
			// 0.00% --> 2 decimal places
			// 0% --> 0 decimal places
			// rs.setCustomFormat("0.0%;-0.0%;* \"-\"_);_(@_)");
			cellFormat = "0" + decimalFormat + "%;-0" + decimalFormat + "%;* \"-\"_);_(@_)";
		} else {
			// unreachable code
		}
		
		return cellFormat;
	}
	
	public static boolean readWorkBook(WorkBook workBook, InputStream inputStream) throws Exception {
		BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
		if (POIXMLDocument.hasOOXMLHeader(bufferedInputStream)) {
			workBook.readXLSX(bufferedInputStream);
			return true;
		} else if (POIFSFileSystem.hasPOIFSHeader(bufferedInputStream)) {
			workBook.read(bufferedInputStream);
			return true;
		} else {
			return false;
		}
	}
	
}
