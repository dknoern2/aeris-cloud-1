package com.cars.platform.util;

import java.util.Random;

public class RandomHelper {

	static Random rnd = new Random();
	static final String AB = "0123456789";

	public static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

}