package com.cars.platform.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.cars.platform.domain.MetricValue;

/**
 * Helper class to organize and lookup MetricValues for a company by year, quarter (optional), and accountCode
 * 
 * @author dknoern
 *
 */
public class MetricValueMap {

	
	HashSet<Integer> years = new HashSet<Integer>();
	HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
	
	public MetricValueMap(List<MetricValue> metricValues){
		// build lookup by metric account code
		// also build list of years

		for (MetricValue metricValue : metricValues) {
			metricValueMap.put(metricValue.getYear() + "|"
					+ metricValue.getMetric().getFullAccountCode(), metricValue);
			
			//System.out.println("adding year " + metricValue.getYear()  );
			years.add(metricValue.getYear());
		}
	}
	
	
	public double getMetricValueAmount(String accountCode,int year){
		String key = Integer.toString(year) + "|" + accountCode;
		MetricValue metricValue = metricValueMap.get(key);
		
		double amount = 0;
		if(metricValue!=null){
			amount = metricValue.getAmountValue();
		}
		
		return amount;	
	}


	public HashSet<Integer> getYears() {
		return years;
	}
	
}
