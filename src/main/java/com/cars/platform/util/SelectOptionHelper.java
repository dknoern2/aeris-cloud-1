package com.cars.platform.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.cars.platform.domain.GraphShowYears;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.State;
import com.cars.platform.domain.UnitType;

public class SelectOptionHelper {
	
	public static String SELECT = "--Select--";

	public static Map<String,String> getStates(List<State> states){
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
		
		
		for(State state:states){
			map.put(state.getStateCode(),state.getName());
		}
		
		return map;
	}

	public static Map<String,String> getFiscalYearEnds(){
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
	
	    map.put("2","January 31");
	    map.put("3","February 28");
	    map.put("4","March 31");
	    map.put("5","April 30");
	    map.put("6","May 31");
	    map.put("7","June 30");
	    map.put("8","July 31");
	    map.put("9","August 30");
	    map.put("10","September 30");
	    map.put("11","October 31");
	    map.put("12","November 30");
	    map.put("1","December 31");

		return map;
	}
	

	
	public static Map<String,String> getOrgTypes(){
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
	
	    map.put("1","Loan Fund");
	    map.put("2","Venture Capital Fund");

		
		return map;
	}
		
	
	

	public static Map<String,String> getFinancialStrengths(){
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();

		for(String financialStrength : Review.GetAllowedFullFinancialStrengthValues()){
			map.put(financialStrength, financialStrength);
		}
	    /*map.put("1","1");		
	    map.put("2","2");
	    map.put("3","3");
	    map.put("4","4");
	    map.put("5","5");*/
		
		return map;
	}
	

	public static Map<String,String> getImpactStrengths(){
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
		
		for(String impactStrength : Review.GetAllowedFullImpactValues()){
			map.put(impactStrength, impactStrength);
		}
	    /*map.put("AAA","AAA");
	    map.put("AA","AA");
	    map.put("A","A");
	    map.put("B","B");*/
		return map;
	}

	public static List<Integer> getDecimalPlaces(){
		ArrayList<Integer> set = new ArrayList<Integer>();
		set.add(0);
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		return set;
	}
	
	public static List<GraphShowYears> getShowYears(){
		ArrayList<GraphShowYears> list = new ArrayList<GraphShowYears>();	
		list.add(new GraphShowYears(10,"All Years"));
		list.add(new GraphShowYears(5,"Last 5 Years"));
		list.add(new GraphShowYears(3,"Last 3 Years"));
		return list;
	}

	public static Map<String,String> getYears() {
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
		
		int year = DateHelper.getCurrentYear();
		map.put("0",SELECT);
		for(int i=-1; i<5; i++)
		{
			String yearS = String.valueOf(year-i);
			map.put(yearS,yearS);
		}
		
		return map;
	}
	
	public static Map<String,String> getQuarters() {
		LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
		map.put("0",SELECT);
		map.put("1","First");
	    map.put("2","Second");
	    map.put("3","Third");
	    map.put("4","Fourth");
		return map;
	}
	

	
	
	// Options used for field:select tags follow
	public static List<IntegerSelect> getSelectQuarters() {
		return Arrays.asList(
				new IntegerSelect(SELECT, 0),
				new IntegerSelect("First", 1),
				new IntegerSelect("Second", 2),
				new IntegerSelect("Third", 3),
				new IntegerSelect("Fourth", 4)
				);
	}
	
	public static List<IntegerSelect> getSelectYears(int past, int future) {
		int year = DateHelper.getCurrentYear();
		List<IntegerSelect> years = new ArrayList<IntegerSelect>();
		years.add(new IntegerSelect(SELECT, 0));
		for(int i=-future; i<=past; i++)
		{
			String yearString = String.valueOf(year-i);
			years.add(new IntegerSelect(yearString, year-i));
		}
		return years;
	}
	
	public static List<IntegerSelect> getSelectShowYears() {
		return Arrays.asList(
				new IntegerSelect("All Years", 10),
				new IntegerSelect("Last 5 Years", 5),
				new IntegerSelect("Last 3 Years", 3)
				);
	}

	
	public static class IntegerSelect {
		private String name;
		private int value;
		
		public String getName() {
			return name;
		}

		public int getValue() {
			return value;
		}

		public IntegerSelect(String name, int value) {
			this.name = name;
			this.value = value;
		}
	}

}
