package com.cars.platform.viewmodels.peergroup;

/*
 * Simple data class controlling what to export. These correspond to display controls on the UI
 */
public class ExportRequest {
	private boolean allYears;
	private boolean showInterim;
	private boolean showIncomplete = false;
	private Long[] equationIds;

	public boolean isAllYears() {
		return allYears;
	}
	public boolean isShowInterim() {
		return showInterim;
	}
	public Long[] getEquationIds() {
		return equationIds;
	}
	public void setAllYears(boolean allYears) {
		this.allYears = allYears;
	}
	public void setShowInterim(boolean showInterim) {
		this.showInterim = showInterim;
	}
	public void setEquationIds(Long[] equationIds) {
		this.equationIds = equationIds;
	}
	public boolean isShowIncomplete() {
		return showIncomplete;
	}
	public void setShowIncomplete(boolean showIncomplete) {
		this.showIncomplete = showIncomplete;
	}
}
