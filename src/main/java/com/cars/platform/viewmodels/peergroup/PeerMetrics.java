package com.cars.platform.viewmodels.peergroup;

import com.cars.platform.domain.Equation;
import com.cars.platform.viewmodels.graph.LineChart;
import com.cars.platform.viewmodels.graph.Table;

public class PeerMetrics extends PeerMetricBase {
	private Table table;
	private LineChart lineChart;
	
	
	public PeerMetrics(Equation equation) {
		super(equation);
	}
	
	public Table getTable() {
		return table;
	}

	public LineChart getLineChart() {
		return lineChart;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public void setLineChart(LineChart lineChart) {
		this.lineChart = lineChart;
	}

}
