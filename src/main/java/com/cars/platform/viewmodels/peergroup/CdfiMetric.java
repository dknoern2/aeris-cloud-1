package com.cars.platform.viewmodels.peergroup;

import java.util.ArrayList;
import java.util.List;

public class CdfiMetric {
	private String metric;
	private List<String> values;
	
	public CdfiMetric(String metric) {
		super();
		this.metric = metric;
		this.values = new ArrayList<String>();
	}

	public String getMetric() {
		return metric;
	}

	public List<String> getValues() {
		return values;
	}
	

}
