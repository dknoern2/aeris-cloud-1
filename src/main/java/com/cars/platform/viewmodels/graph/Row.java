package com.cars.platform.viewmodels.graph;

import java.util.List;

public class Row {
	
	private String label;
	
	private List<Value> values;

	public String getLabel() {
		return label;
	}

	public List<Value> getValues() {
		return values;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setValues(List<Value> values) {
		this.values = values;
	}

}
