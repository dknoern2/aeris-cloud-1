package com.cars.platform.viewmodels.graph;

import com.cars.platform.domain.IEquationFormat;
import com.cars.platform.domain.UnitType;

public class Value implements IEquationFormat {
	
	private Double amount;
	
	private IEquationFormat format;
	
	private String value;
	
	public Value(String value) {
		this.value = value;
		this.amount = null;
	}
	
	public Value(Double amount, IEquationFormat format, String value) {
		if (format == null) {
			throw new IllegalArgumentException("format argument is required");
		}
		this.amount = amount;
		this.value = value;
		this.format = format;
	}

	public Double getAmount() {
		return amount;
	}

	public String getValue() {
		return value;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public UnitType getUnitType() {
		if (null == format) {
			return null;
		}
		return format.getUnitType();
	}
	@Override
	public Integer getDecimalPlaces() {
		if (null == format) {
			return null;
		}
		return format.getDecimalPlaces();
	}

}
