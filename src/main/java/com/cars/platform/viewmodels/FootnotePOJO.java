package com.cars.platform.viewmodels;

public class FootnotePOJO {

	String label;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	String comment;
	String reportPage;

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getReportPage() {
		return reportPage;
	}
	public void setReportPage(String reportPage) {
		this.reportPage = reportPage;
	}
}
