package com.cars.platform.viewmodels;

import com.cars.platform.domain.Company;

public class PeerPOJO {
	
	private long id;
	private String name;
	
	public PeerPOJO() {
	}
	
	public PeerPOJO(Company peer) {
		this.id = peer.getId();
		this.name = peer.getName();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
}

