package com.cars.platform.viewmodels.report;

import java.util.List;

/**
 * Subclass of Equation which provides the data values, as strings, for online rendering of the report
 * or the spreadsheet formulas, in terms of account codes for off-line reporting
 * @author kurtl
 *
 */
public class Data extends Equation {
	

	private List<String> values;
	
	
	public Data(com.cars.platform.domain.Equation equation) {
		super(equation);
	}
	
	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	
}
