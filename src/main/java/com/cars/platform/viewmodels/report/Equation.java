package com.cars.platform.viewmodels.report;

import com.cars.platform.domain.UnitType;


/**
 * The equation representing a row in a report.
 * This class is returned when editing a report (ReportCategoryService)
 * The Data sublcass is returned for online/offline generation of the report (ReportService2)
 * @author kurtl
 *
 */
public class Equation {
	
	private Long id;
	private String name;
	private boolean isGlobal;
	private boolean isOverride;
	private String formula;
	private int decimalPlaces;
	private UnitType unitType;
	private boolean isSmallBetter;
	private String definition;
	
	public Equation(com.cars.platform.domain.Equation equation) {
		this.id = equation.getId();
		this.name = equation.getName();
		this.isGlobal = (null == equation.getCompany());
		this.isOverride = equation.getOverride() != null;
		this.formula = equation.getFormula();
		this.decimalPlaces = equation.getDecimalPlaces();
		this.unitType = equation.getUnitType();
		this.isSmallBetter = equation.isSmallerBetter();
		this.definition = equation.getDefinition();
	}
	
	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public boolean isGlobal() {
		return isGlobal;
	}
	public boolean isOverride() {
		return isOverride;
	}
	public String getFormula() {
		return formula;
	}

	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	public UnitType getUnitType() {
		return unitType;
	}
	
	public boolean getIsSmallBetter() {
		return isSmallBetter;
	}
	
	public String getDefinition() {
		return definition;
	}
}
