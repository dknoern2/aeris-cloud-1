package com.cars.platform.viewmodels;

import java.util.ArrayList;
import java.util.List;

public class ParentCategory {
	
	private String name;
	private List<ChildCategory> children;
	
	public ParentCategory(){

		children = new ArrayList<ChildCategory>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ChildCategory> getChildren() {
		return children;
	}

	public void setChildren(List<ChildCategory> children) {
		this.children = children;
	}

	
	

}
