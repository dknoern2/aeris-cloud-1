package com.cars.platform.viewmodels;

public class MappedCell {
	private String labelText;
	private String valueText;
	private String metricName;
	private String foundAfter;
	private String foundBefore;
	private int labelX;
	private int labelY;
	private int labelZ;
	private int valueX;
	private int valueY;
	private int valueZ;
	private long companyId;
	private int year;
	private int quarter;
	private long metricId;
	private long categoryId;
	private long parentCategoryId;
	private boolean required;
	private boolean negate;
	private boolean valuesRight;
	
	public MappedCell() {

	}

	public String getLabelText() {
		return labelText;
	}
	
	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	public String getValueText() {
		return valueText;
	}

	public void setValueText(String valueText) {
		this.valueText = valueText;
	}
	
	public String getMetricName() {
		return metricName;
	}

	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}
	
	public String getFoundAfter() {
		return foundAfter;
	}
	
	public void setFoundAfter(String foundAfter) {
		this.foundAfter = foundAfter;
	}
	
	public String getFoundBefore() {
		return foundBefore;
	}
	
	public void setFoundBefore(String foundBefore) {
		this.foundBefore = foundBefore;
	}
	
	public boolean isValuesRight() {
		return this.valuesRight;
	}
	
	public void setValuesRight(boolean valuesRight) {
		this.valuesRight = valuesRight;
	}

	public int getLabelX() {
		return labelX;
	}

	public void setLabelX(int labelX) {
		this.labelX = labelX;
	}

	public int getLabelY() {
		return labelY;
	}

	public void setLabelY(int labelY) {
		this.labelY = labelY;
	}
	
	public int getLabelZ() {
		return labelZ;
	}

	public void setLabelZ(int labelZ) {
		this.labelZ = labelZ;
	}

	public int getValueX() {
		return valueX;
	}

	public void setValueX(int valueX) {
		this.valueX = valueX;
	}

	public int getValueY() {
		return valueY;
	}

	public void setValueY(int valueY) {
		this.valueY = valueY;
	}
	

	public int getValueZ() {
		return valueZ;
	}

	public void setValueZ(int valueZ) {
		this.valueZ = valueZ;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public long getMetricId() {
		return metricId;
	}

	public void setMetricId(long metricId) {
		this.metricId = metricId;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
	public boolean isNegate() {
		return negate;
	}

	public void setNegate(boolean negate) {
		this.negate = negate;
	}
}
