package com.cars.platform.viewmodels;

import com.cars.platform.domain.MetricBreakdown;

/**
 * viewmodel object necessary as domain version includes a circular reference of attributes
 * @author kurtl
 *
 */
public class MetricBreakdownPOJO {
	
	private String name;
	private long id;
	
	public MetricBreakdownPOJO() {
	}
	
	public MetricBreakdownPOJO(MetricBreakdown domainObject) {
		this.name = domainObject.getName();
		this.id = domainObject.getId();
	}
	
	
	public String getName() {
		return name;
	}
	public long getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setId(long id) {
		this.id = id;
	}

}
