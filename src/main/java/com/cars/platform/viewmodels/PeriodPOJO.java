package com.cars.platform.viewmodels;

import com.cars.platform.domain.PeriodType;

public class PeriodPOJO {

	public PeriodPOJO() {

	}

	private long companyId;
	private String companyName;
	private int year;
	private int quarter;
	private String endDate;
	private PeriodType periodType;
	private long id;
	
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getQuarter() {
		return quarter;
	}
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public PeriodType getPeriodType() {
		return periodType;
	}
	public void setPeriodType(PeriodType periodType) {
		this.periodType = periodType;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}



}
