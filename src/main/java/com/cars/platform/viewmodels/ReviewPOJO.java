package com.cars.platform.viewmodels;

import java.util.Date;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.cars.platform.domain.Review;
import com.cars.platform.domain.ReviewType;

public class ReviewPOJO {
	
	private Long id;
	@Enumerated
    private ReviewType reviewType;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date dateOfAnalysis;

    private String impactPerformance;
    
    private boolean policyPlus;

    private String financialStrength;
    
    private boolean skipNotification;
    
    public ReviewPOJO(Review review){
    	setId(review.getId());
    	setReviewType(review.getReviewType());
    	setDateOfAnalysis(review.getDateOfAnalysis());
    	setImpactPerformance(review.getImpactPerformance());
    	setPolicyPlus(review.isPolicyPlus());
    	setFinancialStrength(review.getFinancialStrength());
    	setSkipNotification(review.isSkipNotification());
    }
    
    public ReviewType getReviewType() {
        return this.reviewType;
    }
    
    public void setReviewType(ReviewType reviewType) {
        this.reviewType = reviewType;
    }
    
    public Date getDateOfAnalysis() {
        return this.dateOfAnalysis;
    }
    
    public void setDateOfAnalysis(Date dateOfAnalysis) {
        this.dateOfAnalysis = dateOfAnalysis;
    }

	public String getImpactPerformance() {
		return impactPerformance;
	}

	public void setImpactPerformance(String impactPerformance) {
		this.impactPerformance = impactPerformance;
	}

	public boolean getPolicyPlus() {
		return policyPlus;
	}

	public void setPolicyPlus(boolean policyPlus) {
		this.policyPlus = policyPlus;
	}

	public String getFinancialStrength() {
		return financialStrength;
	}

	public void setFinancialStrength(String financialStrength) {
		this.financialStrength = financialStrength;
	}

	public boolean isSkipNotification() {
		return skipNotification;
	}

	public void setSkipNotification(boolean skipNotification) {
		this.skipNotification = skipNotification;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}