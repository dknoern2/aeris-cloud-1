package com.cars.platform.viewmodels;

import java.util.List;

public class MappedRow {

	private List<SpreadsheetCell> row;

	public List<SpreadsheetCell> getRow() {
		return row;
	}

	public void setRow(List<SpreadsheetCell> row) {
		this.row = row;
	}

}
