package com.cars.platform.viewmodels;

import com.cars.platform.domain.Period;

public class TentativeChange {

	Long metricValueId;
	String metricFullAccountCode;

	String metricName;
	public String getMetricFullAccountCode() {
		return metricFullAccountCode;
	}
	public void setMetricFullAccountCode(String metricFullAccountCode) {
		this.metricFullAccountCode = metricFullAccountCode;
	}
	String period;
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	String newValue;
	public Long getMetricValueId() {
		return metricValueId;
	}
	public void setMetricValueId(Long metricValueId) {
		this.metricValueId = metricValueId;
	}
	public String getMetricName() {
		return metricName;
	}
	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	String comment;
	
	
	public String getDescription(){
		return this.metricName + " for " + this.period + " " + this.getComment();
	}
	
	
	public static String getTentativeChangeKey(TentativeChange tentativeChange) {
		Long metricValueId = tentativeChange.getMetricValueId();
		String period = tentativeChange.getPeriod();
		String metricFullAccountCode = tentativeChange.getMetricFullAccountCode();
		return getTentativeChangeKey(metricValueId, period, metricFullAccountCode);
	}
	
	public static String getTentativeChangeKey(Long metricValueId, String period, String metricFullAccountCode) {
		String key;
		if (metricValueId == null)
		{
			key = period + '_' +
					metricValueId + '_' + metricFullAccountCode;
		} else{
			key = metricValueId.toString();
		}
		return key;
	}
	
}
