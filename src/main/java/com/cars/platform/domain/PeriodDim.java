package com.cars.platform.domain;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class PeriodDim implements Comparable<PeriodDim> {
	
	@NotNull
    private int year;

	@NotNull
    private int quarter;
	
	public static PeriodDim findByPeriod(Period period) {
        if (period == null) throw new IllegalArgumentException("The period argument is required");
		TypedQuery<PeriodDim> q = entityManager().createQuery("SELECT o FROM PeriodDim o where o.year = :year and o.quarter =:quarter", PeriodDim.class);	
		q.setParameter("year", period.getYear());		
		q.setParameter("quarter", period.getQuarter());		
		q.setMaxResults(1);
		
		List<PeriodDim> list = q.getResultList();
        if(list!=null && list.size()>0) return list.get(0);
        return null;
	}
	
	public static PeriodDim findOrCreateByPeriod(Period period) {
        if (period == null) throw new IllegalArgumentException("The period argument is required");
        
        PeriodDim periodDim = findByPeriod(period);
        if (null == periodDim) {
        	periodDim = new PeriodDim();
        	periodDim.setYear(period.getYear());
        	periodDim.setQuarter(period.getQuarter());
        	periodDim.persist();
        }
        
        return periodDim;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + quarter;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodDim other = (PeriodDim) obj;
		if (quarter != other.quarter)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public int compareTo(PeriodDim o) {
		if (year > o.year) {
		    return 1;
		}
		if (year < o.year) {
			return -1;
		}
		if (quarter > o.quarter) {
			return 1;
		}
		if (quarter < o.quarter) {
			return -1;
		}
		return 0;
	}



}
