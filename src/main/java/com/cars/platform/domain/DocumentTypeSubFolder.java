package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class DocumentTypeSubFolder {

    @NotNull
    @Size(min = 2)
    public String name;
    
    @ManyToOne
    private DocumentTypeFolder documentTypeFolder;
    
    public static List<DocumentTypeSubFolder> findSubFoldersByFolder(DocumentTypeFolder documentTypeFolder) {
        if (documentTypeFolder == null) throw new IllegalArgumentException("The documentTypeFolder argument is required");
        EntityManager em = entityManager();
        TypedQuery<DocumentTypeSubFolder> q = em.createQuery("SELECT o FROM DocumentTypeSubFolder AS o WHERE o.documentTypeFolder = :documentTypeFolder order by o.id", DocumentTypeSubFolder.class);
        q.setParameter("documentTypeFolder", documentTypeFolder);
        return q.getResultList();
    }
}
