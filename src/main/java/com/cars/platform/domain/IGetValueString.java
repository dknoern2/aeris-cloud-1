package com.cars.platform.domain;



public interface IGetValueString {
	
	String getValueString(double value, IEquationFormat equation);

}
