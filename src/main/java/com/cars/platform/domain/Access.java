package com.cars.platform.domain;

import java.util.Date;

import javax.persistence.ManyToOne;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

@RooJavaBean
// @RooToString(excludeFields={"company"})
@RooJpaActiveRecord	
public class Access {

    @ManyToOne
    private Company company;
	private Date effectiveDate;
	private Date expirationDate;
	
	public boolean isActive() {
		// dates should only be set for Select subscriptions
		if (null == expirationDate || null == effectiveDate) {
			return true;
		}
		Date today = new Date();
		if (today.before(expirationDate) && !today.before(effectiveDate)) {
			return true;
		}
		return false;
	}
}
