// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;

privileged aspect MetricBreakdownItem_Roo_JavaBean {
    
    public String MetricBreakdownItem.getName() {
        return this.name;
    }
    
    public void MetricBreakdownItem.setName(String name) {
        this.name = name;
    }
    
    public int MetricBreakdownItem.getRank() {
        return this.rank;
    }
    
    public void MetricBreakdownItem.setRank(int rank) {
        this.rank = rank;
    }
    
    public boolean MetricBreakdownItem.isActive() {
        return this.active;
    }
    
    public void MetricBreakdownItem.setActive(boolean active) {
        this.active = active;
    }
    
    public MetricBreakdown MetricBreakdownItem.getMetricBreakdown() {
        return this.metricBreakdown;
    }
    
    public void MetricBreakdownItem.setMetricBreakdown(MetricBreakdown metricBreakdown) {
        this.metricBreakdown = metricBreakdown;
    }
    
}
