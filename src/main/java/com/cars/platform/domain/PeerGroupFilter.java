package com.cars.platform.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@Entity
//@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class PeerGroupFilter {
	
	private static final Logger logger = LoggerFactory.getLogger(PeerGroupFilter.class);

	
    @Enumerated
    private CdfiFilterType filter;
	

    @Size(min=2, max = 1024) // this was not affecting the db column width for some reason so added @Column
    @Column(length = 1024)
    private String value;
    

    @ManyToOne
    private PeerGroup peerGroup;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

	@Version
    @Column(name = "version")
    private Integer version;

	@JsonIgnore
	public Long getId() {
        return this.id;
    }

	public void setId(Long id) {
        this.id = id;
    }

	@JsonIgnore
	public Integer getVersion() {
        return this.version;
    }

	public void setVersion(Integer version) {
        this.version = version;
    }
	
	public CdfiFilterType getFilter() {
        return this.filter;
    }

	public void setFilter(CdfiFilterType filter) {
        this.filter = filter;
    }
	
	@JsonIgnore // see getValues
	public String getValue() {
        return this.value;
    }

	public void setValue(String value) {
        this.value = value;
    }
	
	@JsonIgnore
	public PeerGroup getPeerGroup() {
		return peerGroup;
	}

	public void setPeerGroup(PeerGroup peerGroup) {
		this.peerGroup = peerGroup;
	}

	// JSON API returns the filter values as an array of strings
	public String[] getValues() {
		if (null == getValue()) {
			return null;
		}
		
		String[] values = null;
		try {
			JSONDeserializer<String[]> d = new JSONDeserializer<String[]>();
			values = d.deserialize(getValue(), String[].class);
		}
		catch (Exception e) {
			logger.error("Unable to deserialize filter value: " + getValue(), e);
			// return null if stored value is mal-formed
		}
		return values;
	}
	
	public void setValues(String[] values) {
		if (null == values) {
			setValue(null);
		}
		
		JSONSerializer s = new JSONSerializer();
		String newValue = s.serialize(values);
		setValue(newValue);
	}

	public PeerGroupFilter cloneToPeerGroup(PeerGroup pgClone) {
		PeerGroupFilter clone = new PeerGroupFilter();
		clone.filter = this.filter;
		clone.value = this.value;
		clone.peerGroup = pgClone;
		
		return clone;
	}
	
	
	public String getDisplayName() {
		return filter.getDisplayName();
	}
	
	// set method needed to make JSON deserialization from the UI behave, it echoes back displayName
	public void setDisplayName(String displayName) {
	}
}
