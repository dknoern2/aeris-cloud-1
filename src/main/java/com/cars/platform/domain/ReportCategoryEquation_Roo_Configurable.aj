// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.ReportCategoryEquation;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect ReportCategoryEquation_Roo_Configurable {
    
    declare @type: ReportCategoryEquation: @Configurable;
    
}
