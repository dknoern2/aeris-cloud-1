package com.cars.platform.domain;


public enum CompanyType {

    CDFI, CARS, INVESTOR;
}
