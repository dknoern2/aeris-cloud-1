package com.cars.platform.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Provides the data to represent a single breakdown equation in pie chart format.
 * @author kurtl
 *
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class BreakdownPie extends BreakdownSingle {

	@Override
	public GraphType getGraphType() {
		return GraphType.PIE;
	}

	@Override
	public String getType() {
		return "Pie Chart";
	}
}
