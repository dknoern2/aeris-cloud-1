// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.PeriodDim;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PeriodDim_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager PeriodDim.entityManager;
    
    public static final EntityManager PeriodDim.entityManager() {
        EntityManager em = new PeriodDim().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long PeriodDim.countPeriodDims() {
        return entityManager().createQuery("SELECT COUNT(o) FROM PeriodDim o", Long.class).getSingleResult();
    }
    
    public static List<PeriodDim> PeriodDim.findAllPeriodDims() {
        return entityManager().createQuery("SELECT o FROM PeriodDim o", PeriodDim.class).getResultList();
    }
    
    public static PeriodDim PeriodDim.findPeriodDim(Long id) {
        if (id == null) return null;
        return entityManager().find(PeriodDim.class, id);
    }
    
    public static List<PeriodDim> PeriodDim.findPeriodDimEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM PeriodDim o", PeriodDim.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void PeriodDim.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void PeriodDim.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            PeriodDim attached = PeriodDim.findPeriodDim(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void PeriodDim.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void PeriodDim.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public PeriodDim PeriodDim.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        PeriodDim merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
