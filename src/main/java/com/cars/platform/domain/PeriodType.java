package com.cars.platform.domain;

public enum PeriodType {
	DRAFT,INTERNAL, INTERIM, AUDIT;
}
