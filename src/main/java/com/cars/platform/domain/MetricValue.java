package com.cars.platform.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.cars.platform.util.DateHelper;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findMetricValuesByReportingPeriod", "findMetricValuesByQuarter",
	"findMetricValuesByMetric" })
public class MetricValue {

    private Double amount;

    private String textValue;

    @ManyToOne
    private Metric metric;

    @ManyToOne
    private Company company;

    private int year;

    private int quarter;

    // @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Comment> comments = new HashSet<Comment>();
    
    // if the associated Metric has a MetricBreakdown then this must be set to one if its MetricBreakdownItems
    @ManyToOne
    private MetricBreakdownItem breakdownItem;
    

    /**
     * Defaults constructor. Setting comments here as for some reason the field initializer above is not working.
     */
	public MetricValue() {
        super();
        comments = new HashSet<Comment>();
    }

    
    /**
     * Copy constructor used for transient purposes. Does not copy comments or breakdownItem
     * 
     * @param other
     */
    public MetricValue(MetricValue other) {
    	this.amount = other.amount;
    	this.textValue = other.textValue;
    	this.metric = other.metric;
    	this.company = other.company;
    	this.year = other.year;
    	this.quarter = other.quarter;
    }
    
    public Double getAmountValue() {
    	amount = getAmount();
    	textValue = getTextValue();
        if ((amount==null)&&(textValue!=null))
        {
           try{
               amount = Double.parseDouble(textValue.replace(",", ""));
           }
           catch(Exception ex){
           }
        }
        return amount;
    }
    
    public Period getPeriod() {
    	Period period = Period.findPeriod(company, year, quarter);
    	return period;
    }

/* not referenced TODO remove
    public static List<MetricValue> findMetricValues(long companyId) {
	EntityManager em = MetricValue.entityManager();
	TypedQuery<MetricValue> q = em.createQuery("SELECT o FROM MetricValue AS o WHERE o.company.id = :companyId",
		MetricValue.class);
	q.setParameter("companyId", companyId);
	return q.getResultList();
    }
*/
    
	public static List<MetricValue> findMetricValues(int quarter, int year, long companyId) {
		EntityManager em = MetricValue.entityManager();
		TypedQuery<MetricValue> q = em
				.createQuery(
						"SELECT o FROM MetricValue AS o WHERE o.quarter = :quarter AND o.year = :year AND o.company.id = :companyId",
						MetricValue.class);
		q.setParameter("quarter", quarter);
		q.setParameter("year", year);
		q.setParameter("companyId", companyId);
		return q.getResultList();
	}
    

	private static List<MetricValue> findBreakdownMetricValues(int quarter, int year, Company company) {
		EntityManager em = MetricValue.entityManager();
		TypedQuery<MetricValue> q = em
				.createQuery(
						"SELECT o FROM MetricValue AS o WHERE o.quarter = :quarter AND o.year = :year AND o.company = :company and o.breakdownItem is not null",
						MetricValue.class);
		q.setParameter("quarter", quarter);
		q.setParameter("year", year);
		q.setParameter("company", company);
		return q.getResultList();
	}
	
	private static List<MetricValue> findNonBreakdownMetricValues(int quarter, int year, Company company) {
		EntityManager em = MetricValue.entityManager();
		TypedQuery<MetricValue> q = em
				.createQuery(
						"SELECT o FROM MetricValue AS o WHERE o.quarter = :quarter AND o.year = :year AND o.company = :company and o.breakdownItem is null",
						MetricValue.class);
		q.setParameter("quarter", quarter);
		q.setParameter("year", year);
		q.setParameter("company", company);
		return q.getResultList();
	}


    public String getMetricName() {
	return metric.getName();
    }


    /**
     * Returns all MetricValues for the provided Company that DO NOT have an associated breakdownItem
     */
	public static List<com.cars.platform.domain.MetricValue> findMetricValues(Company company) {
		if (company == null)
			throw new IllegalArgumentException("The company argument is required");
		EntityManager em = MetricValue.entityManager();
		TypedQuery<MetricValue> q = em
				.createQuery(
						"SELECT MetricValue FROM MetricValue AS metricValue WHERE metricValue.company = :company and metricValue.breakdownItem is null",
						MetricValue.class);
		q.setParameter("company", company);
		return q.getResultList();
	}
	
    /**
     * Returns all MetricValues for the provided Company that DO have an associated breakdownItem
     */	
    public static List<MetricValue> findBreakdownMetricValues(Company company) {
		EntityManager em = MetricValue.entityManager();
		TypedQuery<MetricValue> q = em.createQuery("SELECT o FROM MetricValue AS o WHERE o.company = :company and o.breakdownItem is not null",
			MetricValue.class);
		q.setParameter("company", company);
		return q.getResultList();
    }

    
    

    public static MetricValue findMetricValue(int quarter, int year, long companyId, long metricId) {

	MetricValue metricValue = null;
	EntityManager em = MetricValue.entityManager();
	TypedQuery<MetricValue> q = em
		.createQuery(
			"SELECT o FROM MetricValue AS o WHERE o.quarter = :quarter AND o.year = :year AND o.company.id = :companyId AND o.metric.id = :metricId",
			MetricValue.class);
	q.setParameter("quarter", quarter);
	q.setParameter("year", year);
	q.setParameter("companyId", companyId);
	q.setParameter("metricId", metricId);

	try {
	    metricValue = q.getSingleResult();
	} catch (Exception e) {
	    // return null if not found
	}
	return metricValue;
    }
    
    
/* TODO remove dead code
    public static List<MetricValue> findNonEmptyMetricValues(int quarter, int year, long companyId) {

	List<MetricValue> metricValues = null;
	EntityManager em = MetricValue.entityManager();
	TypedQuery<MetricValue> q = em
		.createQuery(
			"SELECT o FROM MetricValue AS o WHERE o.quarter = :quarter AND o.year = :year AND o.company.id = :companyId AND (o.amount IS NOT NULL OR o.textValue IS NOT NULL)",
			MetricValue.class);
	q.setParameter("quarter", quarter);
	q.setParameter("year", year);
	q.setParameter("companyId", companyId);

	try {
	    metricValues = q.getResultList();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return metricValues;
    }

*/
    
    @Transactional
    public static void removeMetricValuesFromMetric(Metric metric) {
    	TypedQuery<MetricValue> q = MetricValue.findMetricValuesByMetric(metric);
		List<MetricValue> metricValues = q.getResultList();
		for(MetricValue metricValue : metricValues)
		{
			metricValue.remove();
		}
    }

    public static List<com.cars.platform.domain.MetricValue> findMetricValuesWithPendingComments() {
	EntityManager em = MetricValue.entityManager();
	TypedQuery<MetricValue> q = em.createQuery(
		"SELECT distinct o FROM MetricValue AS o join o.comments as c where c.pending=true", MetricValue.class);
	return q.getResultList();
    }

    public static List<com.cars.platform.domain.MetricValue> findMetricValueIdsWithComments(Company company) {
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = MetricValue.entityManager();
	TypedQuery<MetricValue> q = em.createQuery(
		"SELECT distinct o FROM MetricValue AS o join o.comments as c where o.company = :company",
		MetricValue.class);
	q.setParameter("company", company);
	return q.getResultList();
    }

    @Transactional
    public static void deleteMetricValues(int quarter, int year, Company company, boolean includeBreakdowns, boolean includeNonBreakdowns) {
	
    	List<MetricValue> metricValues = null;
    	if (includeBreakdowns && includeNonBreakdowns) {
    		metricValues = MetricValue.findMetricValues(quarter, year, company.getId());
    	}
    	else if (includeNonBreakdowns) {
    		metricValues = MetricValue.findNonBreakdownMetricValues(quarter, year, company);    		
    	}
    	else if (includeBreakdowns) {
    		metricValues = MetricValue.findBreakdownMetricValues(quarter, year, company);
    	}
    	else {
    		throw new IllegalArgumentException("Either includeBreakdowns or includeNonBreakdowns (or both) must be set");
    	}
    	for(MetricValue metricValue : metricValues)
    	{
    		metricValue.remove();
    	}
    }
    


    public static Period getMostRecentReportedPeriod(Company company) {

	Period period = null;
	int year = 0;
	int quarter = 0;
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = MetricValue.entityManager();
	Query q = em.createQuery("select max(year) FROM MetricValue o WHERE o.company = :company");
	q.setParameter("company", company);
	Object maxCode = q.getSingleResult();

	if (maxCode == null)
	    return null;

	year = Integer.parseInt(maxCode.toString());

	if (year > 0) {

	    Query q2 = em
		    .createQuery("select max(quarter) FROM MetricValue o WHERE o.company = :company and o.year = :year");
	    q2.setParameter("company", company);
	    q2.setParameter("year", year);
	    Object maxCode2 = q2.getSingleResult();
	    quarter = Integer.parseInt(maxCode2.toString());
	}

	if (quarter > 0) {
	    period = new Period();
	    period.setCompany(company);
	    // assume most recent quarter has current default FYE for compny
	    period.setEndDate(DateHelper.getQuarterEndDate(year, quarter, company.getFiscalYearStart()));
	    period.setYear(year);
	    period.setQuarter(quarter);
	}

	return period;
    }


}
