package com.cars.platform.domain;


public enum ReportType {

    NONE, FULL, OPINION, ANNUAL;
}

