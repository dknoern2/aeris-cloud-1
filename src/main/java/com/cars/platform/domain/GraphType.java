package com.cars.platform.domain;


public enum GraphType {

    LINE, COLUMN, PIE, TABLE;
}
