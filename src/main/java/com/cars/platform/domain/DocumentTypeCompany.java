package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Entity holding CDFI overrides for report periods
 * @author kurtl
 *
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord()
public class DocumentTypeCompany {


    @ManyToOne
	@NotNull
    private DocumentType documentType;
    
    @ManyToOne
	@NotNull
    private Company company;

    private boolean quarterly;
    private boolean annual;
    private boolean full;
    private boolean annualIsAvailable;
    
    
    public DocumentTypeCompany() {
		super();
	}
    
    
    public DocumentTypeCompany(DocumentType docType, Company company) {
    	this.documentType = docType;
    	this.company = company;
    	this.setQuarterly(docType.isQuarterly());
    	this.setAnnual(docType.isAnnual());
    	this.setFull(docType.isFull());
    	this.setAnnualIfAvailable(docType.isAnnualIfAvailable());
    }



	public static DocumentTypeCompany findForTypeAndCompany(DocumentType documentType, Company company) {
           
		EntityManager em = Company.entityManager();
		TypedQuery<DocumentTypeCompany> q = em
				.createQuery(
						"SELECT o FROM DocumentTypeCompany o where o.documentType = :documentType and o.company = :company",
						DocumentTypeCompany.class);
		q.setParameter("documentType", documentType);
		q.setParameter("company", company);
		List<DocumentTypeCompany> results = q.getResultList();
		
		if (null == results || results.size() == 0) {
			return null;
		}
		return results.get(0);
    }
	
	public static List<DocumentTypeCompany> findForCompany(Company company) {
        
		EntityManager em = Company.entityManager();
		TypedQuery<DocumentTypeCompany> q = em
				.createQuery(
						"SELECT o FROM DocumentTypeCompany o where o.company = :company",
						DocumentTypeCompany.class);
		q.setParameter("company", company);
		List<DocumentTypeCompany> results = q.getResultList();
		
		return results;
    }


 
}
