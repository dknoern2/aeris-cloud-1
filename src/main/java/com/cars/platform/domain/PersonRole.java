package com.cars.platform.domain;


public enum PersonRole {

    BASIC, ADMIN;
}
