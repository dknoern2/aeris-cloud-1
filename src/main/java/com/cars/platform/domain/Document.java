package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDocumentsByCompany", "findDocumentsBySubmittedDateBetween" })
public class Document {

    @ManyToOne
    private DocumentType documentType;

    private int fiscalYear;

    private int fiscalQuarter;

    private String fileName;

    @ManyToOne
    private Company company;

    @ManyToOne
    private Person submittedBy;

    @ManyToOne
    private Person approvedBy;
    
    @ManyToOne
    private Person checkedOutBy;
    
    @OneToMany(mappedBy = "document", cascade=CascadeType.ALL, orphanRemoval=true)
    private Set<Activity> activities;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date submittedDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date approvedDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date checkedOutDate;    
    
    @ManyToMany
    private List<Company> authorizedSubscribers;

    private boolean allSubscribers = false;

    private boolean allAnalysts = true;
    
    public boolean isCarsFull() {
    	return this.getDocumentType().getReportType() == ReportType.FULL;
    }
    
    public boolean isCarsOpinion() {
    	return this.getDocumentType().getReportType() == ReportType.OPINION;
    }

    public boolean isCarsAnnual() {
    	return this.getDocumentType().getReportType() == ReportType.ANNUAL;
    }


    public static List<Document> findDocument(Company company, DocumentType documentType, int fiscalYear, int fiscalQuarter) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (documentType == null) throw new IllegalArgumentException("The document type argument is required");
        EntityManager em = MetricValue.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT Document FROM Document AS document WHERE document.fiscalYear = :fiscalYear and document.fiscalQuarter = :fiscalQuarter and document.company = :company and document.documentType = :documentType", Document.class);
        q.setParameter("company", company);
        q.setParameter("documentType", documentType);
        q.setParameter("fiscalYear", fiscalYear);
        q.setParameter("fiscalQuarter", fiscalQuarter);
        return q.getResultList();
    }
    
    public static List<Document> findQuarterlyDocuments(Company company, int fiscalYear, int fiscalQuarter) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = MetricValue.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT Document FROM Document AS document JOIN document.documentType AS dt WHERE document.fiscalYear = :fiscalYear and document.fiscalQuarter = :fiscalQuarter and document.company = :company and dt.quarterly = 1", Document.class);
        q.setParameter("company", company);
        q.setParameter("fiscalYear", fiscalYear);
        q.setParameter("fiscalQuarter", fiscalQuarter);
        return q.getResultList();
    }


    public static List<Document> getDocuments(Company company, Person person) {
        List<Document> docs = new ArrayList<Document>();
        List<Document> allDocs = Document.findDocumentsByCompany(company).getResultList();
        PersonRole personRole = person.getPersonRole();
        CompanyType companyType = person.getCompany().getCompanyType();
        for (Document doc : allDocs) {
            boolean include = false;
            if (companyType.equals(CompanyType.CARS)) {
                if (personRole.equals(PersonRole.ADMIN)) {
                    include = true;
                } else if (doc.isAllAnalysts()) {
                    include = true;
                } else if (doc.getAuthorizedSubscribers().contains(person.getCompany())) {
                    include = true;
                }
            } else if (companyType.equals(CompanyType.CDFI)) {
                if (doc.getCompany().equals(person.getCompany()) && !"Historic Financial Data".equals(doc.getDocumentType().getName())) {
                    include = true;
                } else if (doc.getAuthorizedSubscribers().contains(person.getCompany())) {
                    include = true;
                }
            } else {
                if (doc.allSubscribers) {
                    include = true;
                } else if (doc.getAuthorizedSubscribers().contains(person.getCompany())) {
                    include = true;
                }
            }
            if (include) {
                docs.add(doc);
            }
        }
        return docs;
    }
    
    
    /**
     * Global documents are templates that Subscribers/CDFIs can use for creating their own files to upload
     */
    public static List<Document>findGlobalDocuments() {
        EntityManager em = Document.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT o FROM Document AS o WHERE o.company is null", Document.class);
        return q.getResultList();
    }

    public static Document findDocument(Company company, DocumentType documentType, int fiscalYear, int fiscalQuarter, String fileName) {
        Document document = null;
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (documentType == null) throw new IllegalArgumentException("The document type argument is required");
        if (fileName == null) throw new IllegalArgumentException("The filename argument is required");
        EntityManager em = MetricValue.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT Document FROM Document AS document WHERE document.fiscalYear = :fiscalYear " + "and document.fiscalQuarter = :fiscalQuarter and document.company = :company " + "and document.documentType = :documentType and fileName = :fileName", Document.class);
        q.setParameter("company", company);
        q.setParameter("documentType", documentType);
        q.setParameter("fiscalYear", fiscalYear);
        q.setParameter("fiscalQuarter", fiscalQuarter);
        q.setParameter("fileName", fileName);
        List<Document> list = q.getResultList();
        if (list != null && list.size() > 0) document = list.get(0);
        return document;
    }



    public static List<Document> findNewApprovedDocumentsByDocumentTypeFolder(String documentTypeFolderName, Date fromDate, Date toDate) {
        if (documentTypeFolderName == null) throw new IllegalArgumentException("The document type category argument is required");
        if (fromDate == null) throw new IllegalArgumentException("The from date argument is required");
        if (toDate == null) throw new IllegalArgumentException("The to date argument is required");
        EntityManager em = Document.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT d FROM Document AS d WHERE d.documentType.documentTypeFolder.name = :documentTypeFolderName and d.approvedDate is not null and d.approvedDate BETWEEN :fromDate AND :toDate", Document.class);
        q.setParameter("documentTypeFolderName", documentTypeFolderName);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        return q.getResultList();
    }

    public static List<Document> findNewApprovedDocumentsByDocumentTypeFolderAndCompany(String documentTypeFolderName, Company company, Date fromDate, Date toDate) {
        if (documentTypeFolderName == null) throw new IllegalArgumentException("The document type folder argument is required");
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (fromDate == null) throw new IllegalArgumentException("The from date argument is required");
        if (toDate == null) throw new IllegalArgumentException("The to date argument is required");
        EntityManager em = Document.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT d FROM Document AS d WHERE d.documentType.documentTypeFolder.name = :documentTypeFolderName and d.company =:company and d.approvedDate is not null and d.approvedDate BETWEEN :fromDate AND :toDate", Document.class);
        q.setParameter("documentTypeFolderName", documentTypeFolderName);
        q.setParameter("company", company);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        return q.getResultList();
    }        
    
	public static List<Document> findDocument(Company company,DocumentType documentType) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (documentType == null) throw new IllegalArgumentException("The document type argument is required");
        EntityManager em = MetricValue.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT Document FROM Document AS document WHERE document.company = :company and document.documentType = :documentType order by document.fiscalYear desc, document.fiscalQuarter desc  ", Document.class);
        q.setParameter("company", company);
        q.setParameter("documentType", documentType);
        return q.getResultList();	
	}  
	
	
	
	

	public static TypedQuery<Document> findDocumentsByCompany(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Document.entityManager();
        TypedQuery<Document> q = em.createQuery("SELECT o FROM Document AS o WHERE o.company = :company ORDER BY o.fiscalYear DESC, o.fiscalQuarter DESC", Document.class);
        q.setParameter("company", company);
        return q;
    }
	
	
	/**
	 * Provides all the Documents for a Company keyed by DocumentType's ID
	 */
	public static Map<Long, List<Document>> findDocumentsByTypeForCompany(Company company) {
		Map<Long, List<Document>> result = new HashMap<Long, List<Document>>();
		List<Document> allDocs = findDocumentsByCompany(company).getResultList();
		for (Document doc : allDocs) {
			DocumentType docType = doc.getDocumentType();
			List<Document> docsByType = result.get(docType.getId());
			if (null == docsByType) {
				docsByType = new ArrayList<Document>();
				result.put(docType.getId(), docsByType);
			}
			docsByType.add(doc);
		}
		return result;
	}
}
