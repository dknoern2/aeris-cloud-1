package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Graph {

	@ManyToOne
	private Company company;
	private String title;

	private String code;
	private GraphType graphType;

	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="graph")
	@OrderBy("rank ASC")
	private List<GraphEquation> graphEquations;

	private UnitType unitType = UnitType.DOLLAR;

	private boolean showInterim = false;
	
	private boolean forFactSheet = false;

	private int showYears = 10;

	@Size(max = 512)
	@Column(length=512) 
	private String notes;
	
	@Transient
	private boolean overridden=false;

	// In the Fact Sheet representation of graphs there is a special case where
	// we do not show the second FYE (when quarter = 5). This is a transient property
	// set in the FactSheetService to control this (ugh)
	@Transient
	private boolean supressExtraFYE=false;
	
	
	public static List<Graph> findGraphsByCompany(Company company) {
		if (company == null)
			throw new IllegalArgumentException(
					"The company argument is required");
		EntityManager em = Graph.entityManager();
		TypedQuery<Graph> q = em
				.createQuery(
						"SELECT Graph FROM Graph AS graph WHERE graph.company = :company order by graph.code",
						Graph.class);
		q.setParameter("company", company);
		return q.getResultList();
	}
	
	public static List<com.cars.platform.domain.Graph> findAllGraphsByCompany(
			Company company) {
		if (company == null)
			throw new IllegalArgumentException(
					"The company argument is required");
		EntityManager em = Graph.entityManager();
		// TypedQuery<Graph> q =
		// em.createQuery("SELECT Graph FROM Graph AS graph WHERE graph.company is null or graph.company = :company order by company,code",
		// Graph.class);
		TypedQuery<Graph> q = em
				.createQuery(
						"SELECT Graph FROM Graph AS graph WHERE graph.company is null or graph.company = :company order by cast(code, int), graph.company",
						Graph.class);
		q.setParameter("company", company);

		List<Graph> rawList = q.getResultList();

		// filter list, remove entries where global graph is overridden by
		// company-specific
		List<Graph> list = new ArrayList<Graph>();

		if (rawList.size() > 0) {

			LinkedHashMap<String, Graph> map = new LinkedHashMap<String, Graph>();

			for (Graph graph : rawList) {				
				Graph graphInMap = map.get(graph.getCode());
				if (graphInMap == null) {
					map.put(graph.getCode(), graph);
				} else if (graphInMap != null) {
					graph.setOverridden(true);
					map.put(graph.getCode(), graph);
				}
			}

			list.addAll(map.values());
		}

		return list;
	}

	public static List<com.cars.platform.domain.Graph> findGlobalGraphs() {
		EntityManager em = Graph.entityManager();
		TypedQuery<Graph> q = em
				.createQuery(
						"SELECT Graph FROM Graph AS graph WHERE graph.company is null order by graph.code",
						Graph.class);
		return q.getResultList();
	}
	
	public static Graph findGraphByCodeAndCompany(int code, Company company) {
		EntityManager em = Graph.entityManager();
		TypedQuery<Graph> q = em.createQuery(
			"SELECT Graph FROM Graph AS graph WHERE (graph.company is null or graph.company = :company) and cast(graph.code, int) = :code order by graph.company desc",
						Graph.class);
		q.setParameter("code", code);
		q.setParameter("company", company);
		
		List<Graph> graphs = q.getResultList();
		if (null == graphs || graphs.size() == 0) {
			return null;
		}
		
		return graphs.get(0);
	}


	/**
	 * Provides a list of Equations for this report graph and company sorted by rank
	 * @param company
	 * @return All equations, both global and company specific for this graph.
	 * Note that if a company has overridden a global equation only the company override will be included.
	 */
	public List<Equation> getEquations(Company company) {
		List<Equation> rawEquations = GraphEquation.findEquationsByGraph(this);

		// If we are getting only global equations there are no overrides
		if (null == company) {
			return rawEquations;
		}

		// Replace global equations which have company specific overrides
		List<Equation> result = new ArrayList<Equation>();
		for (Equation equation : rawEquations) {
			Equation override = equation.getCompanyOverride(company);
			if (null == override) {
				result.add(equation);
			}
			else {
				result.add(override);
			}
		}
		return result;
	}

	/**
	 * Updates the equations for this graph.
	 * @param equations contains the list of equations for this graph in the order they should be displayed
	 */
	public void updateEquations(List<Equation> equations) {
		//List<ReportCategoryEquation> newRces = new ArrayList<ReportCategoryEquation>(getCategoryEquations());
		List<GraphEquation> ges = this.getGraphEquations();
		ges.clear();
		for (Equation equation : equations) {
			GraphEquation newGe = new GraphEquation(this, equation, -1);
			ges.add(newGe);
		}

		// for all equations set the rank to the new values
		for (int i=0; i<ges.size(); i++) {
			GraphEquation ge = ges.get(i);
			ge.setRank(i);
		}
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	
	public Graph cloneGraph() {
	    Graph clone = new Graph();
	    clone.setCode(this.getCode());
	    clone.setGraphEquations(new ArrayList<GraphEquation>());
	    for (GraphEquation ge : this.getGraphEquations()) {
			GraphEquation equation = ge.clone(clone);
			clone.getGraphEquations().add(equation);
		}
	    clone.setGraphType(this.getGraphType());
	    clone.setNotes(this.getNotes());
	    clone.setShowInterim(this.isShowInterim());
	    clone.setShowYears(this.getShowYears());
	    clone.setTitle(this.getTitle());
	    clone.setUnitType(this.getUnitType());
	    clone.setVersion(this.getVersion());
	    return clone;
	}

	public boolean isOverridden() {
		return overridden;
	}

	public void setOverridden(boolean overridden) {
		this.overridden = overridden;
	}

	public boolean isSupressExtraFYE() {
		return supressExtraFYE;
	}

	public void setSupressExtraFYE(boolean supressExtraFYE) {
		this.supressExtraFYE = supressExtraFYE;
	}

	public boolean isForFactSheet() {
		return forFactSheet;
	}

	public void setForFactSheet(boolean forFactSheet) {
		this.forFactSheet = forFactSheet;
	}
}
