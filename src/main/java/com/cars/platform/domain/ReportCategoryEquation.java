package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ReportCategoryEquation {

    @ManyToOne
    private ReportCategory reportCategory;

    @ManyToOne
    private Equation equation;

    private int rank;
    
    
    public ReportCategoryEquation(ReportCategoryEquation rce) {
    	this.reportCategory = rce.reportCategory;
    	this.equation = rce.equation;
    	this.rank = rce.rank;
    }
    
    public ReportCategoryEquation(ReportCategory reportCategory, Equation equation, int rank) {
    	this.reportCategory = reportCategory;
    	this.equation = equation;
    	this.rank = rank;
    }

    
    
    public static List<Equation> findEquationsByReportCategoryAndCompany(ReportCategory reportCategory, Company company, boolean allEquations) {
        if (reportCategory == null) throw new IllegalArgumentException("The reportCategory argument is required");
        EntityManager em = ReportCategoryEquation.entityManager();
        TypedQuery<Equation> q;
        if (null == company) {
        	if (allEquations)
        		q = em.createQuery("SELECT e FROM ReportCategoryEquation AS o JOIN o.equation AS e WHERE o.reportCategory = :reportCategory order by o.rank", Equation.class);
        	else
        		q = em.createQuery("SELECT e FROM ReportCategoryEquation AS o JOIN o.equation AS e WHERE o.reportCategory = :reportCategory and e.company is null order by o.rank", Equation.class);
        }
        else {
        	q = em.createQuery("SELECT e FROM ReportCategoryEquation AS o JOIN o.equation AS e WHERE o.reportCategory = :reportCategory and (e.company is null or e.company = :company) order by o.rank,e.company", Equation.class);
            q.setParameter("company", company);
        }
        q.setParameter("reportCategory", reportCategory);
        
        return q.getResultList();
    }
}
