

package com.cars.platform.domain;

import java.util.List;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class TargetBeneficiary {
	
	@Size(min = 2, max = 30)
    private String name;

	public static List<TargetBeneficiary> findAllTargetBeneficiaries() {
        return entityManager().createQuery("SELECT o FROM TargetBeneficiary o order by name", TargetBeneficiary.class).getResultList();
    }
}
