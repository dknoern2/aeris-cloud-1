package com.cars.platform.domain;


public enum MetricType {
    NUMERIC, TEXT, BOTH;
}
