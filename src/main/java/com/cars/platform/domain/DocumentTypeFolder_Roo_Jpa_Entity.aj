// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.DocumentTypeFolder;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

privileged aspect DocumentTypeFolder_Roo_Jpa_Entity {
    
    declare @type: DocumentTypeFolder: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long DocumentTypeFolder.id;
    
    public Long DocumentTypeFolder.getId() {
        return this.id;
    }
    
    public void DocumentTypeFolder.setId(Long id) {
        this.id = id;
    }
}
