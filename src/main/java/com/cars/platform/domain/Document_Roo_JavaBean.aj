// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Person;
import java.util.Date;
import java.util.List;

privileged aspect Document_Roo_JavaBean {
    
    public DocumentType Document.getDocumentType() {
        return this.documentType;
    }
    
    public void Document.setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }
    
    public int Document.getFiscalYear() {
        return this.fiscalYear;
    }
    
    public void Document.setFiscalYear(int fiscalYear) {
        this.fiscalYear = fiscalYear;
    }
    
    public int Document.getFiscalQuarter() {
        return this.fiscalQuarter;
    }
    
    public void Document.setFiscalQuarter(int fiscalQuarter) {
        this.fiscalQuarter = fiscalQuarter;
    }
    
    public String Document.getFileName() {
        return this.fileName;
    }
    
    public void Document.setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public Company Document.getCompany() {
        return this.company;
    }
    
    public void Document.setCompany(Company company) {
        this.company = company;
    }
    
    public Person Document.getSubmittedBy() {
        return this.submittedBy;
    }
    
    public void Document.setSubmittedBy(Person submittedBy) {
        this.submittedBy = submittedBy;
    }
    
    public Person Document.getApprovedBy() {
        return this.approvedBy;
    }
    
    public void Document.setApprovedBy(Person approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    public Person Document.getCheckedOutBy() {
        return this.checkedOutBy;
    }
    
    public void Document.setCheckedOutBy(Person checkedOutBy) {
        this.checkedOutBy = checkedOutBy;
    }
    
    public Date Document.getSubmittedDate() {
        return this.submittedDate;
    }
    
    public void Document.setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }
    
    public Date Document.getApprovedDate() {
        return this.approvedDate;
    }
    
    public void Document.setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }
    
    public Date Document.getCheckedOutDate() {
        return this.checkedOutDate;
    }
    
    public void Document.setCheckedOutDate(Date checkedOutDate) {
        this.checkedOutDate = checkedOutDate;
    }
    
    public List<Company> Document.getAuthorizedSubscribers() {
        return this.authorizedSubscribers;
    }
    
    public void Document.setAuthorizedSubscribers(List<Company> authorizedSubscribers) {
        this.authorizedSubscribers = authorizedSubscribers;
    }
    
    public boolean Document.isAllSubscribers() {
        return this.allSubscribers;
    }
    
    public void Document.setAllSubscribers(boolean allSubscribers) {
        this.allSubscribers = allSubscribers;
    }
    
    public boolean Document.isAllAnalysts() {
        return this.allAnalysts;
    }
    
    public void Document.setAllAnalysts(boolean allAnalysts) {
        this.allAnalysts = allAnalysts;
    }
    
}
