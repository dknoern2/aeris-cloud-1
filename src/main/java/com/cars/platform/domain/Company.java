package com.cars.platform.domain;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.util.DateHelper;

@RooJavaBean
@RooToString(excludeFields={"requiredMetrics", "analysts"})
@RooJpaActiveRecord(finders = { "findCompanysByCompanyType" })
public class Company implements Serializable, Comparable<Company> {

    //@ManyToMany(fetch=FetchType.EAGER) // in order to avoid no session error in ActionItemService
    @ManyToMany 
    List<Metric> requiredMetrics;

    @NotNull
    @Size(min = 2)
    private String name;

    @Enumerated
    private CompanyType companyType = CompanyType.INVESTOR;

    @Enumerated
    private TaxType taxType = TaxType.NON_PROFIT;
    
    
    private String address;

    private String city;

    private String state;

    private String zip;

    private String phone;

    private int fiscalYearStart = 1;
    
    private int startRequestingYear = DateHelper.getCurrentYear();
    
    private int startRequestingQuarter = 2;

    private boolean rated = true;
    
    private boolean showRatingsTab = true;
    
    // Can subscribers see this CDFI's financials
    @Enumerated
    private ShareFinancials shareFinancials = ShareFinancials.SPECIFIC;
    
    // Can this CDFI be included in a peer group
    private boolean peerGroupAllowed = true;

    private String logo;

    private String address2;

    private String fax;

    private boolean allRows = false;

    @ManyToMany
    List<State> areaServed;

    private String url;

    @Size(max = 1000)
    private String description;
    
    @Size(max = 200)
    private String mission;
    
    // Employee Identification Number, for non profits
    private String EIN;

    @ManyToMany
    List<LendingType> lendingType;
    
    @ManyToMany
    private List<SectoralFocus> sectoralFocus;
    
    @ManyToMany
    private List<TargetBeneficiary> targetBeneficiary;

    private String orgType;

    private boolean active = false;

    @ManyToMany
    private List<Person> analysts;

    private boolean suppress = false;

    private boolean hideActivity = false;
    
    private Date activeDate;

    
    
    // accessors for sectoralFocus and mission weren't being added to Company_Roo_JavaBean for some reason
    public List<SectoralFocus> getSectoralFocus() {
		return sectoralFocus;
	}
	public void setSectoralFocus(List<SectoralFocus> sectoralFocus) {
		this.sectoralFocus = sectoralFocus;
	}
	public List<TargetBeneficiary> getTargetBeneficiary(){
		return targetBeneficiary;
	}
	public void setTargetBeneficiary(List<TargetBeneficiary> targetBeneficiary){
		this.targetBeneficiary = targetBeneficiary;
	}
	public String getMission() {
		return mission;
	}
	public void setMission(String mission) {
		this.mission = mission;
	}
	
	public String getEIN() {
		return EIN;
	}
	public void setEIN(String eIN) {
		EIN = eIN;
	}
	public Date getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(Date activeDate) {
		if (null == this.activeDate) {
		    this.activeDate = activeDate;
		}
	}
	
	public boolean isActive() {
        return this.active;
    }

	public void setActive(boolean active) {
		if (!this.active && active && null == this.activeDate) {
			setActiveDate(new Date());
		}
        this.active = active;
    }
	
	public boolean isEmailValid() {		
		boolean ans = false;
		List<Person> persons = Person.findPeopleByCompany(this).getResultList();
		for (Person person : persons) {
			String emailAddress = person.getEmail();
			if (null == emailAddress)
				continue;
			
			try {
				InternetAddress.parse(emailAddress, true);
				ans = true;
				break;
			} catch (AddressException e) {
				ans = false;
			}				
		}
			
		return ans;
	}
	
	public boolean isCDFI() {
        return CompanyType.CDFI.equals(companyType);
    }

    public boolean isInvestor() {
        return companyType.equals(CompanyType.INVESTOR);
    }

    public boolean isCARS() {
        return companyType.equals(CompanyType.CARS);
    }

    public String getFiscalYearStartString() {
        switch(fiscalYearStart) {
            case 1:
                return "January 01";
            case 2:
                return "February 01";
            case 3:
                return "March 01";
            case 4:
                return "April 01";
            case 5:
                return "May 01";
            case 6:
                return "June 01";
            case 7:
                return "July 01";
            case 8:
                return "August 01";
            case 9:
                return "September 01";
            case 10:
                return "October 01";
            case 11:
                return "November 01";
            case 12:
                return "December 01";
            default:
                return "";
        }
    }

    public String getFiscalYearEndString() {
        switch(fiscalYearStart) {
            case 1:
                return "December 31";
            case 2:
                return "January 31";
            case 3:
                return "February 28";
            case 4:
                return "March 31";
            case 5:
                return "April 30";
            case 6:
                return "May 31";
            case 7:
                return "June 30";
            case 8:
                return "July 31";
            case 9:
                return "August 31";
            case 10:
                return "September 30";
            case 11:
                return "October 31";
            case 12:
                return "November 30";
            default:
                return "";
        }
    }

    public String getAreaServedString() {
        boolean first = true;
        String areaServedString = "";
        for (State areaServed : this.getAreaServed()) {
            if (!first) areaServedString += ", ";
            first = false;
            areaServedString += areaServed.getStateCode();
        }
        return areaServedString;
    }

    public String getLendingTypeString() {
        boolean first = true;
        String lendingTypeString = "";
        for (LendingType lendingType : this.getLendingType()) {
            if (!first) lendingTypeString += ", ";
            first = false;
            lendingTypeString += lendingType.getName();
        }
        return lendingTypeString;
    }
    
    public String getSectoralFocusString() {
        boolean first = true;
        String sectoralFocusString = "";
        for (SectoralFocus sectoralFocus : this.getSectoralFocus()) {
            if (!first) sectoralFocusString += ", ";
            first = false;
            sectoralFocusString += sectoralFocus.getName();
        }
        return sectoralFocusString;
    }
    
    // since zip is stored as an int. Boston zip for example start with zero
    public String getZipString() {
    	// Initially zip was defined as an int (but a string in the database) this resulted in zips with leading zeros
    	// being stored as 4 digits
    	if (zip.length() == 4) {
    		int zipInt = Integer.parseInt(zip);
        	return String.format("%05d", zipInt);
    	}
    	
    	return zip;
    }

    public static List<com.cars.platform.domain.Company> findAllCompanysAlphabetically() {
        EntityManager em = Company.entityManager();
        return em.createQuery("SELECT o FROM Company o order by o.name", Company.class).getResultList();
    }

    public Review getMostRecentCompletedFullAnalysisReview() {
    	return Review.findMostRecentCompletedFullAnalysisReview(this);
    }
    
    public String getRating() {
        String rating = "";
        Review review = getMostRecentCompletedFullAnalysisReview();
        if (review != null) {
            if (review.getImpactPerformance() != null) rating += review.getImpactPerformance();
            if (review.isPolicyPlus()) rating += " policy plus ";
            if (review.getFinancialStrength() != null) rating += " " + review.getFinancialStrength();
        }
        return rating;
    }

    public String getRatedSince() {
        String ratedSince = "";
        Review review = Review.findEarliestCompletedFullAnalysisReview(this);
        if (review != null && review.getDateOfAnalysis() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(review.getDateOfAnalysis());
            ratedSince = Integer.toString(cal.get(Calendar.YEAR));
        }
        return ratedSince;
    }
    
    public Double getTotalAssets() {
    	Equation totalAssetsEquation = Equation.findTotalAssets();
    	Period period = Period.findMostRecentAuditOrInternalPeriod(this);
    	if (null == period) {
    		return null;
    	}
    	PeriodDim periodDim = PeriodDim.findByPeriod(period);
    	
    	ValueFact totalAssetFact = ValueFact.findValueFact(periodDim, totalAssetsEquation, this);
    	if (null == totalAssetFact) {
    		return null;
    	}
    	return totalAssetFact.getAmount();
    }
    
    public String getTotalAssetsString() {
    	Double totalAssets = getTotalAssets();
    	if (null == totalAssets) {
    		return "No financial data is currently available";
    	}
    	else {
    	    NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    	    currencyFormat.setMaximumFractionDigits(0);
    	    currencyFormat.setMinimumFractionDigits(0);
    	    return currencyFormat.format(totalAssets);
    	}
    }

    public static List<Company> findCompanysByCompanyTypeAlphabetically(CompanyType companyType) {
        if (companyType == null) throw new IllegalArgumentException("The companyType argument is required");
        EntityManager em = entityManager();
        TypedQuery<Company> q = em.createQuery("SELECT o FROM Company AS o WHERE o.companyType = :companyType order by o.name", Company.class);
        q.setParameter("companyType", companyType);
        return q.getResultList();
    }

    
    // these are the rules for a CDFI being 'available' by a subscriber
    // Is Active and (Is Rated or Share Financial or Allow in Peer Group)
    public static List<Company> findAvailableCDFIs() {
        EntityManager em = entityManager();
        TypedQuery<Company> q = em.createQuery(
        		"SELECT o FROM Company AS o WHERE o.companyType = :companyType and " + 
                "o.active = 1 and (o.rated = 1 or o.shareFinancials != :shareFinancials or o.peerGroupAllowed = 1) order by o.name", Company.class);
        q.setParameter("companyType", CompanyType.CDFI);
        q.setParameter("shareFinancials", ShareFinancials.NONE);
        return q.getResultList();
    }
    
    
    public static List<com.cars.platform.domain.Company> findRatedCDFIs() {
        EntityManager em = entityManager();
        TypedQuery<Company> q = em.createQuery(
        		"SELECT o FROM Company AS o WHERE o.companyType = :companyType and " +
                "o.rated = true and o.active = true order by o.name", Company.class);
        q.setParameter("companyType", CompanyType.CDFI);
        return q.getResultList();
    }

    public static Object findAllNonCARSCompanys() {
        EntityManager em = entityManager();
        // TypedQuery<Company> q = em.createQuery("SELECT o FROM Company AS o WHERE o.companyType != :companyType and o.rated = true order by o.name", Company.class);
        // allow to get not rated companies
        TypedQuery<Company> q = em.createQuery("SELECT o FROM Company AS o WHERE o.companyType != :companyType order by o.name", Company.class);
        q.setParameter("companyType", CompanyType.CARS);
        return q.getResultList();
    }

    public static List<com.cars.platform.domain.Company> findCompanysByAnalyst(Person person) {
        if (person == null) throw new IllegalArgumentException("The person argument is required");
        EntityManager em = entityManager();
        TypedQuery<Company> q = em.createQuery("SELECT c FROM Company AS c join c.analysts as a where a = :person order by c.name", Company.class);
        q.setParameter("person", person);
        return q.getResultList();
    }
    
    
    public static List<com.cars.platform.domain.Company> findActiveCdfisSince(Date sinceDate) {
        if (sinceDate == null) throw new IllegalArgumentException("The sinceDate argument is required");
        EntityManager em = entityManager();
        TypedQuery<Company> q = em.createQuery("SELECT c FROM Company AS c where c.active = 1 and c.activeDate > :sinceDate and c.companyType = :type order by c.name", Company.class);
        q.setParameter("sinceDate", sinceDate);
        q.setParameter("type", CompanyType.CDFI);
        return q.getResultList();
    }
    
    public static List<com.cars.platform.domain.Company> findSubscriersWithAllPeerGroupSubscription() {
        EntityManager em = entityManager();
        TypedQuery<Company> q = em.createQuery("SELECT o FROM Subscription AS s JOIN s.owner as o where o.companyType = :companyType and s.subscriptionType = :subscriptionType and s.peerGroups = 1 and s.expirationDate > CURRENT_DATE group by o.id", Company.class);
        q.setParameter("companyType", CompanyType.INVESTOR);
        q.setParameter("subscriptionType", SubscriptionType.ALL);
        return q.getResultList();
    }
    
	@Override
	public int compareTo(Company o) {
		return this.name.compareTo(o.name);
	}

}
