package com.cars.platform.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ValueFact {
	
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;
    
    @NotNull
    // For performance in PeerGroup.getComparisonData. Company and periodDim are already fetched as they are part of the query
    @ManyToOne(fetch = FetchType.LAZY) 
    private Equation equation;
    
    @NotNull
    @ManyToOne
    private PeriodDim periodDim;
   
    @NotNull
    private Double amount;
    
    // TODO make amount nullable and store the required amount text
    //@NotNull
    private String amountText = "CHANGE_ME";
    
    @Size(max=1024)
    @Column(length=1024) 
    private String spreadFormula;
    
    public String getSpreadFormula() {
		return spreadFormula;
	}

	public void setSpreadFormula(String spreadFormula) {
		this.spreadFormula = spreadFormula;
	}

	@NotNull
    @Enumerated
    private PeriodType periodType = PeriodType.DRAFT;
	
    
    @Transactional
    public static void deleteForCompany(Company company) {
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = entityManager();
	Query q = em
		.createQuery("DELETE FROM ValueFact AS o WHERE o.company = :company");
	q.setParameter("company", company);
	q.executeUpdate();
    }
    
    public static List<ValueFact> findValueFacts(Company company, PeriodDim periodDim) {
    	if (null == company || null == periodDim) {
    	    throw new IllegalArgumentException("The company and periodDim arguments are required");
    	}

		EntityManager em = ValueFact.entityManager();
		TypedQuery<ValueFact> q = em
			.createQuery(
				"SELECT o FROM ValueFact AS o WHERE o.company = :company AND o.periodDim = :periodDim",
				ValueFact.class);
		q.setParameter("company", company);
		q.setParameter("periodDim", periodDim);
	
		return q.getResultList();
    }
    
    public static ValueFact findValueFact(PeriodDim periodDim, Equation equation, Company company ) {
    	if (null == company || null == periodDim || null == equation) {
    	    throw new IllegalArgumentException("The company, periodDim, and equation arguments are required");
    	}

		EntityManager em = ValueFact.entityManager();
		TypedQuery<ValueFact> q = em
			.createQuery(
				"SELECT o FROM ValueFact AS o WHERE o.company = :company AND o.periodDim = :periodDim and o.equation = :equation",
				ValueFact.class);
		q.setParameter("company", company);
		q.setParameter("periodDim", periodDim);
		q.setParameter("equation", equation);
	
		List<ValueFact> results = q.getResultList();
		if (null == results || results.size()==0) {
			return null;
		}
		else {
			return results.get(0);
		}
    }
    
    public static List<ValueFact> findForCompanyAndPeriod(Company company, PeriodDim periodDim) {
    	if (null == company || null == periodDim) {
    	    throw new IllegalArgumentException("The company and periodDim arguments are required");
    	}

		EntityManager em = ValueFact.entityManager();
		TypedQuery<ValueFact> q = em
			.createQuery(
				"SELECT o FROM ValueFact AS o WHERE o.company = :company AND o.periodDim = :periodDim",
				ValueFact.class);
		q.setParameter("company", company);
		q.setParameter("periodDim", periodDim);
	
		return q.getResultList();
    }

    
}
