package com.cars.platform.domain.constraint;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target( { TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = CheckBreakdownTableValidator.class)
public @interface CheckBreakdownTable {
    String message() default "All breakdown columns selected must have the same Metric Breakdown as this table.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}