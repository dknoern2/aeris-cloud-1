package com.cars.platform.service.export;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.cars.platform.domain.Period;
import com.cars.platform.service.export.ExportService;
import com.smartxls.WorkBook;


public class ExportServiceTest {
	
    private final String filename = "src/main/doc/Progress Fund CARS Financials 092512.xls";

	@Test
	public void testGetExcelColumn() {
		assertTrue(ExportService.getExcelColumn(0).equals("A"));
		assertTrue(ExportService.getExcelColumn(25).equals("Z"));
		assertTrue(ExportService.getExcelColumn(26).equals("AA"));
		assertTrue(ExportService.getExcelColumn(27).equals("AB"));
		assertTrue(ExportService.getExcelColumn(51).equals("AZ"));
		assertTrue(ExportService.getExcelColumn(52).equals("BA"));
	}
	
	@Test
	public void testIsLastQuarter() throws Exception {
		WorkBook wb = new WorkBook();
		wb.read(filename);
		wb.setSheet(2);
		final Map<String,Period> periodsByEndDate = new HashMap<String, Period> ();
		Period p1 = new Period();
		p1.setYear(2007);
		p1.setQuarter(3);
		Period p2 = new Period();
		p2.setYear(2008);
		p2.setQuarter(4);
		periodsByEndDate.put("12/31/2009", p1);
		periodsByEndDate.put("12/31/2008", p2);
		
		final ExportService exportService = new ExportService();
		
		assertFalse(exportService.isLastQuarter(wb, 6, periodsByEndDate));
		assertTrue(exportService.isLastQuarter(wb, 4, periodsByEndDate));
	}
	
	@Test
	public void testExtractPeriod() {
		final ExportService exportService = new ExportService();
		assertEquals("12/31/2012", exportService.extractPeriod("12/31/2012"));
		assertEquals("6/30/2012", exportService.extractPeriod("6/30/2012"));
		assertEquals("6/30/2012", exportService.extractPeriod("6/30/2012 (a)"));
		assertEquals("12/30/2012", exportService.extractPeriod("12/30/2012 (a)"));
		assertEquals("6/30/2012", exportService.extractPeriod("6/30/2012 (a) (b)"));
	}

}
