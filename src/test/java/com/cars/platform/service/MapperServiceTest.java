package com.cars.platform.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.mock.staticmock.AnnotationDrivenStaticEntityMockingControl;
import org.springframework.mock.staticmock.MockStaticEntityMethods;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Footnote;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricMap;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;

@RunWith(JUnit4.class)
@MockStaticEntityMethods
public class MapperServiceTest {

    private static final double DELTA = 0.001;

    private final Company company = new Company();
    private final int year = 2007;
    private final int quarter = 4;
    private final Period period = new Period();

    private final MetricMap metricMap = new MetricMap() {
	public MetricMap merge() {
	    return metricMap;
	};
    };
    
    private final List<MetricMap> metricMap2 = new ArrayList<MetricMap>();

    private final MetricMap positiveMap1 = new MetricMap();
    private final MetricMap positiveMap2 = new MetricMap();
    private final MetricMap negateMap1 = new MetricMap();
    private final MetricMap negateMap2 = new MetricMap();

    private final Metric metric = new Metric();

    private final String filename = "src/main/doc/Progress Fund CARS Financials 092512.xls";
    private InputStream is;

    private final MapperService2 mapperService = new MapperService2();

    @Before
    public void init() throws FileNotFoundException {
	is = new FileInputStream(filename);

	company.setId(1L);

	period.setPeriodType(PeriodType.AUDIT);
	period.setYear(year);
	period.setQuarter(quarter);
	period.setCompany(company);

	metric.setCompany(company);
	metric.setName("Cash and investments");
	metric.setType(MetricType.NUMERIC);
	metric.setId(1L);

	metricMap.setCompany(company);
	metricMap.setYear(year);
	metricMap.setQuarter(quarter);
	metricMap.setLabelText("Cash and investments for operations");
	metricMap.setLabelZ(0);
	metricMap.setLabelX(7);
	metricMap.setLabelY(0);
	metricMap.setValueX(7);
	metricMap.setValueY(1);
	metricMap.setMetric(metric);

	positiveMap1.setValueText("10");
	positiveMap1.setNegate(false);
	positiveMap2.setValueText("20");
	positiveMap2.setNegate(false);
	negateMap1.setValueText("-30");
	negateMap1.setNegate(true);
	negateMap2.setValueText("-40");
	negateMap2.setNegate(true);
    }

    /*
    @Test
    public void testMap() throws IOException {
	Period.findPeriod(company, year, quarter);
	AnnotationDrivenStaticEntityMockingControl.expectReturn(period);

	MetricMap.findMetricMaps(company, year, quarter);
	AnnotationDrivenStaticEntityMockingControl.expectReturn(Arrays.asList(metricMap));
	
	MetricMap.findMetricMapsBefore(company, metric, year, quarter);
	AnnotationDrivenStaticEntityMockingControl.expectReturn(metricMap2);
	AnnotationDrivenStaticEntityMockingControl.playback();

	int cells = mapperService.map(filename, is, company, year, quarter);
	Assert.assertEquals(1, cells);
    }
    */

    /*
     * @Test public void testMap() throws FileNotFoundException {
     * 
     * String filename =
     * "src/main/doc/Progress Fund CARS Financials 092512.xls"; String text =
     * "Cash and investments for operations"; MetricValue metricValue =
     * getSingleMetricValue(filename, text,7,1);
     * assertNotNull("no metric value returned", metricValue);
     * //assertEquals(1264774, metricValue.getAmountValue(), .001); }
     * 
     * @Test public void testMapForZero() throws FileNotFoundException {
     * 
     * String filename =
     * "src/main/doc/Progress Fund CARS Financials 092512.xls";
     * 
     * String text = "Certificates of Deposit"; MetricValue metricValue =
     * getSingleMetricValue(filename, text,10,1);
     * assertNotNull("no metric value returned", metricValue); assertEquals(0.0,
     * metricValue.getAmountValue(), .001); }
     * 
     * 
     * private MetricValue getSingleMetricValue(String filename, String text,int
     * row, int column) throws FileNotFoundException {
     * 
     * InputStream is = new FileInputStream(filename);
     * 
     * Metric metric = new Metric(); metric.setName(text);
     * 
     * List<MetricMap> metricMapList = new ArrayList<MetricMap>();
     * 
     * MetricMap metricMap = new MetricMap(); metricMap.setMetric(metric);
     * //metricMap.setMoveRight(moveRight); metricMap.setLabelText(text);
     * metricMap.setValueX(row); metricMap.setValueY(column);
     * 
     * 
     * metricMapList.add(metricMap);
     * 
     * MapperService mapperService = new MapperService();
     * 
     * List<MetricValue> metricValueList = null; try { metricValueList =
     * mapperService.map(is, metricMapList); } catch (IOException e) {
     * fail("IOException"); }
     * 
     * MetricValue metricValue = null;
     * 
     * if (metricValueList != null && metricValueList.size() == 1) { metricValue
     * = metricValueList.get(0); }
     * 
     * return metricValue; }
     */
    /*
     * @Test public void testMapHistoric() throws FileNotFoundException {
     * 
     * String filename =
     * "src/main/doc/Progress Fund CARS Financials 092512.xls";
     * 
     * InputStream is = new FileInputStream(filename);
     * 
     * MapperService mapperService = new MapperService();
     * 
     * List<Metric> metricList = new ArrayList<Metric>();
     * 
     * Metric metric = new Metric();
     * 
     * metric.setName("Loans Receivable"); metricList.add(metric);
     * 
     * metric = new Metric(); metric.setName("Collection");
     * metricList.add(metric);
     * 
     * metric = new Metric(); metric.setType(MetricType.TEXT);
     * metric.setName("Largest Donor"); metricList.add(metric);
     * 
     * 
     * 
     * Company company = new Company(); company.setFiscalYearStart(1);
     * 
     * List<MetricValue> metricValues =
     * mapperService.mapHistoric(is,company,metricList);
     * assertNotNull("no metric values returned", metricValues);
     * assertEquals("wrong number of metrics returned",13,metricValues.size());
     * 
     * 
     * }
     */

    @Test
    public void testMapHistoricFootnotes() throws FileNotFoundException {

	String filename = "src/main/doc/Progress Fund CARS Financials 092512.xls";

	InputStream is = new FileInputStream(filename);

	MapperService2 mapperService = new MapperService2();
	Company company = new Company();

	List<Footnote> footnotes = mapperService.mapHistoricFootnotes(
		"src/main/doc/Progress Fund CARS Financials 092512.xls", is, company);

	assertNotNull("no footnotes returned", footnotes);
	assertEquals("wrong number of footnotes returned", 3, footnotes.size());

	// found footnote: (a) TPF provided separate, in-house, unconsolidated
	// financial statements for TPF and TPF Loan Corporation at the March
	// 31, 2012 interim period. South Water Street LLC interim statements
	// were not provided. The CARS analysts combined the separate provided
	// statements in these spreads. Thus, the $495 descrepancy between total
	// assets and total liabilities and net assets is due to the combination
	// of provided statements.
	// footnote label raw: Investment in TPF Corporation Inc (a)

	// found footnote: (b) The museum collection is the assets of the
	// Windber Coal Heritage Center that TPF acquired in FY 1998 and sold in
	// FY 2010.
	// footnote label raw: Museum Collection (b)
	// found footnote: (c) Deferred revenue represents grants received that
	// TPF's auditors designated as deferred until used.
	// footnote label raw: Deferred Revenue (c)

	assertEquals(
		footnotes.get(0).getComment(),
		"TPF provided separate, in-house, unconsolidated financial statements for TPF and TPF Loan Corporation at the March 31, 2012 interim period. South Water Street LLC interim statements were not provided. The CARS analysts combined the separate provided statements in these spreads.  Thus, the $495 descrepancy between total assets and total liabilities and net assets is due to the combination of provided statements.");
	assertEquals(footnotes.get(0).getLabel(), "Investment in TPF Corporation Inc");
    }

    @Test
    public void testGetSum() {
	assertEquals(0.0, mapperService.getSum(Collections.<MetricMap> emptyList()), DELTA);
	assertEquals(10.0, mapperService.getSum(Arrays.asList(positiveMap1)), DELTA);
	assertEquals(30.0, mapperService.getSum(Arrays.asList(negateMap1)), DELTA);
	assertEquals(30.0, mapperService.getSum(Arrays.asList(positiveMap1, positiveMap2)), DELTA);
	assertEquals(70.0, mapperService.getSum(Arrays.asList(negateMap1, negateMap2)), DELTA);
	assertEquals(40.0, mapperService.getSum(Arrays.asList(positiveMap1, negateMap1)), DELTA);
    }

}
