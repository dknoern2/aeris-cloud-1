package com.cars.platform.service;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class EvaluatorTest {

	public static void main(String[] args) throws IOException {

		FileInputStream fis = new FileInputStream(
				"src/main/doc/CPCDC Financial Spreads 091712-exel.xls");
		Workbook wb = new HSSFWorkbook(fis); // or new
												// XSSFWorkbook("c:/temp/test.xls")
		Sheet sheet = wb.getSheet("Sum");
		FormulaEvaluator evaluator = wb.getCreationHelper()
				.createFormulaEvaluator();

		// suppose your formula is in B3

		String[] cells = { "I7", "I8", "I9", "I10", "I11", "I12", "I13", "I14",
				"I15" };

		for (String c : cells) {
			CellReference cellReference = new CellReference(c);
			Row row = sheet.getRow(cellReference.getRow());
			Cell cell = row.getCell(cellReference.getCol());

			if (cell == null) {
				System.out.println("NULL");
			} else {
				CellValue cellValue = evaluator.evaluate(cell);

				if (cellValue == null) {
					System.out.println("NULL");
				} else {
					switch (cellValue.getCellType()) {
					case Cell.CELL_TYPE_BOOLEAN:
						System.out.println(cellValue.getBooleanValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						System.out.println(cellValue.getNumberValue());
						break;
					case Cell.CELL_TYPE_STRING:
						System.out.println(cellValue.getStringValue());
						break;
					case Cell.CELL_TYPE_BLANK:
						break;
					case Cell.CELL_TYPE_ERROR:
						break;

					// CELL_TYPE_FORMULA will never happen
					case Cell.CELL_TYPE_FORMULA:
						break;
					}
				}
			}
		}
	}

}
