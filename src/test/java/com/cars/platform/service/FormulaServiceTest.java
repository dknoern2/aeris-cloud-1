package com.cars.platform.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.script.ScriptException;

import net.sourceforge.jeval.EvaluationException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.staticmock.AnnotationDrivenStaticEntityMockingControl;
import org.springframework.mock.staticmock.MockStaticEntityMethods;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.service.FormulaService.FormulaResult;
import com.cars.platform.util.DateHelper;

@MockStaticEntityMethods
public class FormulaServiceTest {

	@Test
	public void expandFormula() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));

		String formula = "(1010#+1020#)/1030#";

		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), null);

		assertEquals(27.5, expandedFormula, 0.001);

	}

	@Test
	public void hardStop() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|10101", createMetricValue(100.0,"10101"));
		metricValueMap.put("3/31/2012|101011", createMetricValue(10000.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(5000.0,"101012"));

		String formula = "10101!+10101#";

		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), null);

		assertEquals(100.0 + (100.0 + 10000.0 + 5000.0), expandedFormula, 0.001);

	}

	@Test
	public void testInifinity() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));

		String formula = "(1010#+1020#)/1030#";

		double expandedFormula = formulaService.crunchFormula(formula, "6/30/2012", metricValueMap, getTestPeriods(), null);
		assertEquals(Double.POSITIVE_INFINITY, expandedFormula, 0.001);

	}

	@Test
	public void testNull() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));
		String formula = "1010#+1020#";
		Double expandedFormula = formulaService.crunchFormula(formula, "9/30/2012", metricValueMap, getTestPeriods(), null);

		assertNull(expandedFormula);

	}

	@Test
	public void testZero() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"103001"));

		String formula = "1070#/1020#";

		Double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), null);
		assertEquals(0.0, expandedFormula, 0.001);
	}
	
	@Test
	public void testSpreadFormula() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0, "101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0, "101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0, "102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0, "103001"));
		metricValueMap.put("3/31/2012|900301", createMetricValue(0.0, "900301"));
		metricValueMap.put("3/31/2012|900401", createMetricValue(5.0, "900401"));
		metricValueMap.put("3/31/2012|66666", createMetricValue(5000.0, "66666"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0, "101012"));

		String formula = "9000#-1010#/(1020#+9001#-9002#+9003#-9004#)+55555!-66666!+MONTH";
		
		FormulaResult result = formulaService.crunchFormulaWithSpread(formula, "3/31/2012", metricValueMap, getTestPeriods(), null);

		if (! (result.spreadFormula.equals("-SUM(101011#,101012#)/(102012#-900401#)-66666#+(3.0)")
			||	result.spreadFormula.equals("-SUM(101012#,101011#)/(102012#-900401#)-66666#+(3.0)")))
		{
			fail("Incorrect spread formula: " + result.spreadFormula);
		}
	}

	
	@Test
	public void testSubFormula() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));
		
		Company company = new Company();
		company.setId(1L);
		Equation equation = new Equation();
		equation.setId(1L);
		equation.setCompany(company);
		equation.setFormula("1030#");
		

		String formula = "(1010#+1020#)/1F";
		//String formula = "(1010#+1020#)/1030#";
		
    	Equation.findEquation(1L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(equation);
    	AnnotationDrivenStaticEntityMockingControl.playback();


		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), company);

		assertEquals(27.5, expandedFormula, 0.001);

	}
	
	@Test
	public void testSubFormulaOverride() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("3/31/2012|104001", createMetricValue(2000.0,"104001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));
		
		Company company = new Company();
		company.setId(1L);
		Equation globalEquation = new Equation();
		globalEquation.setId(1L);
		globalEquation.setCompany(null);
		globalEquation.setFormula("1030#");
		Equation companyEquation = new Equation();
		companyEquation.setId(2L);
		companyEquation.setCompany(company);
		companyEquation.setOverride(globalEquation);
		companyEquation.setFormula("1040#");
		

		String formula = "(1010#+1020#)/1F";
		//String formula = "(1010#+1020#)/1030#";
		
    	Equation.findEquation(1L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(globalEquation);
    	Equation.findCompanyOverride(globalEquation, company);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(companyEquation);
    	AnnotationDrivenStaticEntityMockingControl.playback();


    	// evaluate for global uses 1030#
//		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), null);
//		assertEquals(27.5, expandedFormula, 0.001);
		
    	// evaluate for company uses 1040#
		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), company);
		assertEquals(13.75, expandedFormula, 0.001);

	}
	

	@Test
	public void testSubSubFormula() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("3/31/2012|104001", createMetricValue(2000.0,"104001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));
		
		Company company = new Company();
		company.setId(1L);
		Equation equation1 = new Equation();
		equation1.setId(1L);
		equation1.setCompany(null);
		equation1.setFormula("1030#");
		Equation equation2 = new Equation();
		equation2.setId(2L);
		equation2.setCompany(company);
		equation2.setFormula("1F + 1000");
		

		String formula = "(1010#+1020#)/2F";
		//String formula = "(1010#+1020#)/1030#";
		
    	Equation.findEquation(2L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(equation2);
    	Equation.findEquation(1L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(equation1);
    	Equation.findCompanyOverride(equation1, company);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(null);
    	AnnotationDrivenStaticEntityMockingControl.playback();

	
		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), company);
		assertEquals(13.75, expandedFormula, 0.001);

	}


	@Test
	public void testCircularSubFormula() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12500.0,"101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0,"101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0,"102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0,"103001"));
		metricValueMap.put("3/31/2012|104001", createMetricValue(2000.0,"104001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0,"101012"));
		
		Company company = new Company();
		company.setId(1L);
		Equation equation1 = new Equation();
		equation1.setId(1L);
		equation1.setCompany(null);
		equation1.setFormula("1030# - 2F");
		Equation equation2 = new Equation();
		equation2.setId(2L);
		equation2.setCompany(company);
		equation2.setFormula("1F + 1000");
		

		String formula = "(1010#+1020#)/2F";
		//String formula = "(1010#+1020#)/1030#";
		
    	Equation.findEquation(2L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(equation2);
    	Equation.findEquation(1L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(equation1);
    	Equation.findCompanyOverride(equation1, company);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(null);
    	Equation.findEquation(2L);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(equation2);
    	AnnotationDrivenStaticEntityMockingControl.playback();

		boolean failed = false;
    	try {
		double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), company);
    	}
    	catch (EvaluationException e) {
    		failed = true;
    	}
		Assert.assertTrue(failed);

	}




	
	private MetricValue createMetricValue(double amount, String accountCode) {
		MetricValue metricValue = new MetricValue();
		metricValue.setAmount(amount);
		Metric metric = new Metric();
		metric.setAccountCode(accountCode);
		metricValue.setMetric(metric);
		return metricValue;
	}


	@Test
	public void testMonths() throws NumberFormatException, ScriptException, EvaluationException {

		FormulaService formulaService = new FormulaService(1);

		HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
		metricValueMap.put("3/31/2012|101011", createMetricValue(12000.0, "101011"));
		metricValueMap.put("3/31/2012|101012", createMetricValue(10000.0, "101012"));
		metricValueMap.put("3/31/2012|102012", createMetricValue(5000.0, "102012"));
		metricValueMap.put("3/31/2012|103001", createMetricValue(1000.0, "103001"));
		metricValueMap.put("6/30/2012|101012", createMetricValue(8000.0, "101012"));
		String formula = "101011#/MONTH";
		Double expandedFormula = formulaService.crunchFormula(formula, "3/31/2012", metricValueMap, getTestPeriods(), null);

		assertEquals(4000.0, expandedFormula, 0.0);

	}

	@Test
	public void testGetMonths() throws ParseException {
		final int fiscalYearStart = 7;
		FormulaService formulaService = new FormulaService(fiscalYearStart);
		final int months = formulaService.getMonths("9/30/2012", Collections.<Period> emptyList());
		assertEquals(DateHelper.getQuarter(fiscalYearStart, "9/30/2012") * Period.MONTHS_IN_QUARTER, months);
		assertEquals(12, formulaService.getMonths("6/30/2010", getTestAuditPeriods()));
	}

	private static List<Period> getTestPeriods() {
		ArrayList<Period> list = new ArrayList<Period>();
		list.add(createPeriod("3/31/2012"));
		list.add(createPeriod("6/30/2012"));
		return list;
	}

	private static List<Period> getTestAuditPeriods() {
		ArrayList<Period> list = new ArrayList<Period>();
		Period period = new Period();
		period.setEndDate("6/30/2009");
		period.setPeriodType(PeriodType.AUDIT);
		list.add(period);
		period = new Period();
		period.setEndDate("6/30/2010");
		return list;
	}

	private static Period createPeriod(String endDate) {
		Period period = new Period();
		period.setEndDate(endDate);
		try {
			period.setYear(DateHelper.getYear(1, endDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return period;
	}

}
