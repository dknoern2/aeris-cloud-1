package com.cars.platform.domain;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PeriodTest {

    private final Period p1 = new Period();
    private final Period p2 = new Period();
    private final Period p3 = new Period();
    private final Set<Period> periods = new HashSet<Period>();

    private final Person admin = new Person();
    private final Person analyst = new Person();
    private final Person subscriber = new Person();
    private final Person cdfiUser = new Person();

    @Before
    public void init() {
	p1.setYear(2006);
	p1.setQuarter(4);
	p2.setYear(2006);
	p2.setQuarter(5);
	p3.setYear(2007);
	p3.setQuarter(4);

	periods.add(p1);
	periods.add(p2);
	periods.add(p3);

	final Company cars = new Company();
	cars.setCompanyType(CompanyType.CARS);
	final Company investor = new Company();
	investor.setCompanyType(CompanyType.INVESTOR);
	final Company cdfi = new Company();
	cdfi.setCompanyType(CompanyType.CDFI);

	admin.setCompany(cars);
	admin.setPersonRole(PersonRole.ADMIN);
	analyst.setCompany(cars);
	analyst.setPersonRole(PersonRole.BASIC);
	subscriber.setCompany(investor);
	cdfiUser.setCompany(cdfi);
    }

    @Test
    public void testGetMostRecent() {
	final Period recentPeriod = Period.getMostRecent(periods);
	Assert.assertEquals(p3, recentPeriod);
    }

    @Test
    public void testIsDraftColumnsVisible() {
	Assert.assertTrue(Period.isDraftColumnsVisible(admin));
	Assert.assertTrue(Period.isDraftColumnsVisible(analyst));
	Assert.assertTrue(Period.isDraftColumnsVisible(cdfiUser));
	Assert.assertFalse(Period.isDraftColumnsVisible(subscriber));
    }
    
    @Test
    public void testCompare() {
    	Assert.assertTrue(p1.compareTo(p2) == -1);
    	Assert.assertTrue(p2.compareTo(p1) == 1);
    	Assert.assertTrue(p3.compareTo(p1) == 1);
    	Assert.assertTrue(p3.compareTo(null) == 1);
    	Assert.assertTrue(p3.compareTo(p3) == 0);
    	Assert.assertTrue(p3.newest(p1) == p3);
    	Assert.assertTrue(p1.newest(p2) == p2);

    }
}
