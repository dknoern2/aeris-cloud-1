package com.cars.platform.util;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Test;

public class DateHelperTest {

	@Test
	public void testGetQuarterEndDate() {
		String s = "";
		s = DateHelper.getQuarterEndDate(2012, 1, 1);
		assertEquals("3/31/2012", s);
		s = DateHelper.getQuarterEndDate(2012, 2, 1);
		assertEquals("6/30/2012", s);
		s = DateHelper.getQuarterEndDate(2012, 3, 1);
		assertEquals("9/30/2012", s);
		s = DateHelper.getQuarterEndDate(2012, 4, 1);
		assertEquals("12/31/2012", s);

		s = DateHelper.getQuarterEndDate(2012, 1, 7);
		assertEquals("9/30/2011", s);
		s = DateHelper.getQuarterEndDate(2012, 2, 7);
		assertEquals("12/31/2011", s);
		s = DateHelper.getQuarterEndDate(2012, 3, 7);
		assertEquals("3/31/2012", s);
		s = DateHelper.getQuarterEndDate(2012, 4, 7);
		assertEquals("6/30/2012", s);

		s = DateHelper.getQuarterEndDate(2012, 1, 10);
		assertEquals("12/31/2011", s);
		s = DateHelper.getQuarterEndDate(2012, 2, 10);
		assertEquals("3/31/2012", s);
		s = DateHelper.getQuarterEndDate(2012, 3, 10);
		assertEquals("6/30/2012", s);
		s = DateHelper.getQuarterEndDate(2012, 4, 10);
		assertEquals("9/30/2012", s);
	}

	@Test
	public void testGetQuarterStartDate() {

		String s = "";
		s = DateHelper.getQuarterStartDate(2012, 1, 1);
		assertEquals("1/01/2012", s);
		s = DateHelper.getQuarterStartDate(2012, 2, 1);
		assertEquals("4/01/2012", s);
		s = DateHelper.getQuarterStartDate(2012, 3, 1);
		assertEquals("7/01/2012", s);
		s = DateHelper.getQuarterStartDate(2012, 4, 1);
		assertEquals("10/01/2012", s);

		s = DateHelper.getQuarterStartDate(2012, 1, 7);
		assertEquals("7/01/2012", s);
		s = DateHelper.getQuarterStartDate(2012, 2, 7);
		assertEquals("10/01/2012", s);
		s = DateHelper.getQuarterStartDate(2012, 3, 7);
		assertEquals("1/01/2013", s);
		s = DateHelper.getQuarterStartDate(2012, 4, 7);
		assertEquals("4/01/2013", s);
	}

	@Test
	public void testCurrentYear() {
		DateHelper.getCurrentYear(1);
	}

	@Test
	public void testGetQuarter() throws ParseException {

		// fystart month, current month

		assertEquals(1, DateHelper.getQuarter(1, "1/1/2012"));
		assertEquals(1, DateHelper.getQuarter(1, "2/1/2012"));
		assertEquals(1, DateHelper.getQuarter(1, "3/1/2012"));
		assertEquals(2, DateHelper.getQuarter(1, "4/1/2012"));
		assertEquals(2, DateHelper.getQuarter(1, "5/1/2012"));
		assertEquals(2, DateHelper.getQuarter(1, "6/1/2012"));

		assertEquals(3, DateHelper.getQuarter(1, "7/1/2012"));
		assertEquals(3, DateHelper.getQuarter(1, "8/1/2012"));
		assertEquals(3, DateHelper.getQuarter(1, "9/1/2012"));
		assertEquals(4, DateHelper.getQuarter(1, "10/1/2012"));
		assertEquals(4, DateHelper.getQuarter(1, "11/1/2012"));
		assertEquals(4, DateHelper.getQuarter(1, "12/1/2012"));

		assertEquals(2, DateHelper.getQuarter(4, "7/1/2012"));

		assertEquals(4, DateHelper.getQuarter(11, "8/1/2012"));
		assertEquals(4, DateHelper.getQuarter(11, "9/1/2012"));
		assertEquals(4, DateHelper.getQuarter(11, "10/1/2012"));
		assertEquals(1, DateHelper.getQuarter(11, "11/1/2012"));
		assertEquals(1, DateHelper.getQuarter(11, "12/1/2012"));
		assertEquals(1, DateHelper.getQuarter(11, "1/1/2012"));
		assertEquals(2, DateHelper.getQuarter(11, "2/1/2012"));
		assertEquals(2, DateHelper.getQuarter(11, "3/1/2012"));
		assertEquals(2, DateHelper.getQuarter(11, "4/1/2012"));
		assertEquals(3, DateHelper.getQuarter(11, "5/1/2012"));
		assertEquals(3, DateHelper.getQuarter(11, "6/1/2012"));
		assertEquals(3, DateHelper.getQuarter(11, "7/1/2012"));

	}

	@Test
	public void testGetQuarterStart() {
		// FYstart, year, quarter
		Calendar start = DateHelper.getQuarterStart(1, 2012, 1);

		SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
		// FYstart, year, quarter
		assertEquals("1/1/2012", sdf.format(DateHelper.getQuarterStart(1, 2012, 1).getTime()));
		assertEquals("4/1/2012", sdf.format(DateHelper.getQuarterStart(1, 2012, 2).getTime()));
		assertEquals("4/1/2013", sdf.format(DateHelper.getQuarterStart(1, 2013, 2).getTime()));

		assertEquals("4/1/2011", sdf.format(DateHelper.getQuarterStart(4, 2012, 1).getTime()));
		assertEquals("7/1/2011", sdf.format(DateHelper.getQuarterStart(4, 2012, 2).getTime()));
		assertEquals("7/1/2012", sdf.format(DateHelper.getQuarterStart(4, 2013, 2).getTime()));
	}

	@Test
	public void testPriorYearEndString() throws ParseException {

		assertEquals("12/31/2011", DateHelper.getPriorYearEndString("3/31/2012", 1));
	}

	@Test
	// this doesn't work for 5 days after change to/from daylight savings time
	public void testGetDaysUntilDue() {
		Calendar calendar = Calendar.getInstance();
		assertEquals(0, DateHelper.getDaysUntilDue(calendar));

		calendar.add(Calendar.DAY_OF_MONTH, -5);
		int daysTillDue = DateHelper.getDaysUntilDue(calendar);
		assertEquals(-5, daysTillDue);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetMonthsBetweenIncorrectArgs() throws ParseException {
		DateHelper.getMonthsBetween("12/31/2010", "12/31/2011");
	}

	@Test
	public void testGetMonthsBetween() throws ParseException {
		assertEquals(12, DateHelper.getMonthsBetween("12/31/2011", "12/31/2010"));
		assertEquals(3, DateHelper.getMonthsBetween("9/30/2011", "6/30/2011"));
		assertEquals(18, DateHelper.getMonthsBetween("12/31/2011", "6/30/2010"));
	}

}
